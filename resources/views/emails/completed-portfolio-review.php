<?php echo View::make('emails.partials.email_header'); ?>
  <h1 style="
              background-color: #23c6c8;
              color: #fff;
              padding: 20px;
              ">Completed Portfolio Review</h1>
  <div style="padding: 0 40px 40px 40px;">
  	<p style="color: rgb(103,109,118);">Hello <?=$recipient?>,<br />

  	<?=$message_text?></p>
</div>
<?php echo View::make('emails.partials.email_footer'); ?>
