<?php echo View::make('emails.partials.email_header'); ?>
  <h1 style="
              background-color: #23c6c8;
              color: #fff;
              padding: 20px;
              ">Reset Password</h1>
  <div style="padding: 0 40px 40px 40px;">
  	<p style="color: rgb(103,109,118);">Hello <?=$user['UsersFirstName']?>,<br />

      Lets get started with resetting your password.
      
    </p>
    <hr />

    <p style="color: rgb(103,109,118); font-size: 20px;">Please click here to reset your account password. </p>

    <a
       href="<?=url('set_password/' . md5($user['UsersPassword']))?>"
       style="
              background-color: #1ab394;
              display: block;
              padding: 12px;
              border-radius: 3px;
              text-align: center;
              text-decoration: none;
              font-size: 18px;
              color: #fff;">Set your account password!</a>

    <p style="color: rgb(103,109,118); font-size: 20px;">
      Sincerely,<br />
      Your Portfolio Team
    </p>
</div>
<?php echo View::make('emails.partials.email_footer'); ?>
