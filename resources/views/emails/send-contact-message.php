<?php echo View::make('emails.partials.email_header'); ?>
  <h1 style="
              background-color: #23c6c8;
              color: #fff;
              padding: 20px;
              ">Contact Form Message</h1>
  <div style="padding: 0 40px 40px 40px;">
  	<p style="color: rgb(103,109,118);">Hello,<br />

  	<?=$sender['UsersFirstName']?> <?=$sender['UsersLastName']?> submitted a contact form message.</p>
    <hr />

  	<p style="color: rgb(103,109,118); font-size: 20px;"><?=$message_text?></p>
    <p>You can reply to this user at <?=$sender['UsersEmail']?></p>
</div>
<?php echo View::make('emails.partials.email_footer'); ?>
