<?php echo View::make('emails.partials.email_header'); ?>
  <h1 style="
              background-color: #1ab394;
              color: #fff;
              padding: 20px;
              ">Welcome!</h1>
  <div style="padding: 0 40px 40px 40px;">
  	<p style="color: rgb(103,109,118);">Hello <?=$user['UsersFirstName']?>,<br />

      <?=$sender['UsersFirstName']?> has invited you to the <strong><?=$account['AccountsDisplayName']?></strong> Portfolio account.</p>
    <hr />

  	<p style="color: rgb(103,109,118); font-size: 20px;">Please <a href="<?=url('')?>">click here</a> to visit your account.</p>

    <p style="color: rgb(103,109,118); font-size: 20px;">
      Sincerely,<br />
      Your <?=$account['AccountsDisplayName']?> Portfolio Team
    </p>
</div>
<?php echo View::make('emails.partials.email_footer'); ?>
