<div class="loginColumns animated fadeInDown" ng-controller="AuthForgotPasswordCtrl">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
          <div class="ibox-content">
            <div class="col-md-12 m-b" style="text-align: center;">
              <img src="app/images/logo_stacked_small.png" alt="Venture Logo" />
            </div>
              <form class="m-t" ng-submit="reset_submit()">
                  <h1>Forgot your password?</h1>

                  <?php if ( Request::is('forget_password/expired') ) : ?>
                    <div class="alert alert-danger">
                      <p><strong>Oops!</strong> It looks like the reset password link you
                      followed is expired.</p>
                    </div>
                  <?php endif; ?>

                  <p>Enter your email address below and we'll send you a link to reset your password.</p>

                  <div ng-show="error_msg" class="alert alert-danger">
                    <p><strong>Oops!</strong> You entered an email address that we could not find. Try again or <a href="/login" class="alert-link">go back to the login screen.</a></p>
                  </div>

                  <div ng-show="success_msg" class="alert alert-success">
                    <h4>Success!</h4>
                    <p>Please check your email. We sent you instructions for resetting your password.
                    <br /><a href="/login" class="alert-link">Great! Take me back to the login screen.</a>
                    </p>
                  </div>

                  <div class="form-group">
                    <input type="text" class="form-control" placeholder="Enter your account Email Address" ng-model="fields.UsersEmail" />
                  </div>

                  <button type="submit" class="btn btn-primary block full-width m-b">Reset Password</button>

                  <a href="/login">Cancel</a>
              </form>
            </div>
        </div>
    </div>
</div>
