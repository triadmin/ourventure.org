<script>
  var hash = '<?=$hash?>';
</script>

<div class="loginColumns animated fadeInDown" ng-controller="AuthSetPasswordCtrl">
    <div class="row">

        <div class="col-md-8 col-md-offset-2">
          <div class="ibox-content">
            <div class="col-md-12 m-b" style="text-align: center;">
              <img src="app/images/logo_stacked_small.png" alt="Venture Logo" />
            </div>
            <form class="m-t" ng-submit="submit()">
                <h1>Set or reset your password.</h1>
                <p>You are here because you are setting up a new account password or resetting
                your password for an existing account.</p>

                <div ng-show="error_msg" class="alert alert-danger">{{ error_msg }}</div>

                <div class="form-group">
                  <input type="password" class="form-control" placeholder="Enter your new password" ng-model="fields.UsersPassword" />
                </div>

                <button type="submit" class="btn btn-primary block full-width m-b">Set Password</button>
            </form>
          </div>
        </div>
    </div>
</div>
