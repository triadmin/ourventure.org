<div class="loginColumns animated fadeInDown" ng-controller="AuthLoginCtrl">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">

            <div class="ibox-content">
              <div class="col-md-12 m-b" style="text-align: center;">
                <img src="app/images/logo_stacked_small.png" alt="Venture Logo" />
              </div>
              <form class="m-t" ng-submit="login_submit()">
                  <?php if ( Request::is('login/goodbye') ) : ?>
                  <h1>Goodbye!</h1>
                  <p>You have logged out.  See you again soon!</p>
                  <?php elseif ( Request::is('login/setpassword') ) : ?>
                  <h1>You have reset your password.</h1>
                  <p>Go ahead and log in below.</p>
                  <?php else : ?>
                  <h1>Hello!</h1>
                  <p>Log in to get started.</p>
                  <?php endif; ?>

                  <div ng-show="error_msg" class="alert alert-danger">
                    <p><strong>Oops!</strong> You entered an incorrect email or password. Please try again.<br />
                    <a href="/forget_password" class="alert-link">I think I forgot my password!</a></p>
                  </div>

                  <div class="form-group">
                    <input type="text" class="form-control" placeholder="Email" ng-model="fields.UsersEmail" />
                  </div>

                  <div class="form-group">
                    <input type="password" class="form-control" placeholder="Password" ng-model="fields.UsersPassword" />
                  </div>

                  <button type="submit" class="btn btn-primary block full-width m-b">Login</button>

                  <a href="/forget_password">I forgot my password. Help!</a>
              </form>
            </div>
        </div>
    </div>
</div>
