<!DOCTYPE html>
<html lang="en" ng-app="app">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <base href="/">

  <title>Venture : Welcome</title>

  <link href="/app/css/bootstrap.min.css" rel="stylesheet">
  <link href="/app/font-awesome/css/font-awesome.css" rel="stylesheet">

  <link href="/app/css/animate.css" rel="stylesheet">
  <link href="/app/css/style.css" rel="stylesheet">
  <link href="/app/css/styles_public.css" rel="stylesheet">

  <script src="/app/bower/angular/angular.min.js"></script>
  <script src="/app/bower/angular-route/angular-route.min.js"></script>

  <script src="/app/controllers/public-pages.js"></script>
</head>

<body class="top-navigation">
  <div id="wrapper">
    <div id="page-wrapper" class="gray-bg">

      <?=$body?>

    </div>
  </div>

</body>

</html>
