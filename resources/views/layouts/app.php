<!DOCTYPE html>
<html lang="en" ng-app="app">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <base href="/">

    <title>Venture ePortfolio</title>

    <!-- ***** FAVICONS *****-->
    <link rel="apple-touch-icon" sizes="57x57" href="/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/apple-touch-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/apple-touch-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/apple-touch-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/apple-touch-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/apple-touch-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon-180x180.png">
    <link rel="icon" type="image/png" href="/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="/android-chrome-192x192.png" sizes="192x192">
    <link rel="icon" type="image/png" href="/favicon-96x96.png" sizes="96x96">
    <link rel="icon" type="image/png" href="/favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="/manifest.json">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="msapplication-TileImage" content="/mstile-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <link href="<?=elixir('assets/css/app.css')?>" rel="stylesheet">

    <script>
      <?php
      // Any data from server we need prior to making an AJAX call,
      // should go here
      ?>
      var site = {
        masquerade_secret_url: '<?=Config::get('site.masquerade_secret_url')?>',
        account_id: <?=App\Library\Me::get_account_id()?>,
        user_id: <?=App\Library\Me::get_id()?>,
        scoring_plugins: <?=json_encode(Config::get('site.scoring_plugins'))?>
      }
    </script>

    <script src="<?=elixir('assets/js/app.js')?>"></script>
  </head>

  <body class="top-navigation" ng-controller="SiteWideCtrl">
    <div id="skiptocontent" class="no-print">
      <a href="" ng-click="scrollTo('maincontent')" tabindex="1">Skip to main content</a>
    </div>

    <!-- Toastr notification container -->
    <toaster-container toaster-options="{
          'toaster-id': 'toaster-activity',
          'position-class': 'toast-bottom-left',
          'time-out': 2000,
          'showCloseButton': false
    }"></toaster-container>

    <div id="wrapper">
      <!-- Top navigation -->
      <div ng-include="'app/views/common/topnavbar.html'"></div>

      <!-- Page wrapper :: Main page content of all sorts goes here -->
      <div id="page-wrapper" class="gray-bg">
        <div class="">
          <a name="maincontent"></a>
          <!-- Where the wild things are -->
          <div ng-view></div>
        </div>

        <!-- Footer -->
        <div ng-include="'app/views/common/footer.html'"></div>
      </div>

    </div>
    <!-- End wrapper -->
  </body>

  <script type="text/javascript">
  $(document).ready(function() {
    // Init some stuff for DOM manipulation.
    app_jquery.init();
  });
  </script>

</html>
