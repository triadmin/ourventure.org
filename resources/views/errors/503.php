
<!DOCTYPE html>
<html lang="en" ng-app="app">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <base href="/">

  <title>Venture System Maintenance</title>

  <link href="/app/css/bootstrap.min.css" rel="stylesheet">
  <link href="/app/font-awesome/css/font-awesome.css" rel="stylesheet">

  <link href="/app/css/animate.css" rel="stylesheet">
  <link href="/app/css/style.css" rel="stylesheet">
  <link href="/app/css/styles_public.css" rel="stylesheet">
</head>

<body class="gray-bg image-bg">

  <div class="loginColumns animated fadeInDown" ng-controller="AuthLoginCtrl">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">

            <div class="ibox-content">
              <div class="col-md-12 m-b" style="text-align: center;">
                <img src="app/images/logo_stacked_small.png" alt="Venture Logo" />
              </div>

              <h1>We're doing some maintenance.</h1>
              <h4 class="m-t" style="line-height: 20px;">Every so often, the Venture system needs some extra attention.  We'll get the system back online by tomorrow morning.</h4>
              <h4>Thank you for your patience! ~ The Venture Team</h4>

            </div>
        </div>
    </div>
</div>

</body>

</html>
