
<!DOCTYPE html>
<html lang="en" ng-app="app">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <base href="/">

  <title>404 Error : Not found, oops!</title>

  <link href="/app/css/bootstrap.min.css" rel="stylesheet">
  <link href="/app/font-awesome/css/font-awesome.css" rel="stylesheet">

  <link href="/app/css/animate.css" rel="stylesheet">
  <link href="/app/css/style.css" rel="stylesheet">
  <link href="/app/css/styles_public.css" rel="stylesheet">
</head>

<body class="gray-bg image-bg-404">

  <div class="loginColumns animated fadeInDown" ng-controller="AuthLoginCtrl">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">

            <div class="ibox-content">
              <div class="col-md-12 m-b" style="text-align: center;">
                <img src="app/images/logo_stacked_small.png" alt="Venture Logo" />
              </div>

              <h1>Uh oh! We're lost!</h1>
              <p>The place you're looking for simply isn't there. Would you like to...</p>

              <ul>
                <li><a href="/login">Log in to your account?</a></li>
                <li><a href="/contact">Contact our support staff?</a></li>
                <li><a href="/help">Read or watch some helpful tutorials?</a>
              </ul>

            </div>
        </div>
    </div>
</div>

</body>

</html>
