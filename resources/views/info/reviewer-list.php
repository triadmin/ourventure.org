<div ng-controller="ReviewerListCtrl">
  <div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-12">
      <h1>Reviewers for {{ ptype_info.PTypesTitle }}</h1>
    </div>
  </div>

  <div class="row wrapper wrapper-content animated fadeInRight">
    <div ng-if="has_reviewers">
      <div class="col-lg-6 repeat-item" ng-repeat="row in reviewers">
        <div class="ibox">
          <div class="ibox-content">
            <img src="{{ row.MediaUrl }}" class="img-circle col-sm-2" />
            <div class="col-sm-10">
              <h2>{{ row.UsersFirstName }} {{ row.UsersLastName }}</h2>
              <strong>
                <span ng-if="row.UserTitle.length > 0">{{ row.UsersTitle }}</span>
                <span ng-if="row.UsersInstitution.length > 0"> {{ row.UsersInstitution }}</span>
              </strong>
              <p class="m-t">
                {{ row.UsersBio }}
              </p>
            </div>
            <div class="clearfix"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
