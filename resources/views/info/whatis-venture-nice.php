<div class="row wrapper border-bottom white-bg page-heading">
  <div class="col-sm-12">
    <h1>What is Venture and NICE?</h1>
  </div>
</div>

<div class="row wrapper wrapper-content animated fadeInRight">
  <div class="col-lg-8 col-lg-offset-2 repeat-item">
    <div class="ibox">
      <div class="ibox-content emph-text">
        <p>
          <img style="width: 100%;" src="https://91372e5fba0d1fb26b72-13cee80c2bfb23b1a8fcedea15638c1f.ssl.cf1.rackcdn.com/cms/NICE_logo_certificate_1114.png" alt="NICE logo." />
        </p>

        <h2>Introducing NICE</h2>
        <iframe class="m-b-lg" width="700" height="415" src="https://www.youtube.com/embed/bxBHjpuXvCY?rel=0" frameborder="0" allowfullscreen></iframe>

        <p>
          Venture is the house where NICE lives!  Venture is an e-portfolio platform that is designed to hold many forms of media to create a story about a person's competence.
        </p>

        <p>
          NICE, which stands for the National Intervener Certification E-portfolio, is the specific process that was designed by interveners and people that understand what they do, to have a process where interveners can show what they believe, know and can do in their practice. <a href="https://nationaldb.org/pages/show/national-intervener-certification-e-portfolio-nice/what-is-nice">Get more information about NICE</a>.
        </p>

        <p>
          The NICE process is aligned with national standards for interveners which have been published by the Council for Exceptional Children, an organization that creates standards for all special educators.
        </p>

        <h2>What is an intervener?</h2>
        <iframe class="m-b-lg" width="700" height="415" src="https://www.youtube.com/embed/gupNRww6vFc?rel=0" frameborder="0" allowfullscreen></iframe>

        <p>
          See <a href="https://nationaldb.org/library/page/2267">this NCDB library item</a> for more information on interveners.
        </p>

        <p>
            <a href="https://moodle.nationaldb.org/mod/page/view.php?id=24822">Want to learn more about intervention?</a>
        </p>
      </div>
    </div>
  </div>
</div>
