#!/bin/sh

# Setup settings
servers=('deploy@tr4.nationaldb.org')
branch='master'
port='9022'
remote_dir='/var/www/testing.ourventure.org'

# Loop through the different servers and deploy
for server in "${servers[@]}"
do
	echo "## Deploying to $server ##"
	ssh -p $port $server "cd $remote_dir && git pull origin $branch && php artisan migrate --force && /usr/local/bin/composer install"
done