<?php

namespace App\Models;

use App;
use Auth;
use DB;

class Standards extends \App\Library\Model
{
  public $joins = [];

  /**
   * get_standards_with_competencies: Get all standards and
   *     competencies for a given Portfolio Type Id
   * @param  int $pTypeId Portfolio Type Id
   * @return array $new_data Array of Standards with Competencies
   */
  public function get_standards_with_competencies( $pTypeId )
  {
    $data = DB::select( DB::raw("
      SELECT * FROM Standards S
      LEFT JOIN Competencies C ON C.CompetenciesStandardsId = S.StandardsId
      WHERE
      StandardsPTypesId = $pTypeId
      ORDER BY S.StandardsOrder, C.CompetenciesOrder
    ") );

    $standardsId = '0';
    $new_data = [];
    $j = 0;
    $i = 0;
    foreach ( $data as $key => $value )
    {
      if ( $value->StandardsId != $standardsId )
      {
        $new_data[$i]['StandardsId'] = $value->StandardsId;
        $new_data[$i]['StandardsTitle'] = $value->StandardsTitle;
        $new_data[$i]['Competencies'] = [];

        $standardsId = $value->StandardsId;
        $j = 0;
        $i++;
      }

      if ( $value->CompetenciesId > 0 )
      {
        $competencies_array[$i][$j] = array(
          'Id' => $value->CompetenciesId,
          'Title' => $value->CompetenciesTitle
        );
      } else {
        $competencies_array[$i] = null;
      }
      $j++;
    }

    foreach ( $new_data as $key => $value )
    {
      $new_data[$key]['Competencies'] = $competencies_array[$key + 1];
    }

    return $new_data;
  }

  //
  // Delete by id.
  //
  public function delete_by_id($id)
  {
    // First we delete any related Competencies
    $competencies_model = App::make('App\Models\Competencies');

    $competencies_model->set_col('CompetenciesStandardsId', $id);
    foreach($competencies_model->get() AS $key => $row)
    {
      $competencies_model->delete_by_id($row['CompetenciesId']);
    }

    return parent::delete_by_id($id);
  }
}

/* End File */
