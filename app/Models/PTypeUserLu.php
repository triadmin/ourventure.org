<?php
namespace App\Models;

use DB;
use App;
use App\Helpers\Helpers;

class PTypeUserLu extends \App\Library\Model
{
  public $joins = [
    [ 'table' => 'Users', 'left' => 'UsersId', 'right' => 'PTypeUserLuUserId' ],
    [ 'table' => 'Media', 'left' => 'UsersAvatarId', 'right' => 'MediaId', 'type' => 'left' ]
  ];

  //
  // Construct.
  //
  public function __construct()
  {
    parent::__construct();

    // Load shared modals
    $this->PortfolioUserLu_model = App::make('App\Models\PortfolioUserLu');
    $this->Media_model = App::make('App\Models\Media');
  }

  //
  // Get users for a ptype. user_type: Mentor | Reviewer
  //
  public function get_ptype_users( $user_type, $ptypeid )
  {
    $this->set_col( 'PTypeUserLuPTypeId', $ptypeid );
    $this->set_col( 'PTypeUserLuUserType', $user_type );
    $this->set_order( 'UsersLastName', 'ASC' );
    $data = $this->get();

    $new_data = [];
    $data = Helpers::objToArray( $data );
    foreach ( $data as $item )
    {
      // Get candidates assigned to this mentor or reviewer ( $userid, $user_type, $ptypeid )
      $assigned_candidates = $this->PortfolioUserLu_model->get_portfolios_assigned_to_user( $item['PTypeUserLuUserId'], $user_type, $ptypeid );

      $assigned_candidates_string = '';
      foreach ( Helpers::objToArray( $assigned_candidates ) as $candidate )
      {
        $assigned_candidates_string .= $candidate['UsersFirstName'] . ' ' . $candidate['UsersLastName'] . ', ';
      }
      // Remove trailing comma
      $assigned_candidates_string = rtrim( $assigned_candidates_string, ', ');

      $item['Assigned_candidates'] = $assigned_candidates;
      $item['Assigned_candidates_string'] = $assigned_candidates_string;
      $new_data[] = $item;
    }

    return $new_data;
  }

  //
  // Get users assigned to a candidate: Mentors and Reviewers.
  // Not sure why this is here...I think we're doing this somewhere else.
  //
  public function get_assigned_users( $userid, $ptypeid )
  {
    $this->set_col( 'PTypeUserLuPTypeId', $ptypeid );
    $this->set_col( 'PTypeUserLuUserType', $user_type );
    $this->set_order( 'UsersLastName', 'ASC' );
    $data = $this->get();
    return $data;
  }

  public function _format_get( &$data )
  {
    // Add media
    if(isset($data['MediaId']) && $data['MediaId'] > 0)
    {
      $this->Media_model->add_meta_data($data);
    } else {
      $data['MediaUrl'] = '/app/images/avatar-default.png';
    }
  }
}

/* End File */
