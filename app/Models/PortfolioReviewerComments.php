<?php

namespace App\Models;

use Auth;

class PortfolioReviewerComments extends \App\Library\Model
{
  public $joins = [
    [ 'table' => 'Users', 'left' => 'PortfolioReviewerCommentsUsersId', 'right' => 'UsersId' ]
  ];
}
