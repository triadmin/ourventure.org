<?php

namespace App\Models;

use App;
use Auth;
use DB;

class ArtifactScores extends \App\Library\Model
{
  public $joins = [
    [ 'table' => 'Users', 'left' => 'ArtifactScoresReviewerId', 'right' => 'UsersId' ]
  ];

  //
  // get_scored_ratio: get the ratio of completely scored
  // artifacts (e.g. artifacts with 2 scores and IOA)
  //
  public function get_scored_ratio( $portfolio_id )
  {

  }

  //
  // get_scored_ratio_for_reviewer: get the ratio of
  // scored artifacts for 1 reviewer.
  //
  public function get_scored_ratio_for_reviewer( $portfolio_id, $reviewer_id )
  {
    $artifact_count = DB::table('Artifacts')
                      ->where('ArtifactsPortfolioId', '=', $portfolio_id)
                      ->count();
    $scored_artifact_count = DB::table('ArtifactScores')
                            ->where('ArtifactScoresPortfolioId', '=', $portfolio_id)
                            ->where('ArtifactScoresReviewerId', '=', $reviewer_id)
                            ->count();
    return array(
      'artifact_count' => $artifact_count,
      'scored_artifact_count' => $scored_artifact_count
    );
  }
}
