<?php

namespace App\Models;

class ReleaseMediaLu extends \App\Library\Model
{
  public $joins = [
    [ 'table' => 'Media', 'left' => 'MediaId', 'right' => 'ReleaseMediaLuMediaId' ] 
  ];  
  
  //
  // Construct.
  //
  public function __construct(\App\Models\Media $media_model)
  {
    parent::__construct();
    $this->media_model = $media_model;
  }
  
  //
  // Format get.
  //
  public function _format_get(&$data)
  {
    // Add media
    if(isset($data['MediaId']))
    {
      $this->media_model->add_meta_data($data);
    }
  }
}

/* End File */