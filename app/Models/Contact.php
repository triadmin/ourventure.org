<?php

namespace App\Models;

use Auth;

class Contact extends \App\Library\Model
{
  public $joins = [
    [ 'table' => 'Users', 'left' => 'UsersId', 'right' => 'ContactUsersId' ],
    [ 'table' => 'Media', 'left' => 'UsersAvatarId', 'right' => 'MediaId', 'type' => 'left' ]
  ];

  //
  // Construct.
  //
  public function __construct(\App\Models\Media $media_model)
  {
    parent::__construct();
    $this->media_model = $media_model;
  }

  //
  // Format get.
  //
  public function _format_get(&$data)
  {
    // Add media
    if(isset($data['MediaId']))
    {
      $this->media_model->add_meta_data($data);
    }
  }
}

/* End File */
