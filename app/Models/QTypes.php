<?php

namespace App\Models;

class QTypes extends \App\Library\Model
{
  //
  // Format get.
  //
  public function _format_get(&$data)
  {
    // Explode our values.
    if( isset($data['QTypesValues']) && !empty($data['QTypesValues']) )
    {
      $tmp = explode(',', $data['QTypesValues']);
      foreach($tmp AS $key => $row)
      {
        $data['Values'][] = trim($row);
      }
    }
  }
}

/* End File */
