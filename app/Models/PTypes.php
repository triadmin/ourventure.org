<?php

namespace App\Models;

use App;

class PTypes extends \App\Library\Model
{
  public $joins = [];

  public function get_ptype_stats( $ptype_id )
  {
    // Get number of candidates (portfolios)
    $portfolios_model = App::make('App\Models\Portfolios');
    $portfolios_model->set_col( 'PortfoliosPTypeId', $ptype_id );
    $portfolios_model->set_col( 'PortfoliosStatus', 'Inactive', '<>' );
    $portfolios = $portfolios_model->get();

    // Now, rip through portfolios and count:
    // In Progress, Ready for review, In review, Scored, Average Scored
    $data = [];
    $data['Candidates'] = count( $portfolios );

    $data['InProgress'] = 0;
    $data['ReadyForReview'] = 0;
    $data['InReview'] = 0;
    $data['Scored'] = 0;
    $total_score = 0;
    foreach ( $portfolios as $portfolio )
    {
      switch ( $portfolio['PortfoliosStatus'] )
      {
        case 'In Progress':
          $data['InProgress']++;
          break;
        case 'Submitted for Review':
          $data['ReadyForReview']++;
          break;
        case 'In Review':
          $data['InReview']++;
          break;
        case 'Scored':
          $data['Scored']++;
          break;
      }
      $total_score += $portfolio['PortfoliosScore'];
    }

    if ( count( $portfolios ) > 0 )
    {
      $data['InProgressPercentage'] = number_format( ( $data['InProgress'] / $data['Candidates'] ) * 100 );
      $data['ReadyForReviewPercentage'] = number_format( ( $data['ReadyForReview'] / $data['Candidates'] ) * 100 );
      $data['InReviewPercentage'] = number_format( ( $data['InReview'] / $data['Candidates'] ) * 100 );
      $data['ScoredPercentage'] = number_format( ( $data['Scored'] / $data['Candidates'] ) * 100 );
      $data['AverageScore'] = number_format( ( $total_score/ $data['Candidates'] ) );
    } else {
      $data['InProgressPercentage'] = 0;
      $data['ReadyForReviewPercentage'] = 0;
      $data['InReviewPercentage'] = 0;
      $data['ScoredPercentage'] = 0;
      $data['AverageScore'] = 0;
    }

    return $data;
  }

  //
  // Insert...
  //
  public function insert($data)
  {
    $this->_data_munge_post_update($data);
    return parent::insert($data);
  }

  //
  // Update...
  //
  public function update($data, $id)
  {
    $this->_data_munge_post_update($data);
    return parent::update($data, $id);
  }

  // -------------- Private Helpers ---------------- //

  //
  // Deal with data on update / insert
  //
  private function _data_munge_post_update(&$data)
  {
    // Format start date.
    if(isset($data['PTypesStartDate']))
    {
      $data['PTypesStartDate'] = date('Y-m-d', strtotime($data['PTypesStartDate']));
    }

    // Format end date.
    if(isset($data['PTypesEndDate']))
    {
      $data['PTypesEndDate'] = date('Y-m-d', strtotime($data['PTypesEndDate']));
    }

    // We have to do because of the odd ball switches on the ui.
    $toggles = [ 'PTypesMentor', 'PTypesViewScores', 'PTypesSharing' ];
    foreach($toggles AS $key => $row)
    {
      if(isset($data[$row]) && ($data[$row] == 1))
      {
        $data[$row] = 'Yes';
      } else
      {
        $data[$row] = 'No';
      }
    }
  }
}

/* End File */
