<?php

namespace App\Models;

class AcctUsersLu extends \App\Library\Model
{
  public $no_account = true;
  
  //
  // Construct.
  //
  public function __construct(\App\Models\Media $media_model)
  {
    parent::__construct();
    $this->media_model = $media_model;
  }

  public $joins = [
    [ 'table' => 'Users', 'left' => 'AcctUsersLuUserId', 'right' => 'UsersId' ],
    [ 'table' => 'Media', 'left' => 'UsersAvatarId', 'right' => 'MediaId', 'type' => 'left' ]
  ];

  public function _format_get( &$data )
  {
    $data['IsReviewer'] = $data['AcctUsersLuReviewer'];
    $data['IsCandidate'] = $data['AcctUsersLuCandidate'];
    $data['IsMentor'] = $data['AcctUsersLuMentor'];
    $data['IsAdmin'] = $data['AcctUsersLuAdmin'];

    // Add media
    if(isset($data['MediaId']) && $data['MediaId'] > 0)
    {
      $this->media_model->add_meta_data($data);
    } else {
      $data['MediaUrl'] = '/app/images/avatar-default.png';
    }
  }
}

/* End File */
