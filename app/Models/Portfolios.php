<?php

namespace App\Models;

use Auth;
use App;
use DB;
use App\Library\Me;
use App\Helpers\HelpersPortfolio;
use App\Helpers\Helpers;

class Portfolios extends \App\Library\Model
{
  public $joins = [
    [ 'table' => 'PTypes', 'left' => 'PTypesId', 'right' => 'PortfoliosPTypeId' ],
    [ 'table' => 'Users', 'left' => 'UsersId', 'right' => 'PortfoliosUserId' ],
    [ 'table' => 'Media', 'left' => 'UsersAvatarId', 'right' => 'MediaId', 'type' => 'left' ]
  ];

  //
  // Construct.
  //
  public function __construct()
  {
    parent::__construct();

    // Load shared modals
    $this->PortfolioUserLu_model = App::make('App\Models\PortfolioUserLu');
    $this->Media_model = App::make('App\Models\Media');
    $this->AcctUsersLu_model = App::make('App\Models\AcctUsersLu');
  }

  //
  // Get.
  //
  public function get()
  {
    // Restrict access for users who are not Mentors,
    // Reviewers, or Admins
    if(
      Me::get('AcctUsersLuAdmin') != 'Yes' &&
      Me::get('AcctUsersLuMentor') != 'Yes' &&
      Me::get('AcctUsersLuReviewer') != 'Yes'
    )
    {
      $this->set_col('PortfoliosUserId', Me::get_id());
    }

    // Only return Portfolios that ARE NOT marked Inactive
    $this->set_col( 'PortfoliosStatus', 'Inactive', '<>' );
    return parent::get();
  }

  /**
   * calculate_portfolio_progress: Find all standards and competencies
   *     for a portfolio, run through them and calculate their completion
   *     status. Then calculate completion status of entire portfolio and
   *     save to database. This method will return data needed to build
   *     a portfolio menu (e.g. standards and competencies, if they exist)
   * @param  int $portfolio_id Candidate's portfolioId
   * @return array $progress_array
   */
  public function calculate_portfolio_progress( $portfolio_id )
  {

    // TODO: This method sucks - refactor! We need something like:
    // function get_competency_status
    // function get_standard_status

    $artifacts_model = App::make('App\Models\Artifacts');

    // Get some portfolio information
    $portfolio = $this->get_by_id( $portfolio_id );
    $ptype_id = $portfolio['PortfoliosPTypeId'];

    // Get standards and competencies for this PType
    $standards_model = App::make('App\Models\Standards');
    $standards_competencies = $standards_model->get_standards_with_competencies( $ptype_id );

    $standard_complete_count = 0;
    if ( count( $standards_competencies ) > 0 )
    {
      // Only do this if we actually have standards
      foreach ( $standards_competencies as $key => $standard )
      {
        $competencies = [];

        // Only look for competency completion if we have competencies

        // A standard is complete if all of its artifacts are complete.
        // It's incomplete if even 1 artifact is incomplete.
        $standard_completion_status = 'complete';
        $standard_completion_percentage = 0;

        if ( count( $standard['Competencies'] ) > 0 )
        {

          $competency_count = 0;
          $competency_complete_count = 0;

          foreach ( $standard['Competencies'] as $key2 => $competency )
          {
            $competency_count++;

            /**
             * Get completion data for competency.
             * - If a competency is NOT addressed in any artifact = incomplete
             * - If a competency is addressed in an artifact = partially complete
             * - If a competency is addressed in an artifact AND has at least 1 file (documentation)
             *   AND has additional information AND has responses to ALL extra artifact
             *   questions = complete
            */
            $competency_completion_status = 'incomplete';
            $artifact = $artifacts_model->get_artifact_for_competency( $competency['Id'], $portfolio_id );

            $artifact_completion_status = HelpersPortfolio::is_artifact_complete( $artifact, $portfolio );
            if ( $artifact_completion_status == 'complete' )
            {
              $competency_completion_status = 'complete';
              $competency_complete_count++;
            } elseif ( $artifact_completion_status == 'partially-complete' ) {
              $competency_completion_status = 'partially-complete';
              $competency_complete_count = $competency_complete_count + 0.5;
            }

            $competency['Completion'] = $competency_completion_status;
            $competency['Artifact'] = $artifact;

            $competencies[] = $competency;
          }
          $standards_competencies[$key]['Competencies'] = $competencies;

          $standard_completion_decimal = $competency_complete_count / $competency_count;
          $standard_completion_percentage = $standard_completion_decimal * 100;

          if ( $standard_completion_percentage == 0 )
          {
            $standard_completion_status = 'incomplete';
          } elseif ( $standard_completion_percentage < 100 ) {
            $standard_completion_status = 'partially-complete';
          }
        } else {
          /**
           * Get completion data for standard.
           * If an artifact is attached to a standard AND has at least 1 file (documentation)
           * AND has additional information AND has responses to
           * ALL extra artifact questions = complete
           * If 1 artifact is incomplete, then the standard is incomplete.
          */

          // Get all artifacts for standard
          $artifacts_model->set_col( 'ArtifactsStandardId', $standard['StandardsId'] );
          $artifacts = $artifacts_model->get();

          if ( count( $artifacts ) > 0 )
          {
            foreach ( $artifacts as $key => $artifact )
            {
              if ( !HelpersPortfolio::is_artifact_complete( $artifact, $portfolio ) )
              {
                $standard_completion = 'incomplete';
              } else {
                $standard_completion = 'partially-complete';
                $standard_completion_percentage = 0.5;
              }
            }
          } else {
            $standard_completion = 'incomplete';
          }
        }
        // We're coming to this point having analysed 1 standard.

        if ( $standard_completion_status == 'complete' )
        {
          $standard_complete_count++;
        } else {
          $standard_complete_count += 1 * $standard_completion_decimal;
        }
        $standards_competencies[$key]['Completion'] = $standard_completion_status;
      }
      $portfolio_progress_array = $standards_competencies;

      /**
       * Total portfolio progress percentage.
       * (Complete Standards / Total Standards) * 100
       * Let's get a little more refined and also count partially complete
       * competencies
       */
      $portfolio_progress_percentage = ( $standard_complete_count / count( $standards_competencies ) ) * 100;
      $portfolio_progress_percentage = number_format( $portfolio_progress_percentage, 0 );

      // Update the database
      $data = array('PortfoliosCompletionPercentage' => $portfolio_progress_percentage);
      parent::update( $data, $portfolio_id );

      $progress_array['data'] = $portfolio_progress_array;
      $progress_array['PortfolioProgressPercentage'] = $portfolio_progress_percentage;
      $progress_array['PortfolioType'] = $portfolio;

      return $progress_array;
    } else {
      return ['PortfolioType' => $portfolio, 'data' => []];
    }
  }

  //
  // Get candidates for Portfolio Type
  //
  public function get_candidates_for_ptype( $ptype_id )
  {
    $this->set_col( 'PortfoliosPTypeId', $ptype_id);
    $this->set_order( 'UsersFirstName', 'ASC' );
    $data = $this->get();

    $new_data = [];
    $data = Helpers::objToArray( $data );
    foreach ( $data as $item )
    {
      $this->PortfolioUserLu_model->set_col( 'PortfolioUserLuPortfolioId', $item['PortfoliosId'] );
      $item['Assigned_users'] = $this->PortfolioUserLu_model->get();

      $new_data[] = $item;
    }

    return $new_data;
  }

  //
  // get_portfolio_people: get everyone associated with a portfolio
  // Candidate, Mentors, Reviewers, Admins...and format them all
  // nicely.
  //
  public function get_portfolio_people( $portfolio_id )
  {
    // Get some portfolio information.
    $portfolio = $this->get_by_id( $portfolio_id );

    // Get some portfolio assignment information
    $this->PortfolioUserLu_model->set_col( 'PortfolioUserLuPortfolioId', $portfolio_id );
    $portfolio_assigned = $this->PortfolioUserLu_model->get();

    // Get some account admin information.
    $this->AcctUsersLu_model->set_col( 'AcctUsersLuAdmin', 'Yes' );
    $this->AcctUsersLu_model->set_col( 'AcctUsersLuAcctId', Me::get_account_id() );
    $admins = $this->AcctUsersLu_model->get();

    $people = [];
    $people['Candidate'] = [];
    $people['Mentors'] = [];
    $people['Reviewers'] = [];
    $people['Admins'] = [];
    //Candidate
    array_push( $people['Candidate'], array(
      'Name' => $portfolio['UsersFirstName'] . ' ' . $portfolio['UsersLastName'],
      'UsersId' => $portfolio['UsersId'],
      'Email' => $portfolio['UsersEmail'],
      'UsersGetEmails' => $portfolio['UsersGetEmails']
    ));

    // Mentors, Reviewers
    foreach ( $portfolio_assigned as $assigned )
    {
      if ( $assigned['PortfolioUserLuUserType'] == 'Mentor' )
      {
        array_push( $people['Mentors'], array(
          'Name' => $assigned['UsersFirstName'] . ' ' . $assigned['UsersLastName'],
          'UsersId' => $assigned['UsersId'],
          'Email' => $assigned['UsersEmail'],
          'UsersGetEmails' => $assigned['UsersGetEmails']
        ));
      } else {
        array_push( $people['Reviewers'], array(
          'Name' => $assigned['UsersFirstName'] . ' ' . $assigned['UsersLastName'],
          'UsersId' => $assigned['UsersId'],
          'Email' => $assigned['UsersEmail'],
          'UsersGetEmails' => $assigned['UsersGetEmails']
        ));
      }
    }

    // Admins
    foreach ( $admins as $admin )
    {
      array_push( $people['Admins'], array(
        'Name' => $admin['UsersFirstName'] . ' ' . $admin['UsersLastName'],
        'UsersId' => $admin['UsersId'],
        'Email' => $admin['UsersEmail'],
        'UsersGetEmails' => $admin['UsersGetEmails']
      ));
    }

    return $people;
  }

  public function update_portfolio_status( $portfolio_id, $status )
  {
    DB::table('Portfolios')
    ->where('PortfoliosId', $portfolio_id)
    ->update([
      'PortfoliosStatus' => $status,
      'PortfoliosUpdatedAt' => date('Y-m-d G:i:s')
    ]);
  }

  public function update_portfolio_governance_status( $portfolio_id, $status, $user_type )
  {
    DB::table('Portfolios')
    ->where('PortfoliosId', $portfolio_id)
    ->update([
      'PortfoliosGovernanceConfirmed' . $user_type => $status,
      'PortfoliosUpdatedAt' => date('Y-m-d G:i:s')
    ]);
  }

  public function _format_get( &$data )
  {
    // Add media
    if(isset($data['MediaId']) && $data['MediaId'] > 0)
    {
      $this->Media_model->add_meta_data($data);
    } else {
      $data['MediaUrl'] = '/app/images/avatar-default.png';
    }

    // Decode PortfoliosReviewerComments
    if( isset($data['PortfoliosReviewerComments']) )
    {
      $data['PortfoliosReviewerComments'] = json_decode($data['PortfoliosReviewerComments']);
    }
  }
}

/* End File */
