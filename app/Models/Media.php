<?php

namespace App\Models;

use DB;
use Storage;

class Media extends \App\Library\Model
{
  //
  // Add Meta Data.
  //
  public function add_meta_data(&$data)
  {
    $data['MediaUrl'] = url('/api/v1/media/file/' . $data['MediaId'] . '/' . basename($data['MediaPath']));

    // Add a type
    if(isset($data['MediaType']))
    {
      switch($data['MediaType'])
      {
        case 'image/png':
        case 'image/gif':
        case 'image/jpg':
        case 'image/jpeg':
          $data['MediaTypeShort'] = 'image';
        break;

        case 'video/youtube':
        case 'video/vimeo':
          $data['MediaTypeShort'] = 'video';
        break;

        case 'embed/googledrive':
          $data['MediaTypeShort'] = 'google';
        break;

        case 'embed/dropbox':
          $data['MediaTypeShort'] = 'dropbox';
        break;

        case 'application/pdf':
          $data['MediaTypeShort'] = 'pdf';
        break;

        case 'application/zip':
        case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document':
        case 'application/msword':
        case 'application/vnd.oasis.opendocument.text':
          $data['MediaTypeShort'] = 'doc';
        break;

        case 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet':
        case 'application/vnd.ms-excel':
        case 'application/vnd.oasis.opendocument.spreadsheet':
          $data['MediaTypeShort'] = 'excel';
        break;

        case 'application/vnd.openxmlformats-officedocument.presentationml.presentation':
        case 'application/vnd.ms-powerpoint':
        case 'application/vnd.oasis.opendocument.presentation':
          $data['MediaTypeShort'] = 'power-point';
        break;
      }
    }
  }

  //
  // Delete by id.
  //
  public function delete_by_id($id)
  {
    $obj = $this->get_by_id($id);
    $rt = parent::delete_by_id($id);
    Storage::delete($obj['MediaPath']);

    // Delete Release form.
    DB::table('ReleaseMediaLu')->where('ReleaseMediaLuMediaId', $id)->delete();

    return $rt;
  }

  //
  // Format get.
  //
  public function _format_get(&$data)
  {
    $this->add_meta_data($data);
  }
}

/* End File */
