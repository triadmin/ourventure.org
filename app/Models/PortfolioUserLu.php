<?php

namespace App\Models;

use DB;
use App;
use App\Helpers\Helpers;
use App\Library\Me;

class PortfolioUserLu extends \App\Library\Model
{
  public $joins = [
    [ 'table' => 'Users', 'left' => 'UsersId', 'right' => 'PortfolioUserLuUserId' ],
    [ 'table' => 'Portfolios', 'left' => 'PortfoliosId', 'right' => 'PortfolioUserLuPortfolioId' ],
    [ 'table' => 'Media', 'left' => 'UsersAvatarId', 'right' => 'MediaId', 'type' => 'left' ]
  ];

  //
  // Construct.
  //
  public function __construct()
  {
    parent::__construct();

    // Load shared modals
    $this->Media_model = App::make('App\Models\Media');
  }

  //
  // Get candidates (portfolios) assigned to a user (mentor, reviewer)
  //
  public function get_portfolios_assigned_to_user( $userid, $user_type, $ptypeid )
  {
    $data = DB::table('PortfolioUserLu')
      ->leftJoin( 'Portfolios', 'PortfoliosId', '=', 'PortfolioUserLuPortfolioId' )
      ->leftJoin( 'Users', 'UsersId', '=', 'PortfoliosUserId' )
      ->where('PortfolioUserLuUserId', $userid)
      ->where('PortfolioUserLuUserType', $user_type)
      ->where('PortfoliosPTypeId', $ptypeid)
      ->get();
    return $data;
  }

  //
  // Format get.
  //
  public function _format_get(&$data)
  {
    // Add media
    if(isset($data['MediaId']))
    {
      $this->Media_model->add_meta_data($data);
    }
  }
}
