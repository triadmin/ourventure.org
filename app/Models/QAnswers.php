<?php

namespace App\Models;

class QAnswers extends \App\Library\Model
{
  public $joins = [
    [ 'table' => 'QTypes', 'left' => 'QTypesId', 'right' => 'QAnswersQTypesId' ]
  ];

  //
  // Insert.
  //
  public function insert($data)
  {
    // Encode values
    if(isset($data['QAnswersValues']))
    {
      $data['QAnswersValues'] = json_encode($data['QAnswersValues']);
    }
    
    return parent::insert($data);
  }

  //
  // Update.
  //
  public function update($data, $id)
  {
    // Encode values
    if(isset($data['QAnswersValues']))
    {
      $data['QAnswersValues'] = json_encode($data['QAnswersValues']);
    }
    
    return parent::update($data, $id);
  }

  //
  // Format get.
  //
  public function _format_get(&$data)
  {
    // Decode values
    if(isset($data['QAnswersValues']))
    {
      $data['QAnswersValues'] = json_decode($data['QAnswersValues'], true);
    }
  }
}

/* End File */
