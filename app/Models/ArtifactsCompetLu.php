<?php

namespace App\Models;

class ArtifactsCompetLu extends \App\Library\Model
{
  public $joins = [
    [ 'table' => 'Competencies', 'left' => 'CompetenciesId', 'right' => 'ArtifactsCompetLuCompetId' ]
  ];
}

/* End File */