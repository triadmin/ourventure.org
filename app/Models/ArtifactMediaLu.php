<?php

namespace App\Models;

class ArtifactMediaLu extends \App\Library\Model
{
  public $media_model = null;
  public $releasemedialu_model = null;  
  
  public $joins = [
    [ 'table' => 'Media', 'left' => 'MediaId', 'right' => 'ArtifactMediaLuMediaId' ],
    [ 'table' => 'Artifacts', 'left' => 'ArtifactsId', 'right' => 'ArtifactMediaLuArtifactId' ]    
  ];  
  
  //
  // Construct.
  //
  public function __construct(\App\Models\Media $media_model, \App\Models\ReleaseMediaLu $releasemedialu_model)
  {
    parent::__construct();
    $this->media_model = $media_model;
    $this->releasemedialu_model = $releasemedialu_model;
  }
  
  //
  // Delete by id
  //
  public function delete_by_id($id)
  {
    $obj = $this->get_by_id($id);
    
    // Do this first just to make sure there are no errors after.
    $rt = parent::delete_by_id($id);
    
    // Delete media
    $this->media_model->delete_by_id($obj['ArtifactMediaLuMediaId']);
    
    return $rt;
  }
  
  //
  // Format get.
  //
  public function _format_get(&$data)
  {
    // Add media
    if(isset($data['MediaId']))
    {
      $this->media_model->add_meta_data($data);
    }
    
    // Add in any Release forms.
    $this->releasemedialu_model->set_order('MediaName');
    $this->releasemedialu_model->set_col('ReleaseMediaLuArtifactDocId', $data['ArtifactMediaLuId']);
    $data['ReleaseForms'] = $this->releasemedialu_model->get();
  }
}

/* End File */