<?php

namespace App\Models;

use App;
use DB;
use App\Helpers\Helpers;
use App\Library\Me;

class MessageRecipientLu extends \App\Library\Model
{
  public $joins = [
    [ 'table' => 'Messages', 'left' => 'MessagesId', 'right' => 'MessageRecipientLuMessageId' ],
    [ 'table' => 'Users', 'left' => 'MessagesUserId', 'right' => 'UsersId' ],
    [ 'table' => 'Media', 'left' => 'UsersAvatarId', 'right' => 'MediaId', 'type' => 'left' ]
  ];

  //
  // Construct.
  //
  public function __construct()
  {
    parent::__construct();

    // Load shared modals
    $this->Media_model = App::make('App\Models\Media');
  }

  //
  // Format get.
  //
  public function _format_get(&$data)
  {
    // Add media
    if(isset($data['MediaId']))
    {
      $this->Media_model->add_meta_data($data);
    } else {
      $data['MediaUrl'] = '/app/images/avatar-default.png';
    }
  }
}

/* End File */
