<?php

namespace App\Models;

use App\Library\Me;

class Notifications extends \App\Library\Model
{
  //
  // Get.
  //
  public function get()
  {
    // Only return Notifications that are to me and in my account.
    $this->set_col( 'NotificationsUserId', Me::get_id() );
    $this->set_col( 'NotificationsAccountId', Me::get_account_id() );
    return parent::get();
  }
}

/* End File */
