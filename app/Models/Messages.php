<?php

namespace App\Models;

use App;
use DB;
use App\Helpers\Helpers;
use App\Library\Me;

class Messages extends \App\Library\Model
{
  public $joins = [
    [ 'table' => 'Users', 'left' => 'MessagesUserId', 'right' => 'UsersId' ],
    [ 'table' => 'Media', 'left' => 'UsersAvatarId', 'right' => 'MediaId', 'type' => 'left' ]
  ];

  //
  // Construct.
  //
  public function __construct()
  {
    parent::__construct();

    // Load shared modals
    $this->AcctUsersLu_model = App::make('App\Models\AcctUsersLu');
    $this->MessageRecipientLu_model = App::make('App\Models\MessageRecipientLu');
    $this->Portfolios_model = App::make('App\Models\Portfolios');
    $this->PortfolioUserLu_model = App::make('App\Models\PortfolioUserLu');
    $this->Media_model = App::make('App\Models\Media');
  }

  public function get_messages( $start = 0, $limit = 10, $artifactid = null )
  {
    // Get messages from me...
    $this->set_col( 'MessagesUserId', Me::get_id() );
    $this->set_col( 'MessagesReplyToId', 0 );
    if ( $artifactid != null )
    {
      $this->set_col( 'MessagesArtifactId', $artifactid );
    }
    $messages = $this->get();

    // Get messages to me...
    $this->MessageRecipientLu_model->set_col( 'MessageRecipientLuUserId', Me::get_id() );
    if ( $artifactid != null )
    {
      $this->MessageRecipientLu_model->set_col( 'MessagesArtifactId', $artifactid );
    }
    $messages_to_me = $this->MessageRecipientLu_model->get();
    foreach ( $messages_to_me as $m )
    {
      // Have I read this message?
      array_push( $messages, $m );
    }

    // Sort messages by MessagesCreatedAt, newest messages first.
    usort( $messages, function($a, $b) {
      $t1 = strtotime($a['MessagesCreatedAt']);
      $t2 = strtotime($b['MessagesCreatedAt']);
      return $t2 - $t1;
    });

    // ...then get any replies or other info for each message.
    $new_messages = [];
    for ( $a = 0; $a <= $limit - 1; $a++ )
    {
      if ( isset($messages[$a]) )
      {
        $message = $messages[$a];
        $this->set_col( 'MessagesReplyToId', $message['MessagesId'] );
        $this->set_order( 'MessagesCreatedAt', 'ASC' );
        $message['Replies'] = $this->get();

        $new_messages[] = $message;
      }
    }

    return $new_messages;
  }

  public function get_my_recipients( $user_id = null )
  {
    // What type of user are we?
    // This determines who our recipients are.

    // If I'm a portfolio type admin...
    if ( Me::get('AcctUsersLuAdmin') == 'Yes' )
    {
      // Everyone in the account is a potential recipient.
      $this->AcctUsersLu_model->set_col('AcctUsersLuAcctId', Me::get_account_id());
      $this->AcctUsersLu_model->set_col('UsersStatus', 'Active');
      $this->AcctUsersLu_model->set_order( 'UsersLastName', 'ASC' );
      $recipients = $this->AcctUsersLu_model->get();
      return $recipients;
    }

    // If I'm not an admin, we gotta go through the remaining user types
    // and see who my recipients are.
    $recipients = [];

    // First of all, let's get all admins as recipients.
    $this->AcctUsersLu_model->set_col('AcctUsersLuAcctId', Me::get_account_id());
    $this->AcctUsersLu_model->set_col( 'AcctUsersLuAdmin', 'Yes' );
    $this->AcctUsersLu_model->set_col('UsersStatus', 'Active');
    $this->AcctUsersLu_model->set_order( 'UsersLastName', 'ASC' );

    // Get admin recipients based on my user type.

    $admin_recipients = $this->AcctUsersLu_model->get();

    $new_recipients = [];
    foreach ( $admin_recipients as $recip )
    {
      $recip['RecipientType'] = 'Admin';
      $new_recipients[] = $recip;
    }
    $admin_recipients = $new_recipients;

    if ( Me::get('AcctUsersLuCandidate') == 'Yes' )
    {
      // Candidates can chat with their Mentor(s), but not Reviewers or Administrators.
      // 1. Which portfolios does this candidate have?
      // 2. Get list of PortfolioUserLuUserId's that are Mentors
      //    where PortfolioUserLuPortfolioId is one of the candidate
      //    portfolioId's
      // 3. Get the user info for these PortfolioUserLuUserId's.
      // All filtered by ACCOUNT_ID
      $this->Portfolios_model->set_col( 'PortfoliosUserId', $user_id );
      $this->Portfolios_model->set_col( 'PortfoliosAccountId', Me::get_account_id() );
      $portfolios = $this->Portfolios_model->get();

      foreach ( $portfolios as $portfolio )
      {
        $mentors = DB::table('PortfolioUserLu')
          ->leftJoin( 'Users', 'UsersId', '=', 'PortfolioUserLuUserId' )
          ->leftJoin( 'AcctUsersLu', 'AcctUsersLuUserId', '=', 'PortfolioUserLuUserId' )
          ->leftJoin( 'Media', 'MediaId', '=', 'UsersAvatarId' )
          ->where('PortfolioUserLuPortfolioId', $portfolio['PortfoliosId'])
          ->where('PortfolioUserLuUserType', 'Mentor')
          ->get();

        foreach ( Helpers::objToArray( $mentors ) as $mentor )
        {
          $recip = [
            'UsersId' => $mentor['UsersId'],
            'UsersFirstName' => $mentor['UsersFirstName'],
            'UsersLastName' => $mentor['UsersLastName'],
            'UsersEmail' => $mentor['UsersEmail'],
            'IsReviewer' => $mentor['AcctUsersLuReviewer'],
            'IsMentor' => $mentor['AcctUsersLuMentor'],
            'IsCandidate' => $mentor['AcctUsersLuCandidate'],
            'IsAdmin' => $mentor['AcctUsersLuAdmin'],
            'MediaId' => $mentor['MediaId'],
            'MediaUrl' => url('/api/v1/media/file/' . $mentor['MediaId'] . '/' . basename($mentor['MediaPath'])),
            'RecipientType' => 'Mentor'
          ];

          // Actually, we decided that Candidates should NOT
          // communicate with admins. So let's try emptying.
          array_push( $recipients, $recip );
        }
      }

    }

    if ( Me::get('AcctUsersLuMentor') == 'Yes' )
    {
      // Mentors can chat with their Candidates and Admins.
      // 1. Which PortfolioUsersLuPortfolioId's does this Mentor have based on PortfolioUserLuUserId?
      // 2. Get the user id's for these from the Portfolios table.
      // 3. Get the user info for these PortfoliosUserId's.
      // All filtered by ACCOUNT_ID
      $this->PortfolioUserLu_model->set_col( 'PortfolioUserLuUserId', $user_id );
      $this->PortfolioUserLu_model->set_col( 'PortfolioUserLuUserType', 'Mentor' );
      $this->PortfolioUserLu_model->set_col( 'PortfolioUserLuAccountId', Me::get_account_id() );

      // These are all the portfolio's I'm assigned to as a Mentor
      $portfolios_mentoring = $this->PortfolioUserLu_model->get();

      foreach ( $portfolios_mentoring as $portfolio )
      {
        $candidate = DB::table('Portfolios')
          ->leftJoin( 'Users', 'UsersId', '=', 'PortfoliosUserId' )
          ->leftJoin( 'AcctUsersLu', 'AcctUsersLuUserId', '=', 'PortfoliosUserId' )
          ->leftJoin( 'Media', 'MediaId', '=', 'UsersAvatarId' )
          ->where('PortfoliosId', $portfolio['PortfolioUserLuPortfolioId'])
          ->get();

        $candidate = Helpers::objToArray( $candidate[0] );

        $recip = [
          'UsersId' => $candidate['UsersId'],
          'UsersFirstName' => $candidate['UsersFirstName'],
          'UsersLastName' => $candidate['UsersLastName'],
          'UsersEmail' => $candidate['UsersEmail'],
          'IsReviewer' => $candidate['AcctUsersLuReviewer'],
          'IsMentor' => $candidate['AcctUsersLuMentor'],
          'IsCandidate' => $candidate['AcctUsersLuCandidate'],
          'IsAdmin' => $candidate['AcctUsersLuAdmin'],
          'MediaId' => $candidate['MediaId'],
          'MediaUrl' => url('/api/v1/media/file/' . $candidate['MediaId'] . '/' . basename($candidate['MediaPath'])),
          'RecipientType' => 'Candidate'
        ];
        array_push( $recipients, $recip );
      }

      // Get admin recipients
      $this->AcctUsersLu_model->set_col('AcctUsersLuAcctId', Me::get_account_id());
      $this->AcctUsersLu_model->set_col( 'AcctUsersLuAdmin', 'Yes' );
      $this->AcctUsersLu_model->set_col('AcctUsersLuAdminReceiveMessagesMentor', 'Yes');
      $this->AcctUsersLu_model->set_col('UsersStatus', 'Active');
      $this->AcctUsersLu_model->set_order( 'UsersLastName', 'ASC' );
      $admin_recipients = $this->AcctUsersLu_model->get();

      foreach ( $admin_recipients as $admin_recip )
      {
        $admin_recip['RecipientType'] = 'Admin';
        array_push( $recipients, $admin_recip );
      }
    }
    if ( Me::get('AcctUsersLuReviewer') == 'Yes' )
    {
      // Reviewers can only chat with Account Admins
      // which we got above. But, if we're also a Mentor,
      // then we got the admin recipients above.
      if ( Me::get('AcctUsersLuMentor') == 'No' )
      {
        // Get admin recipients
        $this->AcctUsersLu_model->set_col('AcctUsersLuAcctId', Me::get_account_id());
        $this->AcctUsersLu_model->set_col( 'AcctUsersLuAdmin', 'Yes' );
        $this->AcctUsersLu_model->set_col('AcctUsersLuAdminReceiveMessagesReviewer', 'Yes');
        $this->AcctUsersLu_model->set_col('UsersStatus', 'Active');
        $this->AcctUsersLu_model->set_order( 'UsersLastName', 'ASC' );
        $admin_recipients = $this->AcctUsersLu_model->get();

        foreach ( $admin_recipients as $admin_recip )
        {
          $admin_recip['RecipientType'] = 'Admin';
          array_push( $recipients, $admin_recip );
        }
      }
    }

    return $recipients;
  }

  //
  // Format get.
  //
  public function _format_get(&$data)
  {
    // Add media
    if(isset($data['MediaId']))
    {
      $this->Media_model->add_meta_data($data);
    } else {
      $data['MediaUrl'] = '/app/images/avatar-default.png';
    }
  }
}

/* End File */
