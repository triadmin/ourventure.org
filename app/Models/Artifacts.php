<?php

namespace App\Models;

use App;
use Auth;
use DB;
use App\Helpers\Helpers;
use App\Helpers\HelpersPortfolio;

class Artifacts extends \App\Library\Model
{
  public $joins = [];

  private $ArtifactsCompetLu_model = null;

  //
  // Construct.
  //
  public function __construct()
  {
    parent::__construct();

    // Load shared modals
    $this->ArtifactMediaLu_model = App::make('App\Models\ArtifactMediaLu');
    $this->ArtifactsCompetLu_model = App::make('App\Models\ArtifactsCompetLu');
    $this->PTypes_model = App::make('App\Models\PTypes');
    $this->Portfolios_model = App::make('App\Models\Portfolios');
    $this->ArtifactScores_model = App::make('App\Models\ArtifactScores');
  }

  //
  // Update.
  //
  public function update($data, $id)
  {
    // Deal with Competencies
    if(isset($data['Competencies']) && is_array($data['Competencies']))
    {
      // Delete old Competencies
      $this->ArtifactsCompetLu_model->set_col('ArtifactsCompetLuArtifactId', $id);
      foreach($this->ArtifactsCompetLu_model->get() AS $key => $row)
      {
        $this->ArtifactsCompetLu_model->delete_by_id($row['ArtifactsCompetLuId']);
      }

      // Loop through and insert to looksups
      foreach($data['Competencies'] AS $key => $row)
      {
        $this->ArtifactsCompetLu_model->insert([
          'ArtifactsCompetLuArtifactId' => $id,
          'ArtifactsCompetLuCompetId' => $row
        ]);
      }
    }

    return parent::update($data, $id);
  }

  //
  // Delete by id.
  //
  public function delete_by_id($id)
  {
    $obj = $this->get_by_id($id);
    $rt = parent::delete_by_id($id);

    // Delete ArtifactsCompetLus
    $this->ArtifactsCompetLu_model->set_col('ArtifactsCompetLuArtifactId', $id);
    foreach($this->ArtifactsCompetLu_model->get() AS $key => $row)
    {
      $this->ArtifactsCompetLu_model->delete_by_id($row['ArtifactsCompetLuId']);
    }

     // Delete ArtifactMediaLu
    $this->ArtifactMediaLu_model->set_col('ArtifactMediaLuArtifactId', $id);
    foreach($this->ArtifactMediaLu_model->get() AS $key => $row)
    {
      $this->ArtifactMediaLu_model->delete_by_id($row['ArtifactMediaLuId']);
    }

    return $rt;
  }

  //
  // Artifact for Competency
  //
  public function get_artifact_for_competency( $comp_id, $portfolio_id )
  {
    $data = DB::table('Artifacts')
            ->join('ArtifactsCompetLu', 'ArtifactsCompetLuArtifactId', '=', 'ArtifactsId')
            ->where('ArtifactsCompetLuCompetId', $comp_id)
            ->where('ArtifactsPortfolioId', $portfolio_id)
            ->get();

    if ( isset($data[0]) )
    {
      $data = $data[0];
      // Get any files too...
      $this->ArtifactMediaLu_model->set_col('ArtifactMediaLuArtifactId', $data->ArtifactsId);
      $media = $this->ArtifactMediaLu_model->get();
      $data->Media = $media;
      return Helpers::objToArray( $data );
    }

    return false;
  }

  //
  // Determine artifact completion
  //
  public function determine_artifact_completion( $artifact )
  {
    $portfolio = $this->Portfolios_model->get_by_id( $artifact['ArtifactsPortfolioId'] );

    $ptype = $this->PTypes_model->get_by_id( $portfolio['PortfoliosPTypeId'] );
    return HelpersPortfolio::is_artifact_complete( $artifact, $ptype );
  }

  // --------------------- Helper Functions -------------- //

  //
  // Format get.
  //
  public function _format_get(&$data)
  {
    // Add in related data
    if(isset($data['ArtifactsId']))
    {
      $this->ArtifactsCompetLu_model->set_col('ArtifactsCompetLuArtifactId', $data['ArtifactsId']);
      $data['Competencies'] = $this->ArtifactsCompetLu_model->get();
    }
  }
}

/* End File */
