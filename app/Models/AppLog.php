<?php

namespace App\Models;

use DB;
use App\Library\Me;

class AppLog extends \App\Library\Model
{
  //
  // Log a message into the db.
  //
  public static function log($type, $msg, $userId = null)
  {
    if(is_null($userId))
    {
      $userId = Me::get_id();
    }

    // Insert log.
    $id = DB::table('AppLog')->insertGetId([
      'AppLogAccountId' => Me::get_account_id(),
      'AppLogUserId' => $userId,
      'AppLogType' => trim($type),
      'AppLogText' => trim($msg),
      'AppLogUpdatedAt' => date('Y-m-d G:i:s'),
      'AppLogCreatedAt' => date('Y-m-d G:i:s')
    ]);

    return $id;
  }
}

/* End File */
