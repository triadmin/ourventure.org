<?php

namespace App\Library\Scoring;

use DB;
use Auth;
use App;
use App\Library\Me;

class IntervenerCertification implements PluginInterface
{
  // Scoring criteria
  // Passing is >= 3 (Proficient)
  // Failing is < 3 (None or Emerging)
  // 6/15/2017 Changed None from 0 to 1 and pushed to testing for Julie and Kristi to test.
  private $scoring_obj = [
    'ranks' => [
      'None' => 1,
      'Emerging Mastery' => 2,
      'Proficient' => 3,
      'Advanced' => 4
    ],
    'demonstration_competencies_rate' => 0.6,
    'explanation_rate' => 0.4,
    'passing_percentage' => 75
  ];

  //
  // Construct.
  //
  public function __construct()
  {
    // Load shared modals
    $this->ArtifactScores_model = App::make('App\Models\ArtifactScores');
    $this->Portfolios_model = App::make('App\Models\Portfolios');
    $this->Users_model = App::make('App\Models\Users');
  }

  //
  // We pass in an artifact id and then we score it.
  // The goal of this function is to place an entry in the ArtifactScores table.
  // $post is the data sent to the server from the scoring pop up.
  //
  public function score_artifact($ArtifactsId, $post)
  {
    // If something goes wrong just return false.
    // return false;

    // Do the scoring. Magic here.
    $score = $this->_score_artifact( $post );
    // Determine Inter-Observer Agreement based on score
    $ioa = $this->_determine_ioa_artifact( $score, $ArtifactsId );

    // Does this reviewer already have a score for this artifact?
    // If so, DELETE existing score entry.
    // -------
    DB::table('ArtifactScores')
      ->where('ArtifactScoresReviewerId', Me::get('UsersId'))
      ->where('ArtifactScoresArtifactId', $ArtifactsId)
      ->delete();

    // Then insert into ArtifactScores
    $score_id = DB::table('ArtifactScores')->insertGetId([
      'ArtifactScoresAccountId' => Me::get_account_id(),
      'ArtifactScoresPortfolioId' => $post['portfolio_id'],
      'ArtifactScoresReviewerId' => Me::get('UsersId'),
      'ArtifactScoresArtifactId' => $ArtifactsId,
      'ArtifactScoresStandardId' => $post['standard_id'],
      'ArtifactScoresBody' => json_encode( $post ),
      'ArtifactScoresScore' => $score,
      'ArtifactScoresIOA' => $ioa,
      'ArtifactScoresUpdatedAt' => date('Y-m-d G:i:s'),
      'ArtifactScoresCreatedAt' => date('Y-m-d G:i:s')
    ]);

    // Calculate overall portfolio scores.
    $this->_calculate_for_portfolio( $post['portfolio_id'] );

    // Success! (any data returned here is returned to the browser).
    return [ 'score' => $this->_format_display_score( $score ) ];
  }

  //
  // Get artifact score for me, the reviewer
  //
  public function get_score_for_artifact($ArtifactsId)
  {
    $this->ArtifactScores_model->set_col( 'ArtifactScoresArtifactId', $ArtifactsId );
    $this->ArtifactScores_model->set_col( 'ArtifactScoresReviewerId', Me::get('UsersId') );
    $this->ArtifactScores_model->set_col( 'ArtifactScoresStatus', 'Active' );
    $this->ArtifactScores_model->set_limit(1);
    $data = $this->ArtifactScores_model->get();

    // Something might happen here to munge the data before sending to client.
    if ( isset($data[0]) )
    {
      $data[0]['ArtifactScoresDisplayScore'] = $this->_format_display_score( $data[0]['ArtifactScoresScore'] );
    }

    return $data;
  }

  //
  // Get all scoring data for an artifact (scores from all reviewers)
  //
  public function get_scores_for_artifact($ArtifactsId)
  {
    $this->ArtifactScores_model->set_col( 'ArtifactScoresArtifactId', $ArtifactsId );
    $this->ArtifactScores_model->set_col( 'ArtifactScoresStatus', 'Active' );
    $this->ArtifactScores_model->set_order( 'ArtifactScoresIOA', 'ASC' );
    $data = $this->ArtifactScores_model->get();

    // Something might happen here to munge the data before sending to client.
    $new_data = [];
    foreach ( $data as $item )
    {
      $item['ArtifactScoresData'] = json_decode( $item['ArtifactScoresBody'] );

      // Convert score to percentage
      $item['ArtifactScoresScore'] = $this->_format_display_score( $item['ArtifactScoresScore'] );

      /*
       * Check for any errors in the artifact.
       */
      // 12/21/2018: Changed default of error_ioa from No to Yes
      // in order to account for the possibility of a 3rd reviewer.
      $error_ioa = 'Yes';
      $error_fail = 'No';

      // Do we have IOA?
      // If we encounter a Yes for ANY score for an artifact 
      // we can assume the artifact does have IOA (3rd reviewer problem)
      if ( $item['ArtifactScoresIOA'] == 'Yes' )
      {
        $error_ioa = 'No';
      }

      // Did we pass?
      if ( $item['ArtifactScoresScore'] < 75 )
      {
        $error_fail = 'Yes';
      }

      $item['ArtifactScoresErrorIOA'] = $error_ioa;
      $item['ArtifactScoresErrorFail'] = $error_fail;

      $new_data[] = $item;
    }

    return $new_data;
  }

  //
  // Get all scoring data for a portfolio (scores from all reviewers)
  //
  public function get_scores_for_portfolio($portfolio_id)
  {
    $this->ArtifactScores_model->set_col( 'ArtifactScoresPortfolioId', $portfolio_id );
    $this->ArtifactScores_model->set_col( 'ArtifactScoresStatus', 'Active' );
    $data = $this->ArtifactScores_model->get();

    return $data;
  }

  //
  // Calculate the final score and IOA for a portfolio
  //
  public function _calculate_for_portfolio( $PortfolioId )
  {
    $this->ArtifactScores_model->set_col( 'ArtifactScoresPortfolioId', $PortfolioId );
    $this->ArtifactScores_model->set_order( 'ArtifactScoresStandardId', 'ASC' );
    $scores = $this->ArtifactScores_model->get();

    $standard_id = 0;
    $standard_sum_score = 0;
    $standard_artifact_count = 0;
    $standard_fail_array = [];
    $portfolio_sum_score = 0;
    $score_count = 0;
    $ioa_sum_total = 0;
    $failed_standard = false;

    foreach ( $scores as $score )
    {
      if ( $score['ArtifactScoresStandardId'] != $standard_id )
      {
        // Did we pass the last standard?
        if ( $standard_id > 0 )
        {
          // Get an average of all artifact scores for this standard (Average Standard Score)
          $standard_score = $standard_sum_score / $standard_artifact_count;

          // If our Average Standard Score is < Proficient, we have failed the Standard
          if ( $standard_score < $this->scoring_obj['ranks']['Proficient'] )
          {
            $standard_fail_array[] = $standard_id;
            $failed_standard = true;
          }
          $standard_sum_score = 0;
          $standard_artifact_count = 0;
        }
        $standard_id = $score['ArtifactScoresStandardId'];
      }

      // Portfolio sum score.
      // Jeff Edit 1/19/2017: Moved this line into the if statement below.
      // With the line up here, we were calculating overall portfolio
      // score incorrectly.
      // $portfolio_sum_score += $score['ArtifactScoresScore'];

      // Standard sum score.
      // ONLY COUNT SCORES WITH IOA = YES
      if ( $score['ArtifactScoresIOA'] == 'Yes' )
      {
        // Add to standard score sum.
        $standard_sum_score += $score['ArtifactScoresScore'];

        // Add to portfolio score sum.
        $portfolio_sum_score += $score['ArtifactScoresScore'];

        // And increment score count.
        $score_count++;
        // And increment ioa total.
        $ioa_sum_total++;
      }

      $standard_artifact_count++;
    }

    // $portfolio_sum_score is a decimal like 3.2, 4.0
    if ( $score_count > 0 )
    {
      $portfolio_ave_score = number_format(( $portfolio_sum_score / $score_count ), 1);
    } else {
      $portfolio_ave_score = number_format( $portfolio_sum_score, 1 );
    }

    $portfolio_score_percentage = number_format (($portfolio_ave_score / $this->scoring_obj['ranks']['Advanced']) * 100, 0);
    $portfolio_ave_score_text = $this->_determine_score_rank( $portfolio_ave_score );

    // TODO: This is a problem when we have 3 reviewers because we have one
    //       extra score to deal with.
    // 12/21/2018: Look here again for determining ioa percentage.  It MAY not be working right with
    // 3rd reviewer.
    $ioa_percentage = ( $ioa_sum_total / count($scores) ) * 100;

    $pass_fail = 'Pass';

    // If our total portfolio score is < Proficient, we have failed.
    // 1/23/2018: Per discussion on Dec 15, 2017, removed the requirement for passing all standards
    //            to have a passing portfolio. Removed || $failed_standard == true from the conditional below.
    // See Asana task https://app.asana.com/0/9239516261531/527734316990832 for details.
    if ( $portfolio_ave_score < $this->scoring_obj['ranks']['Proficient'] )
    {
      $pass_fail = 'Fail';
    }

    // Update the Portfolios table
    DB::table('Portfolios')
    ->where('PortfoliosId', $PortfolioId )
    ->update([
      'PortfoliosScore' => $portfolio_score_percentage,
      'PortfoliosScoreText' => $portfolio_ave_score_text,
      'PortfoliosPassFail' => $pass_fail,
      'PortfoliosFailedStandards' => json_encode( $standard_fail_array ),
      'PortfoliosIOAPercentage' => $ioa_percentage,
      'PortfoliosUpdatedAt' => date('Y-m-d G:i:s')
    ]);
  }

  //
  // Determine artifact IOA
  // IOA is always calculated for each artifact based on the LAST reviewer. 
  // In the case of 3 or more reviewers, if the last reviewer score has IOA with any one 
  // of the previous scores, the artifact has IOA.
  // 12/21/2018: Changed the way a 3rd reviewer IOA is determined.
  // When a 3rd reviewer scores a portfolio, if there is already IOA for an artifact between the 1st and 2nd reviewer, 
  // we don't count the 3rd reviewer's score.
  //
  public function _determine_ioa_artifact( $score, $ArtifactsId )
  {
    $ioa = 'No';

    // See if we have another score for this artifact THAT ISN'T MY SCORE (if I'm a reviewer).
    $data = DB::table('ArtifactScores')
      ->where( 'ArtifactScoresReviewerId', '<>', Me::get('UsersId') )
      ->where( 'ArtifactScoresArtifactId', $ArtifactsId )
      ->where( 'ArtifactScoresStatus', 'Active' )
      ->get();

    if ( count( $data ) > 0 )
    {
      // If so, we need to determine IOA.  If not, then fuggetaboutit.
      // We have IOA if both reviewers give the artifact a passing (Proficient or Advanced)
      // or failing (None or Emerging) score.
      // So the Proficient low score is the cutoff point.
      $my_score = $score;

      foreach ( $data as $d )
      {
        $their_score = $d->ArtifactScoresScore;

        // If their score AND my score are BOTH either Passing (Proficient or Advanced) or Failing (None or Emerging),
        // we have IOA.
        if (
          (
            $my_score >= $this->scoring_obj['ranks']['Proficient'] &&
            $their_score >= $this->scoring_obj['ranks']['Proficient']
          ) || (
            $my_score < $this->scoring_obj['ranks']['Proficient'] &&
            $their_score < $this->scoring_obj['ranks']['Proficient']
          )
        ) {
          $ioa = 'Yes';

          // Update their score IOA data.
          DB::table('ArtifactScores')
          ->where('ArtifactScoresId', $d->ArtifactScoresId )
          ->update([
            'ArtifactScoresIOA' => 'Yes',
            'ArtifactScoresUpdatedAt' => date('Y-m-d G:i:s')
          ]);

          // We got what we're looking for. Let's go home.
          // But first, let's set any non-IOA scores to DISABLED because we only
          // care about scores that have IOA.
          // NOT SURE WE NEED THIS.
          /*
          DB::table('ArtifactScores')
          ->where( 'ArtifactScoresArtifactId', $ArtifactsId )
          ->where( 'ArtifactScoresIOA', 'No' )
          ->update([
            'ArtifactScoresStatus' => 'Inactive',
            'ArtifactScoresUpdatedAt' => date('Y-m-d G:i:s')
          ]);
          */

          break;
        } else {
          // Update their score IOA data to NO.
          // Because we need BOTH artifact scores to be NO if they
          // are out of IOA
          DB::table('ArtifactScores')
          ->where('ArtifactScoresId', $d->ArtifactScoresId )
          ->update([
            'ArtifactScoresIOA' => 'No',
            'ArtifactScoresUpdatedAt' => date('Y-m-d G:i:s')
          ]);
        }
      }
    }
    return $ioa;
  }

  // ***** HELPER METHODS *****/

  //
  // Score artifact returns the number score for an individual
  // artifact (e.g. 3.60, 2.00, etc.)
  //
  public function _score_artifact( $post )
  {
    $scoring_obj = $this->scoring_obj;

    $demonstration_competencies_score = $scoring_obj['ranks'][$post['demonstration_competencies']] * $scoring_obj['demonstration_competencies_rate'];
    $explanation_score = $scoring_obj['ranks'][$post['explanation']] * $scoring_obj['explanation_rate'];

    $score = number_format( $demonstration_competencies_score + $explanation_score, 1 );

    return $score;
  }

  //
  // Determine score rank returns the rank text (e.g. None, Emerging Mastery, etc.)
  // based on a number score.
  //
  public function _determine_score_rank( $score )
  {
    if ( $score >= $this->scoring_obj['ranks']['None'] && $score < $this->scoring_obj['ranks']['Emerging Mastery'] )
    { return 'None'; }

    if ( $score >= $this->scoring_obj['ranks']['Emerging Mastery'] && $score < $this->scoring_obj['ranks']['Proficient'] )
    { return 'Emerging Mastery'; }

    if ( $score >= $this->scoring_obj['ranks']['Proficient'] && $score < $this->scoring_obj['ranks']['Advanced'] )
    { return 'Proficient'; }

    if ( $score >= $this->scoring_obj['ranks']['Advanced'] )
    { return 'Advanced'; }
  }

  //
  // Format display score
  //
  public function _format_display_score( $score )
  {
    // Convert score to percentage
    $score_percentage = number_format(($score / $this->scoring_obj['ranks']['Advanced']) * 100, 0);

    $display_score = $score_percentage . '% ' . $this->_determine_score_rank( $score );
    return $display_score;
  }

  //
  // Determine portfolio Pass/Fail.  If all Standards pass, then portfolio
  // passes.  If 1 standard does not pass with Proficient or Advanced, then
  // the portfolio fails and the candidate needs to improve the failed standards.
  //
  public function _determine_pass_fail( $scores )
  {

  }
}

/* End File */
