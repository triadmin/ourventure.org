<?php

namespace App\Helpers;

use DB;
use Auth;

//
// Some general helper methods.
//
class Helpers
{
  public static function objToArray($o)
  {
    $a = array();
    foreach ($o as $k => $v)
      $a[$k] = (is_array($v) || is_object($v)) ? Helpers::objToArray($v): $v;

    return $a;
  }
}
