<?php

namespace App\Helpers;

use DB;
use Auth;
use App;

//
// Some helper methods used with portfolios.
//
class HelpersPortfolio
{
  /**
   * is_artifact_complete: determines if an artifact is complete.
   * @param  array $artifact Complete artifact array with files and
   *     extra questions.
   * @return boolean $artifact_complete
   */
  public static function is_artifact_complete( $artifact, $portfolio )
  {
    // Models we're gonna need
    $qtypes = App::make('App\Models\QTypes');
    $qanswers = App::make('App\Models\QAnswers');

    // Default status
    $artifact_complete = 'incomplete';

    // Does this pType have any qTypes?
    $qtypes->set_col( 'QTypesWhich', 'Artifacts' );
    $qtypes->set_col( 'QTypesPTypesId', $portfolio['PTypesId'] );
    $ptype_questions = $qtypes->get();

    if ( empty( $artifact ) )
    {
      return $artifact_complete;
    } else {
      // Documentation (required for completion)
      $media_complete = false;

      // We decided to remove Additional Information from the
      // completion algorithm. So, we're setting this variable to true.
      // If we need to include Additional Information, set this to 'false'.
      $additional_information_complete = true;

      // Questions and Prompts - not required for completion by default
      // because there may not be any.
      $questions_complete = true;

      // Do we have at least 1 document?
      if ( isset($artifact['Media']) && count($artifact['Media']) > 0 )
      {
        $media_complete = true;
      }

      // Do we have content for Additional Information?
      if ( !empty( $artifact['ArtifactsBody'] ) )
      {
        $additional_information_complete = true;
      }

      // If this ptype has additional questions and prompts, we
      // need to go a step further in checking for completeness.
      if ( count( $ptype_questions ) > 0 )
      {
        // NOTE: At this point we have no way of know whether questions impact
        // completness because we don't know if any questions are required.
        // Therefore, $questions_complete = true;

        // See if we have any answers for this artifact.
        $qanswers->set_col( 'QAnswersArtifactsId', $artifact['ArtifactsId'] );
        $answers = $qanswers->get();

        if ( count( $answers ) > 0 )
        {
          $answered = 0;
          $required_questions_answered = true;

          foreach ( $answers as $answer )
          {
            // Does this ptype allow for a video response?
            // If so, then let's mark questions complete if we find a video.
            if (
              $portfolio['PTypesAllowVideo'] == 'Yes' &&
              $answer['QTypesType'] == 'video' &&
              !empty( $answer['QAnswersValues']['value'] )
            )
            {
              $questions_complete = true;
              break;
            }

            // Otherwise, let's see if all of our required questions
            // have been answered. A question is answered if [other] is not empty,
            // or if [value] is not empty, or if at least 1 [values] is Yes
            $is_answered = false;

            // If a question has a VALUE and DOES NOT have any VALUES[], then
            // it can be considered complete.
            if (
              (
                !empty( $answer['QAnswersValues']['value'] ) &&
                 count( $answer['QAnswersValues']['values'] ) == 0
              ) ||
              !empty( $answer['QAnswersValues']['other']
            ))
            {
              $is_answered = true;
            }

            // Questions with VALUES[] will ALWAYS have a VALUE. We
            // need to do some acrobatics to accurately check for a completed
            // question in this case.
            if ( isset( $answer['QAnswersValues']['values'] ) )
            {
              foreach ( $answer['QAnswersValues']['values'] as $val )
              {
                if ( $val == 'Yes' )
                {
                  // We've answered the questions. Let's go.
                  $is_answered = true;
                  break;
                }
              }
            }

            // If the question is Required but not answered.
            if ( $answer['QTypesRequired'] == 'Yes' && $is_answered == false )
            {
              $required_questions_answered = false;
              $questions_complete = false;
            }

            // Increment number of questions answered.
            if ( $is_answered == true )
            {
              $answered++;
            }

            // Have we reached our answer minumum?
            if (
              $portfolio['PTypesMinimumPrompts'] > 0 &&
              $answered < $portfolio['PTypesMinimumPrompts']
            )
            {
              $minimum_required_met = false;
            } else {
              $minimum_required_met = true;
            }

            //echo 'QAnswersId ' . $answer['QAnswersId'] . ' is required: ' . $answer['QTypesRequired'] . Chr(10);
            //echo 'QAnswersId ' . $answer['QAnswersId'] . ' is answered: ' . $is_answered . Chr(10) . '--------' . Chr(10);

          } // end foreach

          if ( $minimum_required_met == true && $required_questions_answered == true )
          {
            $questions_complete = true;
          }
        }
      }

      // Now, what's our status?
      /*
      print_r( array(
        '$media_complete' => $media_complete, '
        $additional_information_complete' => $additional_information_complete, '
        $questions_complete' => $questions_complete,
        '$minimum_required_met' => $minimum_required_met,
        '$required_questions_answered' => $required_questions_answered
      ) );
      */

      if ( $media_complete == true && $additional_information_complete == true && $questions_complete == true )
      {
        $artifact_complete = 'complete';
      } elseif ( !$media_complete && !$additional_information_complete && !$questions_complete ) {
        $artifact_complete = 'incomplete';
      } else {
        $artifact_complete = 'partially-complete';
      }
    }

    return $artifact_complete;
  }
}
