<?php

namespace App\Http\Controllers\Api\V1;

use DB;
use Auth;
use App\Library\Me;
use App;
use Mail;
use Hash;
use Validator;

class ReleaseMediaLu extends \App\Library\ApiController
{
	public $validation_create = [];
	public $validation_update = [];
	public $validation_message = [];

/*
  //
  // Construct.
  //
  public function __construct()
  {
    parent::__construct();

    // Load shared modals
    $this->ArtifactMediaLu_model = App::make('App\Models\ArtifactMediaLu');
  }

  //
  // save_files: save files to artifact from file modal window
  //
  public function save_files()
  {
    $files = $this->request->input('Files');
    if ( count($files) > 0 )
    {
      foreach ( $files as $f )
      {
        $this->ArtifactMediaLu_model->insert([
          'ArtifactMediaLuAccountId' => Me::get_account_id(),
          'ArtifactMediaLuArtifactId' => $f['ArtifactId'],
          'ArtifactMediaLuMediaId' => $f['MediaId'],
          'ArtifactMediaLuUpdatedAt' => date('Y-m-d G:i:s'),
          'ArtifactMediaLuCreatedAt' => date('Y-m-d G:i:s')
        ]);
      }
    }
  }

  //
  // remove - remove media from an artifact but don't DELETE the file.
  //
  public function remove( $media_lu_id )
  {
    DB::table('ArtifactMediaLu')->where('ArtifactMediaLuId', $media_lu_id)->delete();
  }

	//
  // move - change the order of the media file.
  //
  public function update_order()
  {
		$documents = $this->request->input('Documents');
		print_r($documents);
    foreach ( $documents as $key => $doc )
    {
      DB::table('ArtifactMediaLu')->where('ArtifactMediaLuId', $doc['ArtifactMediaLuId'])->update(['ArtifactMediaLuOrder' => $key+1]);
    }
  }
*/
}

/* End File */
