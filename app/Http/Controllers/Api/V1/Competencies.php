<?php 

namespace App\Http\Controllers\Api\V1;

use DB;
use Auth;
use App;
use Mail;
use Hash;
use Validator;
	
class Competencies extends \App\Library\ApiController
{	
  public $validation_create = [
  'CompetenciesTitle' => 'required'
  ];

  public $validation_update = [
  'CompetenciesTitle' => 'required'
  ];

  public $validation_message = [];

  //
  // Construct.
  //
  public function __construct()
  {
    parent::__construct();

    // Load shared modals
    $this->Competencies_model = App::make('App\Models\Competencies');
    $this->ArtifactsCompetLu_model = App::make('App\Models\ArtifactsCompetLu');
  }

  //
  // get_competencies_for_artifact_edit - get list of ptype competencies
  // that have not been used in portfolio_id yet.
  //
  public function get_competencies_for_artifact_edit( $ptype_id, $portfolio_id )
  {
    // Get all competencies for this ptype_id
    $this->Competencies_model->set_col( 'StandardsPTypesId', $ptype_id );
    $this->Competencies_model->set_order( 'CompetenciesTitle', 'ASC' );
    $competencies = $this->Competencies_model->get();

    // Now, get all competencies we've already used.
    $this->ArtifactsCompetLu_model->set_join( 'Artifacts', 'ArtifactsId', 'ArtifactsCompetLuArtifactId' );
    $this->ArtifactsCompetLu_model->set_col( 'ArtifactsPortfolioId', $portfolio_id );
    $used = $this->ArtifactsCompetLu_model->get();

    $new_data = [];
    foreach( $competencies as $c )
    {
      $add_comp = true;
      foreach( $used as $u )
      {
        if ( $c['CompetenciesId'] == $u['CompetenciesId'])
        {
          $add_comp = false;
        }
      }
      if ( $add_comp == true )
      {
        $new_data[] = $c;
      }
    }
    return $this->api_response( $new_data );
  }

  public function update_order()
  {
    $competencies = $this->request->input('Competencies');
    foreach ( $competencies as $key => $comp )
    {
      DB::table('Competencies')->where('CompetenciesId', $comp['CompetenciesId'])->update([
      'CompetenciesOrder' => $key+1]);
    }
  }
}

/* End File */
