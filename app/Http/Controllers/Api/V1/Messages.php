<?php

namespace App\Http\Controllers\Api\V1;

use DB;
use Auth;
use App;
use Mail;
use Hash;
use Validator;
use App\Library\Me;

class Messages extends \App\Library\ApiController
{
  //
  // Construct.
  //
  public function __construct()
  {
    parent::__construct();

    // Load shared modals
    $this->Users_model = App::make('App\Models\Users');
    $this->Messages_model = App::make('App\Models\Messages');
    $this->Artifacts_model = App::make('App\Models\Artifacts');
    $this->MessageRecipientLu_model = App::make('App\Models\MessageRecipientLu');
    $this->PortfolioUserLu_model = App::make('App\Models\PortfolioUserLu');
  }

  /**
   * get_my_recipients: get all possible messages recipients for
   * a given user_id
   */
  public function get_my_recipients()
  {
    $recipients = $this->Messages_model->get_my_recipients( Me::get_id() );
    return $this->api_response( $recipients );
  }

  /**
   * send: add a message to the database
   */
  public function send()
  {
    $message_id = DB::table('Messages')->insertGetId([
      'MessagesAccountId' => Me::get_account_id(),
      'MessagesPTypeId' => $this->request->input('MessagesPTypeId'),
      'MessagesArtifactId' => $this->request->input('MessagesArtifactId'),
      'MessagesUserId' => Me::get_id(),
      'MessagesTitle' => $this->request->input('MessagesTitle'),
      'MessagesBody' => $this->request->input('MessagesBody'),
      'MessagesReplyToId' => $this->request->input('MessagesReplyToId'),
      'MessagesToString' => $this->request->input('MessagesToString'),
      'MessagesUpdatedAt' => date('Y-m-d G:i:s'),
      'MessagesCreatedAt' => date('Y-m-d G:i:s')
    ]);

    $sender = [
      'UsersName' => Me::get('UsersFirstName') . ' ' . Me::get('UsersLastName'),
      'UsersEmail' => Me::get('UsersEmail')
    ];

    // Insert message recipients
    foreach ( $this->request->input('MessageRecipients') as $recip )
    {
      DB::table('MessageRecipientLu')->insertGetId([
        'MessageRecipientLuAccountId' => Me::get_account_id(),
        'MessageRecipientLuMessageId' => $message_id,
        'MessageRecipientLuUserId' => $recip['UsersId'],
        'MessageRecipientLuUpdatedAt' => date('Y-m-d G:i:s'),
        'MessageRecipientLuCreatedAt' => date('Y-m-d G:i:s')
      ]);

      $user = $this->Users_model->get_by_id( $recip['UsersId'] );

      $mailing = [
        'user' => $user,
        'sender' => $sender,
        'message_text' => $this->request->input('MessagesBody')
      ];

      // Send email to recipient
      if ( $user['UsersGetEmails'] == 'Yes' )
      {
        Mail::send('emails.send-message', $mailing, function ($message) use ($user) {
          $message->from('support@nationaldb.org', 'Portfolio Support');
          $message->to($user['UsersEmail']);
          $message->subject('You received a message');
        });
      }  
    }
  }

  //
  // mark_as_read
  //
  public function mark_as_read()
  {
    DB::table('MessageRecipientLu')
      ->where( 'MessageRecipientLuUserId', $this->request->input('UsersId') )
      ->where( 'MessageRecipientLuMessageId', $this->request->input('MessagesId') )
      ->update([
        'MessageRecipientLuHasRead' => 'Yes',
        'MessageRecipientLuUpdatedAt' => date('Y-m-d G:i:s')
      ]);
  }

  /**
   * get_messages: get messages for user
   */
  public function get_messages( $start = 0, $limit = 10)
  {
    // Get messages that I sent with replies
    // Get messages sent to me with replies
    // If message references an artifact, get the artifact data too
    $artifactid = null;
    if ( $this->request->input('artifactid') > 0 )
    {
      $artifactid = $this->request->input('artifactid');
    }
    $messages = $this->Messages_model->get_messages( $start, $limit, $artifactid );

    $new_data = [];
    foreach ( $messages as $row )
    {
      $row['Artifact'] = $this->Artifacts_model->get_by_id( $row['MessagesArtifactId'] );
      $new_data[] = $row;
    }

    return $this->api_response( $new_data );
  }
}

/* End File */
