<?php

namespace App\Http\Controllers\Api\V1;

use DB;
use Auth;
use App;
use Mail;
use Hash;
use Validator;

class QAnswers extends \App\Library\ApiController
{
	public $validation_create = [];
	public $validation_update = [];
	public $validation_message = [];

  //
  // Create and / or Get.
  //
  public function get_create()
  {
    // Setup query. Apply any filters we might have passed in.
    $this->_setup_query();

    // Load model and run the query.
    $data = $this->model->get();

    // Create if now data
    if(count($data) < 1)
    {
      // Create
      $this->create();

      // Refresh data
      $this->_setup_query();
      $data = $this->model->get(); 
    }

    return $this->api_response($data[0]);
  }
}

/* End File */
