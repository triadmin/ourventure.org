<?php

namespace App\Http\Controllers\Api\V1;

use DB;
use Auth;
use App;
use Mail;
use Hash;
use Validator;
use App\Library\Me;

class Artifacts extends \App\Library\ApiController
{
	public $validation_create = [];
	public $validation_update = [];
	public $validation_message = [];

  //
  // Construct.
  //
  public function __construct()
  {
    parent::__construct();

    // Load shared modals
    $this->ArtifactMediaLu_model = App::make('App\Models\ArtifactMediaLu');
    $this->Artifacts_model = App::make('App\Models\Artifacts');
    $this->Portfolios_model = App::make('App\Models\Portfolios');
    $this->PortfolioUserLu_model = App::make('App\Models\PortfolioUserLu');
    $this->AcctUsersLu_model = App::make('App\Models\AcctUsersLu');
    $this->Notifications_model = App::make('App\Models\Notifications');
    $this->PTypes_model = App::make('App\Models\PTypes');
  }

	//
	// get artifact by id
	//
	public function id( $artifact_id, $plugin_name = null )
	{
		$artifact = $this->Artifacts_model->get_by_id( $artifact_id );

		// Get media
		$this->ArtifactMediaLu_model->set_col('ArtifactMediaLuArtifactId', $artifact['ArtifactsId']);
		$media = $this->ArtifactMediaLu_model->get();
		$artifact['Media'] = $media;

		// Get artifact scoring data
		$artifact['Scores'] = [];
		if ( $plugin_name != null )
		{
			$class = 'App\Library\Scoring\\' . $plugin_name;
	    $scoring = App::make($class);
			$data = $scoring->get_scores_for_artifact($artifact_id);
			$artifact['Scores'] = $data;
		}

		// Determine artifact completion
		$artifact['Completion'] = $this->Artifacts_model->determine_artifact_completion( $artifact );
		return $this->api_response( $artifact );
	}

	//
  // Get artifacts for standard
  //
  public function get_artifacts_for_standard( $portfolioid, $standardid )
  {
    $this->Artifacts_model->set_col( 'ArtifactsStandardId', $standardid );
    $this->Artifacts_model->set_col( 'ArtifactsPortfolioId', $portfolioid );
    $artifacts = $this->Artifacts_model->get();

		// Get portfolio data
		$portfolio = $this->Portfolios_model->get_by_id( $portfolioid );
		$ptype = $this->PTypes_model->get_by_id( $portfolio['PortfoliosPTypeId'] );
		$plugin_name = $ptype['PTypesScoringPlugin'];

    $new_data = [];
    foreach ( $artifacts as $artifact )
    {
      $this->ArtifactMediaLu_model->set_col('ArtifactMediaLuArtifactId', $artifact['ArtifactsId']);
      $media = $this->ArtifactMediaLu_model->get();
      $artifact['Media'] = $media;

			// Determine artifact completion
			$artifact['Completion'] = $this->Artifacts_model->determine_artifact_completion( $artifact );

			// Get artifact scoring data
      $artifact['Scores'] = [];
      $artifact['ScoringError'] = 'No';
			if ( $plugin_name != null )
			{
				$class = 'App\Library\Scoring\\' . $plugin_name;
		    $scoring = App::make($class);
				$data = $scoring->get_scores_for_artifact($artifact['ArtifactsId']);
        $artifact['Scores'] = $data;

        foreach ( $artifact['Scores'] as $score )
        {
          if ( $score['ArtifactScoresErrorFail'] == 'Yes' )
          {
            $artifact['ScoringError'] = 'Yes';
            break;
          }
        }

        //print_r($artifact['Scores']);
			}

      $new_data[] = $artifact;
    }

    return $this->api_response( $new_data );
  }

  //
  // override create method so we can get a count
  // of existing artifacts on create. This is so we
  // can send a notification to account admins that a user
  // has started their portfolio.
  //
  public function create()
  {
    $portfolioid = $this->request->input('ArtifactsPortfolioId');
    $this->Artifacts_model->set_col( 'ArtifactsPortfolioId', $portfolioid );
    $artifacts = $this->Artifacts_model->get();

		// If this is our first artifct, alert mentors and admins that
		// candidate has started their portfolio.
    if ( count($artifacts) == 0 )
    {
      // Get mentors
      $this->PortfolioUserLu_model->set_col( 'PortfolioUserLuPortfolioId', $portfolioid );
      $this->PortfolioUserLu_model->set_col( 'PortfolioUserLuUserType', 'Mentor' );
      $mentors = $this->PortfolioUserLu_model->get();

      // Get admins
      $this->AcctUsersLu_model->set_col( 'AcctUsersLuAcctId', Me::get_account_id() );
      $this->AcctUsersLu_model->set_col( 'AcctUsersLuAdmin', 'Yes' );
      $admins = $this->AcctUsersLu_model->get();

			// Get portfolio information
      $this->Portfolios_model->set_col( 'PortfoliosId', $portfolioid );
      $portfolio = $this->Portfolios_model->get();

      $message = '<b>' . Me::get('UsersFirstName') . ' ' . Me::get('UsersLastName') . '</b> has started their portfolio for <b>' . $portfolio[0]['PTypesTitle'] . '</b>.';

			// Enter notification entries for everyone
      $recipients = array_merge( $mentors, $admins );
      foreach ( $recipients as $recip )
      {
        $this->Notifications_model->insert([
          'NotificationsAccountId' => Me::get_account_id(),
          'NotificationsUserId' => $recip['UsersId'],
          'NotificationsTitle' => 'Portfolio Started',
          'NotificationsBody' => $message,
          'NotificationsType' => 'portfolio-started',
          'NotificationsUpdatedAt' => date('Y-m-d G:i:s'),
          'NotificationsCreatedAt' => date('Y-m-d G:i:s')
        ]);
      }

			// Update our portfolio status
	    $this->Portfolios_model->update_portfolio_status( $portfolioid, 'In Progress' );
    }

		// Save the artifact entry
    $artifact_id = DB::table('Artifacts')->insertGetId([
      'ArtifactsAccountId' => Me::get_account_id(),
      'ArtifactsPortfolioId' => $this->request->input('ArtifactsPortfolioId'),
      'ArtifactsStandardId' => $this->request->input('ArtifactsStandardId'),
      'ArtifactsTitle' => $this->request->input('ArtifactsTitle'),
      'ArtifactsUpdatedAt' => date('Y-m-d G:i:s'),
      'ArtifactsCreatedAt' => date('Y-m-d G:i:s')
    ]);

		// Return happiness
    return $this->api_response([ 'ArtifactsId' => $artifact_id ]);
  }

  //
  // Get media for an artifact by ArtifactId
  //
  public function get_media($id)
  {
    $this->ArtifactMediaLu_model->set_col('ArtifactMediaLuArtifactId', $id);
		$this->ArtifactMediaLu_model->set_order('ArtifactMediaLuOrder', 'asc');
    $media = $this->ArtifactMediaLu_model->get();
    return $this->api_response($media);
  }

  //
  // Add media
  //
  public function add_media(\Illuminate\Http\Request $request)
  {
    $media_id = $request->input('MediaId');

    // Add the media to the look up table.
    $id = $this->ArtifactMediaLu_model->insert([
      'ArtifactMediaLuArtifactId' => $request->input('ArtifactsId'),
      'ArtifactMediaLuMediaId' => $request->input('MediaId')
    ]);

    // Return happy.
    return $this->api_response([ 'Id' => $id ]);
  }

}

/* End File */
