<?php

namespace App\Http\Controllers\Api\V1;

use DB;
use Auth;
use App;
use Mail;
use App\Library\Me;
use App\Helpers\HelpersPortfolio;

class ArtifactScores extends \App\Library\ApiController
{
  //
  // Construct.
  //
  public function __construct()
  {
    parent::__construct();

    // Load shared modals
    $this->ArtifactScores_model = App::make('App\Models\ArtifactScores');
    $this->Artifacts_model = App::make('App\Models\Artifacts');
    $this->Portfolios_model = App::make('App\Models\Portfolios');
    $this->Standards_model = App::make('App\Models\Standards');
    $this->PTypes_model = App::make('App\Models\PTypes');
    $this->PortfolioUserLu_model = App::make('App\Models\PortfolioUserLu');
    $this->AcctUsersLu_model = App::make('App\Models\AcctUsersLu');
    $this->Notifications_model = App::make('App\Models\Notifications');
  }

  //
  // Score artifact
  //
  public function score($ArtifactsId)
  {
    $class = 'App\Library\Scoring\\' . $this->request->input('plugin_name');
    $scoring = App::make($class);

    // Update portfolio status to In Review
    $portfolio_id = $this->request->input('portfolio_id');
    $this->Portfolios_model->update_portfolio_status( $portfolio_id, 'In Review' );

    // Score artifact
    if(! $score = $scoring->score_artifact($ArtifactsId, request()->all()))
    {
      return $this->api_response(null, 0);
    }
    $result = $score;
    return $this->api_response( $result );
  }

  //
  // Get scoring data for Artifact, for me the reviewer.
  //
  public function get_score_for_artifact($ArtifactsId, $plugin_name)
  {
    $class = 'App\Library\Scoring\\' . $plugin_name;
    $scoring = App::make($class);

    $data = $scoring->get_score_for_artifact($ArtifactsId);
    return $this->api_response( $data );
  }

  //
  // Get all scores for an artifact.
  //
  public function get_scores_for_artifact($ArtifactsId, $plugin_name)
  {
    $class = 'App\Library\Scoring\\' . $plugin_name;
    $scoring = App::make($class);

    $data = $scoring->get_scores_for_artifact($ArtifactsId);
    return $this->api_response( $data );
  }

  //
  // See if portfolio has any scores.
  //
  public function check_for_scores( $PortfolioId )
  {
    $this->ArtifactScores_model->set_col( 'ArtifactScoresPortfolioId', $PortfolioId );
    $this->ArtifactScores_model->set_col( 'ArtifactScoresStatus', 'Active' );
    $data = $this->ArtifactScores_model->get();
    return $this->api_response( $data );
  }

  //
  // get_scores_for_portfolio
  //
  public function get_scores_for_portfolio( $portfolio_id, $ptypes_id )
  {
    // Get PType information
    $ptype = $this->PTypes_model->get_by_id( $ptypes_id );
    $plugin_name = $ptype['PTypesScoringPlugin'];

    $class = 'App\Library\Scoring\\' . $plugin_name;
    $scoring = App::make($class);

    $this->Standards_model->set_col( 'StandardsPTypesId', $ptypes_id );
    $this->Standards_model->set_order( 'StandardsOrder', 'ASC' );
    $standards = $this->Standards_model->get();

    $new_data = [];

    // Loop over each standard.
    foreach ( $standards as $key => $standard )
    {
      // Get artifacts for this standard
      $this->Artifacts_model->set_col( 'ArtifactsStandardId', $standard['StandardsId'] );
      $this->Artifacts_model->set_col( 'ArtifactsPortfolioId', $portfolio_id );
      $artifacts = $this->Artifacts_model->get();

      $new_artifacts = [];
      $standard['ScoringRatio'] = [];
      $standard['ScoringPercentage'] = 0;
      $standard['ScoringErrorsCount'] = 0;

      $my_scored_artifact_count = 0;
      $completely_scored_artifact_count = 0;
      $no_ioa_count = 0;
      $i_am_reviewer = false;

      // Loop over each artifact for this standard.
      foreach ( $artifacts as $key => $artifact )
      {
        // Get formatted scores.
        $scores = $scoring->get_scores_for_artifact($artifact['ArtifactsId']);
        $artifact['Scores'] = $scores;

        $artifact['ScoringError'] = 'No';
        $standard['ScoringError'] = 'No';

        // Increment completely_scored_artifact_count if there are 2 scores for this artifact.
        // TODO: We should make this more flexible - what if a portfolio only requires
        // 1 reviewer to score?
        if ( count( $scores ) >= 2 )
        {
          $completely_scored_artifact_count++;
        }

        // Increment my_scored_artifact_count if I scored this artifact.
        foreach ( $scores as $score )
        {
          if ( $score['ArtifactScoresReviewerId'] == Me::get_id() )
          {
            $my_scored_artifact_count++;
            $i_am_reviewer = true;
          }
        }

        // Do we have a scoring error for this artifact.
        foreach ( $scores as $score )
        {
          if ( $score['ArtifactScoresErrorIOA'] == 'Yes' || $score['ArtifactScoresErrorFail'] == 'Yes' )
          {
            $artifact['ScoringError'] = 'Yes';
            $standard['ScoringError'] = 'Yes';
            $standard['ScoringErrorsCount']++;

            // Check explicitely for IOA error.
            if ( $score['ArtifactScoresErrorIOA'] == 'Yes' )
            {
              $no_ioa_count++;
            }
            break;
          }
        }

        $new_artifacts[] = $artifact;
      }
      // End foreach loop for ARTIFACTS

      // Figure out IOA for standard. If 80% or more of artifacts
      // have no_ioa, then the standard does not have ioa.
      $standard['StandardIOAPercentage'] = 100;
      if ( $no_ioa_count > 0 )
      {
        $standard_ioa_percentage = number_format( ( (count( $artifacts ) - $no_ioa_count) / count( $artifacts ) * 100 ), 0 );
        $standard['StandardIOAPercentage'] = $standard_ioa_percentage;
      }

      // Figure out scoring completion ratio.
      if ( $i_am_reviewer == true )
      {
        // If I'm a reviewer, then I should only see the ratio of my own scores.
        // artifact_count: 5, scored_artifact_count: 3
        $standard['ScoringRatio'] = [
          'artifact_count' => count( $artifacts ),
          'scored_artifact_count' => $my_scored_artifact_count
        ];
      } else {
          // If I'm a candidate or an admin, then I should see ratio of completed scored.
          $standard['ScoringRatio'] = [
            'artifact_count' => count( $artifacts ),
            'scored_artifact_count' => $completely_scored_artifact_count
          ];
      }
      if ( $standard['ScoringRatio']['artifact_count'] > 0 )
      {
        $standard['ScoringPercentage'] = number_format( ($standard['ScoringRatio']['scored_artifact_count'] / $standard['ScoringRatio']['artifact_count']) * 100, 0 );
      }

      $standard['Artifacts'] = $new_artifacts;
      $new_data[] = $standard;
    }
    // End foreach loop for STANDARDS

    return $this->api_response( $new_data );
  }

  //
  // calculate_portfolio_score
  //
  public function calculate_portfolio_score( $PortfolioId, $plugin_name )
  {
    // Calculate portfolio score and update Portfolios table.
    $class = 'App\Library\Scoring\\' . $plugin_name;
    $scoring = App::make($class);

    $data = $scoring->calculate_portfolio_score( $PortfolioId );
    // $data will contain final score percentage, PASS/FAIL, Standards that failed.
  }

  public function reviewer_mark_scoring_complete( $portfolio_id )
  {
    // Get portfolio information
    $portfolio = $this->Portfolios_model->get_by_id( $portfolio_id );

    // Update reviewer entry in PortfolioUserLu, ScoringComplete == Yes
    DB::table('PortfolioUserLu')
      ->where('PortfolioUserLuPortfolioId', $portfolio_id)
      ->where('PortfolioUserLuUserId', Me::get_id())
      ->where('PortfolioUserLuUserType', 'Reviewer')
      ->update([
        'PortfolioUserLuScoringComplete' => 'Yes',
        'PortfolioUserLuUpdatedAt' => date('Y-m-d G:i:s')
    ]);

    // Check to see if all reviewers for this portfolio have ScoringComplete = Yes.
    // If they do, then update the portfolio status to Scored.
    $this->PortfolioUserLu_model->set_col('PortfolioUserLuPortfolioId', $portfolio_id);
    $this->PortfolioUserLu_model->set_col('PortfolioUserLuUserType', 'Reviewer');
    $reviewers = $this->PortfolioUserLu_model->get();

    $portfolio_review_complete = true;
    foreach ( $reviewers as $reviewer )
    {
      if ( $reviewer['PortfolioUserLuScoringComplete'] == 'No' )
      {
        $portfolio_review_complete = false;
        break;
      }
    }

    // Send admins email message and notification letting them know:
    // 1. That this reviewer has completed their scoring, and
    // 2. If the whole portfolio has been completed scored.

    // Get admins
    $this->AcctUsersLu_model->set_col( 'AcctUsersLuAdmin', 'Yes' );
    $this->AcctUsersLu_model->set_col( 'AcctUsersLuAcctId', Me::get_account_id() );
    $admins = $this->AcctUsersLu_model->get();

    $message_admin = Me::get('UsersFirstName') . ' ' . Me::get('UsersLastName') . ' completed their review of ' . $portfolio['UsersFirstName'] . ' ' . $portfolio['UsersLastName'] . '\'s ' . $portfolio['PTypesTitle'] . ' portfolio.';

    $message_reviewer = 'You completed your review of ' . $portfolio['UsersFirstName'] . ' ' . $portfolio['UsersLastName'] . '\'s ' . $portfolio['PTypesTitle'] . ' portfolio. <strong>Thank you!</strong>';

    if ( $portfolio_review_complete == true )
    {
      // If portfolio_review_complete == true, that means that ALL reviewers
      // assigned to this portfolio have clicked the REVIEW COMPLETE button on 
      // their dashboard.
      // NOW we can calculate total portfolio score and IOA.

      // Calculate final portfolio score and IOA Yes/No


      // Update message to administrator.
      $message_admin .= ' <strong>All reviews are complete for this portfolio.</strong>';

      // Update the portfolio status to Scored.
      $this->Portfolios_model->update_portfolio_status( $portfolio_id, 'Scored' );
    }

    foreach ( $admins as $admin )
    {
      // Setup object for mailing.
      $mailing = [
        'recipient' => $admin['UsersFirstName'] . ' ' . $admin['UsersLastName'],
        'message_text' => $message_admin
      ];
      // Send email
      Mail::send('emails.completed-portfolio-review', $mailing, function ($message) use ($admin) {
        $message->from('support@nationaldb.org', 'Portfolio Support');
        $message->to( $admin['UsersEmail'] );
        $message->subject('Completed Portfolio Review');
      });

      // Create notification entry
      $this->Notifications_model->insert([
        'NotificationsAccountId' => Me::get_account_id(),
        'NotificationsUserId' => $admin['UsersId'],
        'NotificationsTitle' => 'Completed Portfolio Review',
        'NotificationsBody' => $message_admin,
        'NotificationsType' => 'portfolio-review-complete',
        'NotificationsUpdatedAt' => date('Y-m-d G:i:s'),
        'NotificationsCreatedAt' => date('Y-m-d G:i:s')
      ]);
    }

    // Finally, send email and notification to reviewer.
    // Setup object for mailing.
    $reviewer = Me::get();
    $mailing = [
      'recipient' => Me::get('UsersFirstName') . ' ' . Me::get('UsersLastName'),
      'message_text' => $message_reviewer
    ];
    // Send email
    Mail::send('emails.completed-portfolio-review', $mailing, function ($message) use ($reviewer) {
      $message->from('support@nationaldb.org', 'Portfolio Support');
      $message->to( $reviewer['UsersEmail'] );
      $message->subject('Completed Portfolio Review');
    });

    // Create notification entry
    $this->Notifications_model->insert([
      'NotificationsAccountId' => Me::get_account_id(),
      'NotificationsUserId' => $reviewer['UsersId'],
      'NotificationsTitle' => 'Completed Portfolio Review',
      'NotificationsBody' => $message_reviewer,
      'NotificationsType' => 'portfolio-review-complete',
      'NotificationsUpdatedAt' => date('Y-m-d G:i:s'),
      'NotificationsCreatedAt' => date('Y-m-d G:i:s')
    ]);
  }
}

/* End File */
