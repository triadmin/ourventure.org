<?php

namespace App\Http\Controllers\Api\V1;

use DB;
use App;
use Mail;
use Hash;
use Validator;
use App\Library\Me;

class PTypes extends \App\Library\ApiController
{
  public $validation_create = [
  'PTypesTitle' => 'required'
  ];

  public $validation_update = [
  'PTypesTitle' => 'required'
  ];

  public $validation_message = [
  'PTypesTitle.required' => 'The title field is required.'
  ];

  //
  // Construct.
  //
  public function __construct()
  {
    parent::__construct();

    // Load shared modals
    $this->PortfolioUserLu_model = App::make('App\Models\PortfolioUserLu');
    $this->Portfolios_model = App::make('App\Models\Portfolios');
    $this->PTypeUserLu_model = App::make('App\Models\PTypeUserLu');
    $this->Media_model = App::make('App\Models\Media');
    $this->PTypes_model = App::make('App\Models\PTypes');
  }

  //
  // Select mentors and reviewers for a PType.
  //
  public function attach_user_to_ptype()
  {
    // Is this mentor/reviewer already selected for this PTypeId?
    $new_select_entry = false;
    $entry = (array) DB::table('PTypeUserLu')
      ->where('PTypeUserLuPTypeId', $this->request->input('PTypeUserLuPTypeId'))
      ->where('PTypeUserLuUserId', $this->request->input('PTypeUserLuUserId'))
      ->where('PTypeUserLuUserType', $this->request->input('PTypeUserLuUserType'))
      ->first();
    if(! $entry )
    {
      // Create the entry in PTypeUserLu table.
      $portfolio_id = DB::table('PTypeUserLu')->insertGetId([
        'PTypeUserLuAccountId' => Me::get_account_id(),
        'PTypeUserLuPTypeId' => $this->request->input('PTypeUserLuPTypeId'),
        'PTypeUserLuUserId' => $this->request->input('PTypeUserLuUserId'),
        'PTypeUserLuUserType' => $this->request->input('PTypeUserLuUserType'),
        'PTypeUserLuUpdatedAt' => date('Y-m-d G:i:s'),
        'PTypeUserLuCreatedAt' => date('Y-m-d G:i:s')
      ]);
      $new_select_entry = true;

      // TODO: Send email to mentor/reviewer notifying them that they
      // have been added to the ptype.  Also, add notification to their acct.
    }

    return $this->api_response([ 'new_select_entry' => $new_select_entry ]);
  }

  //
  // Remove mentors and reviewers from a PType.
  //
  public function remove_user_from_ptype()
  {
    if ( $this->request->input('UserType') == 'candidate' )
    {
      // Set this portfolio Status = Inactive. We don't want to delete the portfolio
      DB::table('Portfolios')
      ->where( 'PortfoliosId', $this->request->input('ItemId') )
      ->update([
        'PortfoliosStatus' => 'Inactive',
        'PortfoliosUpdatedAt' => date('Y-m-d G:i:s')
      ]);
    } else {
      // Just remove this user from the PTypesUserLu table
      $this->PTypeUserLu_model->set_col( 'PTypeUserLuUserId', $this->request->input('UsersId') );
      $this->PTypeUserLu_model->set_col( 'PTypeUserLuPTypeId', $this->request->input('ItemId') );
      $this->PTypeUserLu_model->set_col( 'PTypeUserLuUserType', $this->request->input('UserType') );
      $this->PTypeUserLu_model->delete_all();

      // Delete any records in PortfolioUserLu
      $this->PortfolioUserLu_model->set_col( 'PortfolioUserLuUserId', $this->request->input('UsersId') );
      $this->PortfolioUserLu_model->set_col( 'PortfolioUserLuUserType', $this->request->input('UserType') );
      $this->PortfolioUserLu_model->set_col( 'PortfoliosPTypeId', $this->request->input('ItemId') );
      $userlu = $this->PortfolioUserLu_model->get();

      // Can't use delete_all on this query for some reason.
      foreach ( $userlu as $user )
      {
        $this->PortfolioUserLu_model->delete_by_id( $user['PortfolioUserLuId'] );
      }
    }
  }

  //
  // Get all candidates for a given PTypeId.
  //
  public function get_candidates_for_ptype( $ptype_id )
  {
    $candidates = $this->Portfolios_model->get_candidates_for_ptype( $ptype_id );
    return $this->api_response( $candidates );
  }

  //
  // Get all reviewers for a given PTypeId.
  //
  public function get_reviewers_for_ptype( $ptype_id )
  {
    $reviewers = $this->PTypeUserLu_model->get_ptype_users( 'Reviewer', $ptype_id );
    return $this->api_response( $reviewers );
  }

  //
  // Get all mentors for a given PTypeId.
  //
  public function get_mentors_for_ptype( $ptype_id )
  {
    $mentors = $this->PTypeUserLu_model->get_ptype_users( 'Mentor', $ptype_id );
    return $this->api_response( $mentors );
  }

	//
	// assign_people_to_candidate: Assign mentors and reviewers to a candidate.
	// We made this a separate method for two reasons:
	// 1. We changed the UI so that both mentors and reviewers can be assigned
	//    in the same modal.
	// 2. There is a matching algorithm we're gonna have to build so that mentors
	//    and reviewers can be filtered depending on the candidate (e.g. home state,
	//    and other criteria)
	//
	public function get_mentors_reviewers_to_assign( $ptype_id, $user_id )
	{
		$mentors_reviewers = [];
		$mentors_reviewers['Mentors'] = $this->PTypeUserLu_model->get_ptype_users( 'Mentor', $ptype_id );
		$mentors_reviewers['Reviewers'] = $this->PTypeUserLu_model->get_ptype_users( 'Reviewer', $ptype_id );

		// Remove mentors already assigned to this user.
		foreach ( $mentors_reviewers['Mentors'] AS $key => $mentor )
		{
			foreach ( $mentor['Assigned_candidates'] AS $candidate )
			{
				if ( $user_id == $candidate->UsersId )
				{
					unset( $mentors_reviewers['Mentors'][$key] );
					$mentors_reviewers['Mentors'] = array_values( $mentors_reviewers['Mentors'] );
				}
			}
		}

		// Remove reviewers already assigned to this user.
		foreach ( $mentors_reviewers['Reviewers'] AS $key => $reviewer )
		{
			foreach ( $reviewer['Assigned_candidates'] AS $candidate )
			{
				if ( $user_id == $candidate->UsersId )
				{
					unset( $mentors_reviewers['Reviewers'][$key] );
					$mentors_reviewers['Reviewers'] = array_values( $mentors_reviewers['Reviewers'] );
				}
			}
		}
		return $this->api_response( $mentors_reviewers );
	}

  //
  // Get ptype information
  //
  public function get_ptype( $ptype_id )
  {
    $data = $this->PTypes_model->get_by_id( $ptype_id );
    return $this->api_response( $data );
  }

  //
  // Get PType stats
  //
  public function get_ptype_stats( $ptype_id )
  {
    $data = $this->PTypes_model->get_ptype_stats( $ptype_id );
    return $this->api_response( $data );
  }

  //
  // get_portfolios_by_status: get all portfolios of a given ptype_id AND status
  //
  public function get_portfolios_by_status( $ptype_id, $status )
  {
    $this->Portfolios_model->set_col( 'PortfoliosPTypeId', $ptype_id );
    $this->Portfolios_model->set_col( 'PortfoliosStatus', $status );
    $data = $this->Portfolios_model->get();
    return $this->api_response( $data );
  }
}

/* End File */
