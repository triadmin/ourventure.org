<?php

namespace App\Http\Controllers\Api\V1;

use DB;
use Auth;
use App\Library\Me;
use App;
use Mail;
use Hash;
use Validator;

class Accounts extends \App\Library\ApiController
{
  //
  // Construct.
  //
  public function __construct()
  {
    parent::__construct();

    // Load shared modals
    $this->Accounts_model = App::make('App\Models\Accounts');
    $this->PTypes_model = App::make('App\Models\PTypes');
  }

  public function get_account_data()
  {
    $account_data = [];

    // Get pTypes for this account
    $this->PTypes_model->set_col( 'PTypesAccountId', Me::get_account_id() );
    $this->PTypes_model->set_order( 'PTypesTitle', 'ASC' );
    $ptypes = $this->PTypes_model->get();
    $account_data['PTypes'] = $ptypes;

    return $this->api_response( $account_data );
  }
}

/* End File */
