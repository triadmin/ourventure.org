<?php

namespace App\Http\Controllers\Api\V1;

use DB;
use App;
use Mail;
use Hash;
use Validator;
use App\Library\Me;

class Users extends \App\Library\ApiController
{
	public $validation_create = [
    'UsersFirstName' => 'required',
    'UsersLastName' => 'required',
    'UsersEmail' => 'required|email|part_of_account'
  ];

	public $validation_update = [];

	public $validation_message = [
  	'part_of_account' => 'This user\'s email is already part of this account.'
	];

  //
  // Construct.
  //
  public function __construct()
  {
    parent::__construct();

    // Load shared modals
    $this->AcctUsersLu_model = App::make('App\Models\AcctUsersLu');
    $this->Accounts_model = App::make('App\Models\Accounts');
    $this->Portfolios_model = App::make('App\Models\Portfolios');
    $this->PTypeUserLu_model = App::make('App\Models\PTypeUserLu');
		$this->PortfolioUserLu_model = App::make('App\Models\PortfolioUserLu');
		$this->Users_model = App::make('App\Models\Users');
  }

  //
  // Get all users for an account
  //
  public function get($user_type = 'all')
  {
    // Get users for this account.
    $this->AcctUsersLu_model->set_col('AcctUsersLuAcctId', Me::get_account_id());
    // Restrict to Active Users
    $this->AcctUsersLu_model->set_col('UsersStatus', 'Active');

    if ( $user_type != 'all' )
    {
      // candidates, mentors, reviewers, admins
      switch ( $user_type )
      {
        case 'candidates':
          $this->AcctUsersLu_model->set_col('AcctUsersLuCandidate', 'Yes');
          break;
        case 'mentors';
          $this->AcctUsersLu_model->set_col('AcctUsersLuMentor', 'Yes');
          break;
        case 'reviewers';
          $this->AcctUsersLu_model->set_col('AcctUsersLuReviewer', 'Yes');
          break;
        case 'admins';
          $this->AcctUsersLu_model->set_col('AcctUsersLuAdmin', 'Yes');
          break;
      }
    }

    $this->AcctUsersLu_model->set_order( 'UsersLastName', 'ASC' );
    $accts = $this->AcctUsersLu_model->get();

    $accts_new = [];
    foreach ( $accts as $acct )
    {
      $ptypes = [];
      // See if this user is assigned to any PTypes
      if ( $user_type == 'candidates' )
      {
        $this->Portfolios_model->set_col( 'PortfoliosUserId', $acct['UsersId'] );
        $this->Portfolios_model->set_select([ 'PortfoliosPTypeId', 'PortfoliosId', 'PTypesTitle', 'PortfoliosCompletionPercentage', 'PortfoliosStatus' ]);
        $acct['PTypes'] = $this->Portfolios_model->get();
      } elseif ( $user_type == 'mentors' ) {
        $this->PTypeUserLu_model->set_col( 'PTypeUserLuUserId', $acct['UsersId'] );
        $this->PTypeUserLu_model->set_col( 'PTypeUserLuUserType', 'Mentor' );
        $this->PTypeUserLu_model->set_select( 'PTypeUserLuPTypeId' );
        $acct['PTypes'] = $this->PTypeUserLu_model->get();
      } elseif ( $user_type == 'reviewers' ) {
        $this->PTypeUserLu_model->set_col( 'PTypeUserLuUserId', $acct['UsersId'] );
        $this->PTypeUserLu_model->set_col( 'PTypeUserLuUserType', 'Reviewer' );
        $this->PTypeUserLu_model->set_select( 'PTypeUserLuPTypeId' );
        $acct['PTypes'] = $this->PTypeUserLu_model->get();
      } else {
        $this->Portfolios_model->set_col( 'PortfoliosUserId', $acct['UsersId'] );
        $this->Portfolios_model->set_select([ 'PortfoliosPTypeId', 'PortfoliosId', 'PTypesTitle', 'PortfoliosCompletionPercentage', 'PortfoliosStatus' ]);
        $acct['PTypes'] = $this->Portfolios_model->get();
      }

      $accts_new[] = $acct;
    }

    // Return happy!
    return $this->api_response($accts_new);
  }

  //
  // Get one user by id.
  //
  public function id($id)
  {
    $this->AcctUsersLu_model->set_col('AcctUsersLuUserId', $id);
    $this->AcctUsersLu_model->set_col('AcctUsersLuAcctId', Me::get_account_id());
    $user = $this->AcctUsersLu_model->get();
    $user = $user[0];

    // Is this user an account owner?
    $is_account_owner = 'No';
    $this->Accounts_model->set_col('AccountsOwnerId', $id);
    $account_owner = $this->Accounts_model->get();

		// Is this user a Candidate and assigned to any PTypes?
		$user['PTypes'] = [];
		if ( $user['IsCandidate'] == 'Yes' )
		{
			$this->Portfolios_model->set_col( 'PortfoliosUserId', $user['UsersId'] );
			$this->Portfolios_model->set_col( 'PortfoliosAccountId', Me::get_account_id() );
			$this->Portfolios_model->set_select([ 'PortfoliosPTypeId', 'PortfoliosId', 'PTypesTitle', 'PortfoliosCompletionPercentage', 'PortfoliosStatus' ]);
			$user['PTypes'] = $this->Portfolios_model->get();
		}

    if ( count($account_owner) > 0 )
    {
      $is_account_owner = 'Yes';
    }
    $user['IsAccountOwner'] = $is_account_owner;

    // Return happy!
    return $this->api_response( $user );
  }

  //
  // Get the me object.
  //
  public function me()
  {
    $this->AcctUsersLu_model->set_col('AcctUsersLuUserId', Me::get_id());
    $this->AcctUsersLu_model->set_col('AcctUsersLuAcctId', Me::get_account_id());
    $user = $this->AcctUsersLu_model->get();

    // Is this user an account owner?
    $is_account_owner = 'No';
    $this->Accounts_model->set_col('AccountsOwnerId', Me::get_id());
    $account_owner = $this->Accounts_model->get();

    if ( count($account_owner) > 0 )
    {
      $is_account_owner = 'Yes';
    }

    $user = $user[0];
    $user['IsAccountOwner'] = $is_account_owner;

    // Add ptypes that I'm assigned to
    $this->Portfolios_model->set_col( 'PortfoliosUserId', Me::get_id() );
    $this->Portfolios_model->set_select([ 'PortfoliosId', 'PTypesTitle', 'PortfoliosCompletionPercentage', 'PortfoliosStatus' ]);
    $user['Portfolios'] = $this->Portfolios_model->get();

		// Add candidates to which I'm assigned.
		$this->PortfolioUserLu_model->set_col( 'PortfolioUserLuUserId', Me::get('UsersId') );
		$candidate_portfolios = $this->PortfolioUserLu_model->get();

		$user['Candidates'] = [];
    $user['Mentees'] = [];
		foreach ( $candidate_portfolios as $candidate_portfolio )
		{
			$cp = $this->Portfolios_model->get_by_id( $candidate_portfolio['PortfolioUserLuPortfolioId'] );
			if ( $cp !== false )
			{
				$user['Candidates'][] = $cp;
			}
      if ( $candidate_portfolio['PortfolioUserLuUserType'] == 'Mentor' )
      {
        $user['Mentees'][] = $cp;
      }
		}

		// Get the BaseAdmin session variable to see if this user is
		// masquerading as another user.
		$user['BaseAdmin'] = $this->request->session()->get('BaseAdmin');

    // Return happy!
    return $this->api_response( $user );
  }

  //
  // Update password
  //
  public function update_password($id)
  {
    // Make sure I am an admin or the user changing password.
    if((Me::get('AcctUsersLuAdmin') == 'Yes') || (Me::get_id() == $id))
    {
			// Update record in Users table
	    DB::table('Users')
	      ->where('UsersId', $id)
	      ->update([
	        'UsersPassword' => Hash::make($this->request->input('Password')),
	        'UsersUpdatedAt' => date('Y-m-d G:i:s')
	      ]);
      return $this->api_response();
    }

    // Return no bueno!
    return $this->api_response();
  }

  //
  // Update user information
  //
  public function update( $id )
  {
    // Update record in Users table
    DB::table('Users')
      ->where( 'UsersId', $id )
      ->update([
        'UsersFirstName' => $this->request->input('UsersFirstName'),
        'UsersLastName' => $this->request->input('UsersLastName'),
        'UsersEmail' => $this->request->input('UsersEmail'),
				'UsersState' => $this->request->input('UsersState'),
        'UsersTitle' => $this->request->input('UsersTitle'),
				'UsersBio' => $this->request->input('UsersBio'),
        'UsersInstitution' => $this->request->input('UsersInstitution'),
				'UsersGetEmails' => $this->request->input('UsersGetEmails'),
        'UsersAvatarId' => $this->request->input('UsersAvatarId'),
        'UsersUpdatedAt' => date('Y-m-d G:i:s')
      ]);

    // Update record in AcctUsersLu table (permissions)
    DB::table('AcctUsersLu')
      ->where( 'AcctUsersLuAcctId', Me::get_account_id() )
      ->where( 'AcctUsersLuUserId', $id )
      ->update([
        'AcctUsersLuAdmin' => $this->request->input('AcctUsersLuAdmin'),
        'AcctUsersLuCandidate' => $this->request->input('AcctUsersLuCandidate'),
        'AcctUsersLuMentor' => $this->request->input('AcctUsersLuMentor'),
        'AcctUsersLuReviewer' => $this->request->input('AcctUsersLuReviewer'),
        'AcctUsersLuAdminReceiveMessagesCandidate' => $this->request->input('AcctUsersLuAdminReceiveMessagesCandidate'),
        'AcctUsersLuAdminReceiveMessagesMentor' => $this->request->input('AcctUsersLuAdminReceiveMessagesMentor'),
        'AcctUsersLuAdminReceiveMessagesReviewer' => $this->request->input('AcctUsersLuAdminReceiveMessagesReviewer'),
        'AcctUsersLuUpdatedAt' => date('Y-m-d G:i:s')
      ]);

    // Return the new user id.
    return $this->api_response([ 'Id' => $id ]);
  }

  //
  // Add new user to the system.
  //
  public function create()
  {
    $new_user = false;

    // Validate this request.
    if($rt = $this->validate_request('create'))
    {
        return $rt;
    }

    // Success. Now lets deal with the user. First we see if the user is already in the system.
		// Jeff - removed this existing users check because we do it in the validator method below.

    // Insert the user.
    $user_id = DB::table('Users')->insertGetId([
      'UsersFirstName' => $this->request->input('UsersFirstName'),
      'UsersLastName' => $this->request->input('UsersLastName'),
      'UsersEmail' => $this->request->input('UsersEmail'),
			'UsersState' => $this->request->input('UsersState'),
      'UsersTitle' => $this->request->input('UsersTitle'),
      'UsersInstitution' => $this->request->input('UsersInstitution'),
      'UsersPassword' => '(Need To Create) :: ' . md5(uniqid()),
      'UsersUpdatedAt' => date('Y-m-d G:i:s'),
      'UsersCreatedAt' => date('Y-m-d G:i:s')
    ]);

    // Get the user.
    $user = (array) DB::table('Users')->where('UsersId', $user_id)->first();

    // Set flag that this is a new user. We send them a welcome email.
    $new_user = true;

    // Now that we have the user lets add them to the account.
    DB::table('AcctUsersLu')->insert([
      'AcctUsersLuAcctId' => Me::get_account_id(),
      'AcctUsersLuUserId' => $user['UsersId'],
      'AcctUsersLuAdmin' => $this->request->input('AcctUsersLuAdmin'),
      'AcctUsersLuCandidate' => $this->request->input('AcctUsersLuCandidate'),
      'AcctUsersLuMentor' => $this->request->input('AcctUsersLuMentor'),
      'AcctUsersLuReviewer' => $this->request->input('AcctUsersLuReviewer'),
      'AcctUsersLuAdminReceiveMessagesCandidate' => $this->request->input('AcctUsersLuAdminReceiveMessagesCandidate'),
      'AcctUsersLuAdminReceiveMessagesMentor' => $this->request->input('AcctUsersLuAdminReceiveMessagesMentor'),
      'AcctUsersLuAdminReceiveMessagesReviewer' => $this->request->input('AcctUsersLuAdminReceiveMessagesReviewer'),
      'AcctUsersLuUpdatedAt' => date('Y-m-d G:i:s'),
      'AcctUsersLuCreatedAt' => date('Y-m-d G:i:s')
    ]);

    // Setup sender.
    $sender = [
      'UsersFirstName' => Me::get('UsersFirstName'),
      'UsersLastName' => Me::get('UsersLastName'),
      'UsersEmail' => Me::get('UsersEmail')
    ];

    // Setup object for mailing.
    $mailing = [
      'user' => $user,
      'sender' => $sender,
      'account' => (array) DB::table('Accounts')->where('AccountsId', Me::get_account_id())->first()
    ];

    // If we have a new user send them an email inviting them to set a password.
    if($new_user)
    {
      // Send email
      Mail::send('emails.welcome-set-password', $mailing, function ($message) use ($user) {
        $message->from('support@nationaldb.org', 'Portfolio Support');
        $message->to($user['UsersEmail']);
        $message->subject('Welcome To Your Portfolio Account');
      });
    }

    // Not a new user. They do not have to set their password. Just give them notice they were added.
    if(! $new_user)
    {
      // Send email
			if ( $user['UsersGetEmails'] == 'Yes' )
			{
	      Mail::send('emails.welcome-new-account', $mailing, function ($message) use ($user) {
	        $message->from('support@nationaldb.org', 'Portfolio Support');
	        $message->to($user['UsersEmail']);
	        $message->subject('Welcome To Your Portfolio Account');
	      });
			}
    }

    // Return the new user id.
    return $this->api_response([ 'Id' => $user['UsersId'] ]);
  }

	//
	// Send welcome email
	//
	public function send_welcome_email( $users_id = null )
	{
		// Get user
		$user = $this->Users_model->get_by_id( $users_id );

		// Setup sender.
    $sender = [
      'UsersFirstName' => Me::get('UsersFirstName'),
      'UsersLastName' => Me::get('UsersLastName'),
      'UsersEmail' => Me::get('UsersEmail')
    ];

    // Setup object for mailing.
    $mailing = [
      'user' => $user,
      'sender' => $sender,
      'account' => (array) DB::table('Accounts')->where('AccountsId', Me::get_account_id())->first()
    ];

		// Send email
		Mail::send('emails.welcome-set-password', $mailing, function ($message) use ($user) {
			$message->from('support@nationaldb.org', 'Portfolio Support');
			$message->to($user['UsersEmail']);
			$message->subject('Welcome To Your Portfolio Account');
		});

	}

  //
  // Delete a record by id.
  //
  public function delete($_id = null)
  {
    // Delete any PTypeUserLu for this account.
    DB::table('PTypeUserLu')
       ->where('PTypeUserLuUserId', $_id)
       ->where('PTypeUserLuAccountId', Me::get_account_id())
       ->delete();

    // Delete any PortfolioUserLu for this account. (in case this user is a mentor or reviewer)
    DB::table('PortfolioUserLu')
       ->where('PortfolioUserLuUserId', $_id)
       ->where('PortfolioUserLuAccountId', Me::get_account_id())
       ->delete();

    // See if we should delete the user.
    /*
    if(! DB::table('AcctUsersLu')->where('AcctUsersLuUserId', $_id)->get())
    {
      DB::table('Users')->where('UsersId', $_id)->delete();
    }
    */

    // Update user status
    DB::table('Users')
      ->where( 'UsersId', $_id )
      ->update([
        'UsersStatus' => 'Disabled',
        'UsersUpdatedAt' => date('Y-m-d G:i:s')
      ]);

    // Return happy
    return $this->api_response();
  }

}

// Custom validation to see if an email address is already part of an account.
Validator::extend('part_of_account', function($attribute, $value, $parameters, $validator)
{
  // Check to see if this user is already part of the account.
  if(! $user = (array) DB::table('Users')
		->where('UsersEmail', $value)
		->where('UsersStatus', 'Active')
		->first()
	)
  {
    return true;
  }

  if(! DB::table('AcctUsersLu')->where('AcctUsersLuUserId', $user['UsersId'])->first())
  {
    return true;
  }

  return false;
});

/* End File */
