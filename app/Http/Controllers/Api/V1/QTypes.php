<?php

namespace App\Http\Controllers\Api\V1;

use DB;
use Auth;
use App;
use Mail;
use Hash;
use Validator;
use App\Library\Me;

class QTypes extends \App\Library\ApiController
{
	public $validation_create = [
    'QTypesLabel' => 'required',
    'QTypesType' => 'required',
    'QTypesWhich' => 'required'
	];

	public $validation_update = [
    'QTypesLabel' => 'required',
    'QTypesType' => 'required'
	];

	public $validation_message = [];

	public function __construct()
  {
    parent::__construct();

    // Load shared modals
    $this->ReleaseMediaLu_model = App::make('App\Models\ReleaseMediaLu');
  }

	//
	// create override
	//
	public function create()
	{
		// We override the create method here so we can increment the
		// qType order and add that to the database. Otherwise, new
		// questions show up in odd places in the list.
		$max_order = DB::table('QTypes')
			->where('QTypesPTypesId', '=', $this->request->input('QTypesPTypesId'))
			->max('QTypesOrder');
		$order = $max_order + 1;

		$qtype_id = DB::table('QTypes')->insertGetId([
			'QTypesAccountId' => Me::get_account_id(),
			'QTypesPTypesId' => $this->request->input('QTypesPTypesId'),
			'QTypesWhich' => $this->request->input('QTypesWhich'),
			'QTypesLabel' => $this->request->input('QTypesLabel'),
			'QTypesType' => $this->request->input('QTypesType'),
			'QTypesValues' => $this->request->input('QTypesValues'),
			'QTypesRequired' => $this->request->input('QTypesRequired'),
			'QTypesOrder' => $order,
			'QTypesUpdatedAt' => date('Y-m-d G:i:s'),
			'QTypesCreatedAt' => date('Y-m-d G:i:s')
		]);

		return $this->api_response([ 'Id' => $qtype_id ]);
	}

  //
  // update_order
  //
  public function update_order()
  {
    $qtypes = $this->request->input('QTypes');
    foreach ( $qtypes as $key => $q )
    {
      DB::table('QTypes')->where('QTypesId', $q['QTypesId'])->update([
      'QTypesOrder' => $key+1]);
    }
  }

	//
	// Get release forms for release-form qtype.
	// For now we're only gonna work with ResumeId - same as Me::UserId
	//
	public function get_release_forms()
	{
		$this->ReleaseMediaLu_model->set_col( 'ReleaseMediaLuResumeId', Me::get('UsersId') );
		$this->ReleaseMediaLu_model->set_col( 'ReleaseMediaLuAccountId', Me::get_account_id() );
    $data['ReleaseForms'] = $this->ReleaseMediaLu_model->get();
		return $this->api_response( $data );
	}
}

/* End File */
