<?php

namespace App\Http\Controllers\Api\V1;

use DB;
use Auth;
use App;
use Mail;
use Hash;
use Cache;
use Config;
use Storage;
use Validator;
use App\Library\Me;

class Contact extends \App\Library\ApiController
{

  //
  // Construct.
  //
  public function __construct()
  {
    parent::__construct();

    // Load shared modals
    $this->Contact_model = App::make('App\Models\Contact');
    $this->AcctUsersLu_model = App::make('App\Models\AcctUsersLu');
  }

  //
  // Add new contact form entry.
  //
  public function create()
  {
    $email_body = $this->request->input('ContactBody');
    $email_body .= '<hr />' . $this->request->input('ContactUA');

    DB::table('Contact')->insert([
      'ContactAccountId' => Me::get_account_id(),
      'ContactUsersId' => Me::get('UsersId'),
      'ContactBody' => $this->request->input('ContactBody'),
      'ContactUserAgent' => $this->request->input('ContactUA'),
      'ContactUpdatedAt' => date('Y-m-d G:i:s'),
      'ContactCreatedAt' => date('Y-m-d G:i:s')
    ]);

    // Setup object for mailing.
    $mailing = [
      'sender' => Me::get(),
      'message_text' => $email_body
    ];

    // Send email
    $recipients = Config::get('site.contact_form_recipients');
    foreach ( $recipients as $recipient )
    {
      Mail::send('emails.send-contact-message', $mailing, function ($message) use ($recipient) {
        $message->from(Config::get('site.email_support'), 'Portfolio Support');
        $message->to( $recipient );
        $message->subject('Venture Contact Form Message');
      });
    }
  }

  //
  // Get all contact entries for an account.
  //
  public function get()
  {
    $this->Contact_model->set_col( 'ContactAccountId', Me::get_account_id() );
    $this->Contact_model->set_order( 'ContactCreatedAt', 'DESC' );
    $contacts = $this->Contact_model->get();

    return $this->api_response( $contacts );
  }
}
