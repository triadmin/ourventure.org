<?php 

namespace App\Http\Controllers\Api\V1;

use DB;
use Auth;
use App;
use Mail;
use Hash;
use Validator;
use App\Helpers\HelpersPortfolio;

class Standards extends \App\Library\ApiController
{
  public $validation_create = [
  'StandardsTitle' => 'required'
  ];

  public $validation_update = [
  'StandardsTitle' => 'required'
  ];

  public $validation_message = [];

  //
  // Construct.
  //
  public function __construct()
  {
    parent::__construct();

    // Load shared modals
    $this->Standards_model = App::make('App\Models\Standards');
  }

  //
  // Get a standard by id
  //
  public function id( $id )
  {
    $data = $this->Standards_model->get_by_id( $id );
    return $this->api_response($data);
  }

  public function get_standards( $ptype_id )
  {
    $this->Standards_model->set_col( 'StandardsPTypesId', $ptype_id );
    $this->Standards_model->set_order( 'StandardsOrder', 'ASC' );
    $data = $this->Standards_model->get();
    return $this->api_response($data);
  }

  public function get_standards_with_competencies( $id )
  {
    $data = $this->Standards_model->get_standards_with_competencies( $id );

    return $this->api_response($data);
  }

  private function check_competency_completion( $competency_id )
  {
  }

  public function update_order()
  {
    $standards = $this->request->input('Standards');
    foreach ( $standards as $key => $standard )
    {
      DB::table('Standards')->where('StandardsId', $standard['StandardsId'])->update([
      'StandardsOrder' => $key+1]);
    }
  }
}

/* End File */
