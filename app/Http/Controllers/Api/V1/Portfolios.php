<?php

namespace App\Http\Controllers\Api\V1;

use DB;
use App;
use Mail;
use Hash;
use Validator;
use App\Library\Me;
use App\Helpers\Helpers;

class Portfolios extends \App\Library\ApiController
{
  public $validation_create = [];
  public $validation_update = [];
  public $validation_message = [];

  //
  // Construct.
  //
  public function __construct()
  {
    parent::__construct();

    // Load shared modals
    $this->Portfolios_model = App::make('App\Models\Portfolios');
    $this->PTypes_model = App::make('App\Models\PTypes');
    $this->Artifacts_model = App::make('App\Models\Artifacts');
    $this->ArtifactScores_model = App::make('App\Models\ArtifactScores');
    $this->PortfolioUserLu_model = App::make('App\Models\PortfolioUserLu');
    $this->Users_model = App::make('App\Models\Users');
    $this->Notifications_model = App::make('App\Models\Notifications');
    $this->QTypes_model = App::make('App\Models\QTypes');
    $this->AcctUsersLu_model = App::make('App\Models\AcctUsersLu');
    $this->PortfolioReviewerComments_model = App::make('App\Models\PortfolioReviewerComments');
  }

  //
  // Get one portfolio by id.
  //
  public function id($id)
  {
    $portfolio = $this->Portfolios_model->get_by_id( $id );

    // Does this ptype have an About Me page?
    $ptype_id = $portfolio['PortfoliosPTypeId'];
    $this->QTypes_model->set_col( 'QTypesPTypesId', $ptype_id );
    $this->QTypes_model->set_col( 'QTypesWhich', 'Resume' );
    $q = $this->QTypes_model->get();

    $portfolio['PortfolioHasAboutMePage'] = 'No';
    if ( count( $q ) > 0 )
    {
      $portfolio['PortfolioHasAboutMePage'] = 'Yes';
    }

    // Get reviewer comments
    $this->PortfolioReviewerComments_model->set_col( 'PortfolioReviewerCommentsPortfolioId', $id );
    $portfolio['ReviewerComments'] = $this->PortfolioReviewerComments_model->get();

    return $this->api_response( $portfolio );
  }

  //
  // Create new portfolio for candidate
  //
  public function create()
  {
    // Does this candidate already have a portfolio of PTypeId but it's
    // just marked INACTIVE?
    $new_portfolio = false;
    $portfolio = (array) DB::table('Portfolios')
      ->where('PortfoliosUserId', $this->request->input('PortfoliosUserId'))
      ->where('PortfoliosPTypeId', $this->request->input('PortfoliosPTypeId'))
      ->first();
    if(! $portfolio )
    {
      // No portfolio...Create the portfolio
      $portfolio_id = DB::table('Portfolios')->insertGetId([
        'PortfoliosAccountId' => Me::get_account_id(),
        'PortfoliosUserId' => $this->request->input('PortfoliosUserId'),
        'PortfoliosPTypeId' => $this->request->input('PortfoliosPTypeId'),
        'PortfoliosUpdatedAt' => date('Y-m-d G:i:s'),
        'PortfoliosCreatedAt' => date('Y-m-d G:i:s')
      ]);
      $new_portfolio = true;

      // Add notification to candidate account.
      // Get portfolioType info
      $ptype = $this->PTypes_model->get_by_id( $this->request->input('PortfoliosPTypeId') );
      $message = 'A new portfolio for <b>' . $ptype['PTypesTitle'] . '</b> has been created for you.';

      $this->Notifications_model->insert([
        'NotificationsAccountId' => Me::get_account_id(),
        'NotificationsUserId' => $this->request->input('PortfoliosUserId'),
        'NotificationsTitle' => 'New Portfolio Created',
        'NotificationsBody' => $message,
        'NotificationsType' => 'portfolio-new',
        'NotificationsUpdatedAt' => date('Y-m-d G:i:s'),
        'NotificationsCreatedAt' => date('Y-m-d G:i:s')
      ]);
    } else {
      $status = 'Not Started';
      // Candidate has a portfolio but it's marked INACTIVE. Change the status.
      // What status? In Progress or Not Started? It depends on whether or not
      // there are any Artifacts for this portfolios. Let's check that now.
      $this->Artifacts_model->set_col( 'ArtifactsPortfolioId', $portfolio['PortfoliosId'] );
      $artifacts = $this->Artifacts_model->get();
      if ( count( $artifacts ) > 0 )
      {
        $status = 'In Progress';
      }

      // Update our portfolio status
      $this->Portfolios_model->update_portfolio_status( $portfolio['PortfoliosId'], $status );
    }

    return $this->api_response([ 'new_portfolio' => $new_portfolio ]);
  }

  public function update_status()
  {
    // Our portfolio id
    $portfolio_id = $this->request->input('PortfoliosId');

    // Our new portfolio status
    $portfolio_status = $this->request->input('PortfoliosStatus');

    // Update our portfolio status
    $this->Portfolios_model->update_portfolio_status( $portfolio_id, $portfolio_status );

    // Get people (mentors and reviewers) for this portfolio
    $portfolio_people = $this->Portfolios_model->get_portfolio_people( $portfolio_id );

    // Get portfolio information
    $portfolio = $this->Portfolios_model->get_by_id( $portfolio_id );

    switch ( $portfolio_status )
    {
      case 'Submitted for Review':
        // Do we need to update our governanace status for this portfolio?
        $ptype = $this->PTypes_model->get_by_id( $portfolio['PortfoliosPTypeId'] );
        if ( !empty( $ptype['PTypesGovernanceCandidate'] ))
        {
          $this->Portfolios_model->update_portfolio_governance_status( $portfolio_id, 'Yes', 'Candidate' );
        }

        // Enter notifications for candidate, reviewers, and admins.
        // Send emails to candiate, reviewers, and admins.
        foreach ( $portfolio_people as $key => $people_type )
        {
          if ( $key == 'Candidate' )
          {
            $message_email = 'Congratulations! You submitted your portfolio for review.';
            $message_notification = 'Congratulations! You submitted your portfolio for review.';
          } else {
            $candidate_name = $portfolio_people['Candidate'][0]['Name'];
            $message_email = $candidate_name . ' submitted their portfolio for review.';
            $message_notification = $candidate_name . ' submitted their portfolio for review.';
          }

          foreach ( $people_type as $person )
          {
            $this->Notifications_model->insert([
              'NotificationsAccountId' => Me::get_account_id(),
              'NotificationsUserId' => $person['UsersId'],
              'NotificationsTitle' => 'Portfolio Submitted for Review',
              'NotificationsBody' => $message_notification,
              'NotificationsType' => 'portfolio-submit',
              'NotificationsUpdatedAt' => date('Y-m-d G:i:s'),
              'NotificationsCreatedAt' => date('Y-m-d G:i:s')
            ]);

            // Setup object for mailing.
            $mailing = [
              'recipient' => $person['Name'],
              'message_text' => $message_email
            ];
            // Send email
            if ( $person['UsersGetEmails'] == 'Yes' )
            {
              Mail::send('emails.submit-portfolio', $mailing, function ($message) use ($person) {
                $message->from('support@nationaldb.org', 'Portfolio Support');
                $message->to( $person['Email'] );
                $message->subject('Portfolio Submitted for Review');
              });
            }
          }
        }
        break;
      case 'Reopened for Editing':
        // Enter notification for candidate and send them an email message
        foreach ( $portfolio_people as $key => $people_type )
        {
          if ( $key == 'Candidate' )
          {
            $person = $people_type[0];

            $message_email = 'Your portfolio has been reopened for editing by your portfolio administrator.';
            $message_notification = 'Your portfolio has been reopened for editing.';

            $this->Notifications_model->insert([
              'NotificationsAccountId' => Me::get_account_id(),
              'NotificationsUserId' => $person['UsersId'],
              'NotificationsTitle' => 'Portfolio Reopened for Editing',
              'NotificationsBody' => $message_notification,
              'NotificationsType' => 'portfolio-reopen',
              'NotificationsUpdatedAt' => date('Y-m-d G:i:s'),
              'NotificationsCreatedAt' => date('Y-m-d G:i:s')
            ]);

            // Setup object for mailing.
            $mailing = [
              'recipient' => $person['Name'],
              'message_text' => $message_email
            ];
            // Send email
            if ( $person['UsersGetEmails'] == 'Yes' )
            {
              Mail::send('emails.reopen-portfolio', $mailing, function ($message) use ($person) {
                $message->from('support@nationaldb.org', 'Portfolio Support');
                $message->to( $person['Email'] );
                $message->subject('Portfolio Reopened for Editing');
              });
            }
          }
        }
        break;
      case 'Scored and Released to Candidate':
        // TODO: Send email to candidate.
        // TODO: Send email to admins.
        // TODO: Add notifications to candidate and admins.
        break;
    }
  }

  //
  // Assign mentors and reviewers to a portfolio (candidate).
  //
  public function assign_user_to_portfolio()
  {
    // Is this mentor/reviewer already assigned to this Candidate?

    // Let's see if this UserId is already assigned to a PortfolioId but
    // they're portfolio status is INACTIVE.  If so, just set it to
    // In Progress
    $new_assignment = false;
    $entry = (array) DB::table('PortfolioUserLu')
      ->where('PortfolioUserLuUserId', $this->request->input('PortfolioUserLuUserId'))
      ->where('PortfolioUserLuPortfolioId', $this->request->input('PortfolioUserLuPortfolioId'))
      ->first();
    if(! $entry )
    {
      // Create the entry in PTypeUserLu table.
      $portfolio_id = DB::table('PortfolioUserLu')->insertGetId([
        'PortfolioUserLuAccountId' => Me::get_account_id(),
        'PortfolioUserLuPortfolioId' => $this->request->input('PortfolioUserLuPortfolioId'),
        'PortfolioUserLuUserId' => $this->request->input('PortfolioUserLuUserId'),
        'PortfolioUserLuUserType' => $this->request->input('PortfolioUserLuUserType'),
        'PortfolioUserLuUpdatedAt' => date('Y-m-d G:i:s'),
        'PortfolioUserLuCreatedAt' => date('Y-m-d G:i:s')
      ]);
      $new_assignment = true;

      // Send notification to mentor and reviewer about assignment.
      // Send notification to candidate IF a mentor is assigned (not a reviewer).
      $portfolio = $this->Portfolios_model->get_by_id( $this->request->input('PortfolioUserLuPortfolioId') );
      $assigned_user = $this->Users_model->get_by_id( $this->request->input('PortfolioUserLuUserId') );

      if ( $this->request->input('PortfolioUserLuUserType') == 'mentor' )
      {
        $notification_type = 'assign-mentor';

        // Send notification to candidate.
        $message = 'You have been assigned <b>' . $assigned_user['UsersFirstName'] . ' ' . $assigned_user['UsersLastName'] . '</b> as a mentor for your <b>' . $portfolio['PTypesTitle'] . '</b> portfolio.';
        // Enter notification for candidate.
        $this->Notifications_model->insert([
          'NotificationsAccountId' => Me::get_account_id(),
          'NotificationsUserId' => $portfolio['UsersId'],
          'NotificationsTitle' => 'Mentor Assignment',
          'NotificationsBody' => $message,
          'NotificationsType' => $notification_type,
          'NotificationsUpdatedAt' => date('Y-m-d G:i:s'),
          'NotificationsCreatedAt' => date('Y-m-d G:i:s')
        ]);

        $message = 'You have been assigned as a <b>Mentor to ' . $portfolio['UsersFirstName'] . ' ' . $portfolio['UsersLastName'] . '</b> and their <b>' . $portfolio['PTypesTitle'] . '</b> portfolio.';
      } elseif ( $this->request->input('PortfolioUserLuUserType') == 'reviewer' ) {
        $notification_type = 'assign-reviewer';
        $message = 'You have been assigned as a <b>Reviewer to ' . $portfolio['UsersFirstName'] . ' ' . $portfolio['UsersLastName'] . '</b> and their <b>' . $portfolio['PTypesTitle'] . '</b> portfolio.';
      }

      // Notification to mentor/reviewer
      $this->Notifications_model->insert([
        'NotificationsAccountId' => Me::get_account_id(),
        'NotificationsUserId' => $assigned_user['UsersId'],
        'NotificationsTitle' => 'Candidate Assignment',
        'NotificationsBody' => $message,
        'NotificationsType' => $notification_type,
        'NotificationsUpdatedAt' => date('Y-m-d G:i:s'),
        'NotificationsCreatedAt' => date('Y-m-d G:i:s')
      ]);
    }

    return $this->api_response([ 'new_assignment' => $new_assignment ]);
  }

  //
  // Remove mentor or reviewer from candidate portfolio
  //
  public function remove_user_from_portfolio( $portfolio_user_lu_id )
  {
    $this->PortfolioUserLu_model->delete_by_id( $portfolio_user_lu_id );
    return $this->api_response([ 'portfolio_user_lu_id' => $portfolio_user_lu_id ]);
  }

  //
  // Get dashboard
  //
  public function get_portfolios_for_dashboard()
  {
    $this->Portfolios_model->set_col('PortfoliosAccountId', Me::get_account_id());
    $this->Portfolios_model->set_col('PortfoliosUserId', Me::get_id());
    $portfolios_array = $this->Portfolios_model->get();

    return $this->api_response( $portfolios_array );
  }

  //
  // Get portfolios (candidates) that you are mentoring or reviewing
  // user_type: mentor - I am mentoring candidates, reviewer - I am reviewing
  //
  public function get_portfolios_for_mentor_reviewer( $user_type, $user_id = null )
  {
    if ( $user_id == null )
    {
      $user_id = Me::get_id();
    }
    $this->PortfolioUserLu_model->set_col( 'PortfolioUserLuUserId', $user_id );
    $this->PortfolioUserLu_model->set_col( 'PortfolioUserLuAccountId', Me::get_account_id() );
    $this->PortfolioUserLu_model->set_col( 'PortfolioUserLuUserType', $user_type );
    $data = $this->PortfolioUserLu_model->get();

    $new_data = [];
    foreach ( $data as $row )
    {
      $row['PortfolioUser'] = $this->Users_model->get_by_id( $row['PortfoliosUserId'] );
      $row['PType'] = $this->PTypes_model->get_by_id( $row['PortfoliosPTypeId'] );

      // If $user_type == 'Reviewer' then let's get percentage of
      // artifacts scored to see if we can activate the Review Completed button.
      $row['ScoringRatio'] = [];
      $row['ScoringPercentage'] = '';
      if ( $user_type == 'Reviewer' )
      {
        $row['ScoringRatio'] = $this->ArtifactScores_model->get_scored_ratio_for_reviewer( $row['PortfolioUserLuPortfolioId'], $user_id );
        if ( $row['ScoringRatio']['artifact_count'] > 0 )
        {
          $row['ScoringPercentage'] = number_format( ($row['ScoringRatio']['scored_artifact_count'] / $row['ScoringRatio']['artifact_count']) * 100, 0);
        }
      }

      $new_data[] = $row;
    }

    return $this->api_response( $new_data );
  }

  //
  // Get Menu
  //
  public function get_portfolio_menu( $portfolio_id )
  {
    $portfolio_menu = $this->Portfolios_model->calculate_portfolio_progress( $portfolio_id );

    // What if we have no standards or competencies?
    if ( $portfolio_menu === false )
    {
      $portfolio_menu = 'no standards';
    }

    // Does this ptype have an About Me page?
    $ptype_id = $portfolio_menu['PortfolioType']['PTypesId'];
    $this->QTypes_model->set_col( 'QTypesPTypesId', $ptype_id );
    $this->QTypes_model->set_col( 'QTypesWhich', 'Resume' );
    $q = $this->QTypes_model->get();

    $portfolio_menu['PortfolioHasAboutMePage'] = 'No';
    if ( count( $q ) > 0 )
    {
      $portfolio_menu['PortfolioHasAboutMePage'] = 'Yes';
    }

    return $this->api_response( $portfolio_menu );
  }

  //
  // is_user_assigned: Is the logged in user assigned to this
  // candidate? Can they view their portfolio?
  //
  public function is_user_assigned( $portfolio_id )
  {
    $this->PortfolioUserLu_model->set_col('PortfolioUserLuPortfolioId', $portfolio_id);
    $this->PortfolioUserLu_model->set_col('PortfolioUserLuUserId', Me::get('UsersId'));
    $assigned = $this->PortfolioUserLu_model->get();
    $is_assigned = false;
    if ( count($assigned) > 0 )
    {
      $is_assigned = true;
    }
    return $this->api_response( array('IsAssigned' => $is_assigned) );
  }

  //
  // save_reviewer_comments
  //
  public function save_reviewer_comments( $portfolio_id )
  {
    $comment_array = $this->request->input();

    if ( $comment_array['ReviewerId'] > 0 )
    {
      // Update comment record.
      DB::table('PortfolioReviewerComments')
        ->where('PortfolioReviewerCommentsPortfolioId', $portfolio_id)
        ->where('PortfolioReviewerCommentsUsersId', Me::get_id())
        ->update([
          'PortfolioReviewerCommentsStrengths' => $comment_array['CommentsStrengths'],
          'PortfolioReviewerCommentsAreasOfImprovement' => $comment_array['CommentsAreasImprovement'],
          'PortfolioReviewerCommentsOverall' => $comment_array['CommentsOverall'],
          'PortfolioReviewerCommentsUpdatedAt' => date('Y-m-d G:i:s')
        ]);
    } else {
      // Insert new record.
      $this->PortfolioReviewerComments_model->insert([
        'PortfolioReviewerCommentsAccountId' => Me::get_account_id(),
        'PortfolioReviewerCommentsUsersId' => Me::get_id(),
        'PortfolioReviewerCommentsPortfolioId' => $portfolio_id,
        'PortfolioReviewerCommentsStrengths' => $comment_array['CommentsStrengths'],
        'PortfolioReviewerCommentsAreasOfImprovement' => $comment_array['CommentsAreasImprovement'],
        'PortfolioReviewerCommentsOverall' => $comment_array['CommentsOverall'],
        'PortfolioReviewerCommentsUpdatedAt' => date('Y-m-d G:i:s'),
        'PortfolioReviewerCommentsCreatedAt' => date('Y-m-d G:i:s')
      ]);
    }

    return $this->api_response();
  }

  //
  // mark governance reviewed
  //
  public function mark_governance_reviewed( $portfolio_id, $user_type )
  {
    DB::table('PortfolioUserLu')
      ->where('PortfolioUserLuAccountId', Me::get_account_id())
      ->where('PortfolioUserLuUserId', Me::get_id())
      ->where('PortfolioUserLuPortfolioId', $portfolio_id)
      ->where('PortfolioUserLuUserType', $user_type)
      ->update([
        'PortfolioUserLuGovernanceConfirmed' => 'Yes',
        'PortfolioUserLuUpdatedAt' => date('Y-m-d G:i:s')
      ]);
    return $this->api_response();
  }
}

/* End File */
