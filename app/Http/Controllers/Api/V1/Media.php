<?php

namespace App\Http\Controllers\Api\V1;

use DB;
use Auth;
use App;
use Mail;
use Hash;
use Cache;
use Image;
use Storage;
use Validator;
use App\Library\Me;

class Media extends \App\Library\ApiController
{
	public $validation_create = [];
	public $validation_update = [];
	public $validation_message = [];

	//
  // Construct.
  //
  public function __construct()
  {
    parent::__construct();

    // Load shared modals
    $this->Media_model = App::make('App\Models\Media');
  }

	//
	// get
	//
	public function get()
	{
		$this->Media_model->set_col('MediaAccountId', Me::get_account_id());
		$this->Media_model->set_col('MediaUserId', Me::get('UsersId'));

		// Set col
		if(request()->input('col_MediaReleaseForm'))
		{
		  $this->Media_model->set_col('MediaReleaseForm', request()->input('col_MediaReleaseForm'));
		}

		$media = $this->Media_model->get();
		return $this->api_response( $media );
	}

  //
  // Upload file. (example: http://portfolio.nationaldb.dev/api/media/upload_file/artifacts)
  //
  public function upload_file(\Illuminate\Http\Request $request, $type)
  {
    //phpinfo();
    // Figure out which directory this is to be stored in.
    switch($type)
    {
      case 'artifacts':
        $dir = 'artifacts';
      break;

      case 'users':
        $dir = 'users';
      break;

      default:
        $dir = 'other';
      break;
    }

    // Build file name
    $name = str_ireplace(' ', '_', str_ireplace($request->file('file')->getClientOriginalExtension(), '', $request->file('file')->getClientOriginalName()) . $request->file('file')->guessExtension());

    // If image resize, first see if file is an image.
    if($size = getimagesize($request->file('file')->getRealPath()))
    {
			// If user avatar image, then center crop to 200 pixels
			if ( $type === 'users' )
			{
				$img = Image::make($request->file('file')->getRealPath());
				$img->fit(200);
				$img->save($request->file('file')->getRealPath());
			}
			elseif ( ($size[0] > 1000) && ($size[1] > 1000) )
      {
				// Else if image is NOT user avatar AND it's over 1000 pixels wide or tall...
        // Resize image.
        Image::make($request->file('file')->getRealPath())->resize(1000, null, function ($constraint)
				{
          $constraint->aspectRatio();
          $constraint->upsize();
        })->save($request->file('file')->getRealPath());
      }
    }

    // Insert into media table.
    $media_id = $this->model->insert([
			'MediaUserId' => Me::get('UsersId'),
      'MediaName' => $request->file('file')->getClientOriginalName(),
      'MediaType' => $request->file('file')->getMimeType(),
      'MediaSize' => $request->file('file')->getSize(),
      'MediaReleaseForm' => ($request->input('MediaReleaseForm')) ? $request->input('MediaReleaseForm') : 'No',
      'MediaPath' => ''
    ]);

    // Build storage path.
    $storage_path = $dir . '/' . Me::get_account_id() . '/' . $media_id . '_' . $name;

    // Update media table.
    $this->model->update([ 'MediaPath' => $storage_path ], $media_id);

    // Send file to rackspace.
    $cont = file_get_contents($request->file('file')->getRealPath());
    Storage::put($storage_path, $cont);

    // Put file in cache
    Cache::put($storage_path, $cont, 10);

    // Return the json object with all this data.
    return $this->api_response([
      'Id' => $media_id,
      'Name' => basename($storage_path),
      'Url' => url('api/v1/media/file/' . $media_id . '/' . basename($storage_path))
    ]);
  }

  //
  // Return file.
  //
  public function file($id, $name)
  {
    // Get the file
    if(! $media = $this->model->get_by_id($id))
    {
      abort(404);
    }

    if($media['MediaType'] == 'video/vimeo' || $media['MediaType'] == 'video/youtube')
    {
      $media['MediaTypeShort'] = 'video';
      return $this->api_response( $media );
    } else
    {
      // Security check.
      if(basename($media['MediaPath']) != $name)
      {
        abort(404);
      }

      // Check the cache first.
      $content = Cache::get($media['MediaPath'], function() use ($media) {
        $cont = Storage::get($media['MediaPath']);
        Cache::put($media['MediaPath'], $cont, 10);
        return $cont;
      });

      // Build temp file.
      $file = tempnam('/tmp', 'portfolio_');
      file_put_contents($file, $content);

      // Clean up old cached files.
      foreach(glob("/tmp/portfolio_*") AS $key => $row)
      {
        // if file is 24 hours (86400 seconds) old then delete it
        if(filemtime($row) < time() - 86400)
        {
          unlink($row);
        }
      }

      if($media['MediaTypeShort'] == 'doc' || $media['MediaTypeShort'] == 'excel')
      {
        header("Content-Disposition: attachment; filename=\"" . basename($media['MediaName']) . "\"");
        header("Content-Type: application/force-download");
        header("Content-Length: " . filesize($file));
        header("Connection: close");
      }

      // Asana: A bagillion XHR requests
      if($media['MediaTypeShort'] == 'pdf')
      {
        header("pragma: private");
        header("Cache-Control: private, max-age=86400");
      }

      // Return file
      return response()->file($file);
    }
  }

  //
  // Save Video
  //
  public function save_embed()
  {
    // Save to Media table.
    $media_id = $this->model->insert([
      'MediaName' => $this->request->input('MediaName'),
      'MediaType' => $this->request->input('MediaType'),
      'MediaPath' => $this->request->input('MediaPath')
    ]);

    // Save to ArtifactMediaLu table.
    $artifactmedialu_model = App::make('App\Models\ArtifactMediaLu');
    $artifactmedialu_model->insert([
      'ArtifactMediaLuAccountId' => Me::get_account_id(),
      'ArtifactMediaLuArtifactId' => $this->request->input('ArtifactsId'),
      'ArtifactMediaLuMediaId' => $media_id
    ]);
    return $this->api_response( ['MediaId' => $media_id] );
  }
}

/* End File */
