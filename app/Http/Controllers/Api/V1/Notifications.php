<?php

namespace App\Http\Controllers\Api\V1;

use DB;
use App;
use App\Library\Me;

class Notifications extends \App\Library\ApiController
{
  //
  // Mark notification as read.
  //
  public function mark_as_read()
  {
    DB::table('Notifications')
      ->where( 'NotificationsId', $this->request->input('NotificationsId') )
      ->update([
        'NotificationsHasRead' => 'Yes',
        'NotificationsUpdatedAt' => date('Y-m-d G:i:s')
      ]);
  }
}
