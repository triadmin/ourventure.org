<?php

namespace App\Http\Controllers;

use Redirect;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class AppController extends Controller
{
  //
  // Return the basic layout for our app.
  //
  public function template()
  {
    return view('layouts.app');
  }

  //
  // Special admin request to login as a different user.
  //
  public function admin_login($id)
  {
    if($id == session('BaseAdmin'))
    {
      session([ 'BaseAdmin' => '' ]);
    } else
    {
      session([ 'BaseAdmin' => session('LoggedIn') ]);
    }

    \App\Library\Auth::login($id);

    return Redirect::to('');
  }
}

/* End File */
