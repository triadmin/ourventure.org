<?php

namespace App\Http\Controllers;

use DB;
use Input;
use Hash;
use App\Library\Me;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class PublicPages extends Controller
{
  //
  // Display reviewers for a given portfolio type id
  //
  public function reviewers( $portfolio_type_id = null )
  {

    return view('layouts.app-public', [])->nest('body', 'info.reviewer-list', []);
  }

  //
  // whatisventurenice: Public page for NICE.
  //
  public function whatisventurenice()
  {

    return view('layouts.app-public', [])->nest('body', 'info.whatis-venture-nice', []);
  }
}
