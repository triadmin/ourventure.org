<?php

namespace App\Http\Controllers;

use DB;
use Input;
use Validator;
use Hash;
use App\Library\Me;
use App\Library\Auth as AuthClass;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class Auth extends Controller
{
  //
  // Login.
  //
  public function login()
  {
    // Already logged in?
    if(AuthClass::check())
    {
      return redirect('dashboard');
    }

    return view('layouts.auth', [])->nest('body', 'auth.login', []);
  }

  //
  // Forget password.
  //
  public function forget_password()
  {
    // Already logged in?
    if(AuthClass::check())
    {
      return redirect('dashboard');
    }

    return view('layouts.auth', [])->nest('body', 'auth.forget-password', []);
  }

  //
  // Logout
  //
  public function logout()
  {
    AuthClass::logout();
    return redirect('login/goodbye');
  }

  //
  // Login Post.
  //
  public function login_post(Request $request)
  {
    // Try to login.
    if(AuthClass::attempt($request->input('UsersEmail'), $request->input('UsersPassword')))
    {
      return [ 'status' => 1 ];
    } else
    {
      return [ 'status' => 0 ];
    }
  }

  //
  // Login Post.
  //
  public function reset_password_post(Request $request)
  {
    // Try to reset password.
    if(AuthClass::reset_password($request->input('UsersEmail')))
    {
      return [ 'status' => 1 ];
    } else
    {
      return [ 'status' => 0 ];
    }
  }

  //
  // Create a password.
  //
  public function set_password(Request $request, $hash)
  {
    // Get the user from the hash.
    if(! $users = DB::select('SELECT * FROM Users WHERE md5(UsersPassword) = :hash', [ 'hash' => $hash ]))
    {
      return redirect('forget_password/expired');
    }

    // Set user.
    $user = $users[0];

    return view('layouts.auth', [])->nest('body', 'auth.set-password', [ 'user' => $user, 'hash' => $hash ]);
  }

  //
  // Post back to reset password.
  //
  public function set_password_post(Request $request, $hash)
  {
    // Get the user from the hash.
    if(! $users = DB::select('SELECT * FROM Users WHERE md5(UsersPassword) = :hash', [ 'hash' => $hash ]))
    {
      abort(404);
    }

    // Set user.
    $user = $users[0];

    // Validate
    $validation = Validator::make($request->all(), [
      'UsersPassword' => 'required|min:6'
    ]);

    // Did we fail?
    if($validation->fails())
    {
      $errors = [];
      $messages = $validation->messages();

      foreach($request->all() AS $key => $row)
      {
      	if($messages->has($key))
      	{
      		$errors[$key] = $messages->first($key);
      	}
      }

      return [ 'status' => 0, 'errors' => $errors ];
    }

    // Set the new password.
    DB::table('Users')->where('UsersId', $user->UsersId)->update([
      'UsersPassword' => Hash::make($request->input('UsersPassword'))
    ]);

    // Logout for good measure.
    AuthClass::logout();

    // Return happy.
    return [ 'status' => 1 ];
  }
}

/* End File */
