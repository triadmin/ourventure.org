<?php

namespace App\Http\Controllers\Centcom;

use DB;
use Hash;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class Accounts extends Controller
{
  //
  // Register.
  //
  public function register()
  {
    return view('centcom.accounts.register', []);
  }

  //
  // Register Post.
  //
  public function register_post(Request $request)
  {
    $input = $request->input();

    // Create account.
    $account_id = DB::table('Accounts')->insertGetId([
      'AccountsDisplayName' => $request->input('AccountsDisplayName'),
      'AccountsUpdatedAt' => date('Y-m-d G:i:s'),
      'AccountsCreatedAt' => date('Y-m-d G:i:s')
    ]);

    // Create User
    $user_id = DB::table('Users')->insertGetId([
      'UsersFirstName' => $request->input('UsersFirstName'),
      'UsersLastName' => $request->input('UsersLastName'),
      'UsersEmail' => $request->input('UsersEmail'),
      'UsersPassword' => Hash::make($request->input('UsersPassword')),
      'UsersUpdatedAt' => date('Y-m-d G:i:s'),
      'UsersCreatedAt' => date('Y-m-d G:i:s')
    ]);

    // Update account with owner.
    DB::table('Accounts')->where('AccountsId', $account_id)->update([ 'AccountsOwnerId' => $user_id ]);

    // Add the lookup table.
    DB::table('AcctUsersLu')->insert([
      'AcctUsersLuAcctId' => $account_id,
      'AcctUsersLuUserId' => $user_id,
      'AcctUsersLuUpdatedAt' => date('Y-m-d G:i:s'),
      'AcctUsersLuCreatedAt' => date('Y-m-d G:i:s')
    ]);

    return "Account Created.";
  }
}

/* End File */
