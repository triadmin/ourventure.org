<?php

// See if this is an api call or not.
if(request()->input('access_token'))
{
  $api_middleware = [ 'auth' ];
} else
{
  $api_middleware = [ 'web', 'auth' ];
}

// Auth Requests
Route::group([ 'middleware' => [ 'web' ] ], function ()
{
  Route::get('login', 'Auth@login');
  Route::get('login/{message}', 'Auth@login');
  Route::get('logout', 'Auth@logout');
  Route::get('forget_password', 'Auth@forget_password');
  Route::get('forget_password/{message}', 'Auth@forget_password');
  Route::get('set_password/{hash}', 'Auth@set_password');
  Route::post('login_post', 'Auth@login_post');
  Route::post('set_password_post/{hash}', 'Auth@set_password_post');
  Route::post('reset_password_post', 'Auth@reset_password_post');
  Route::get('reviewers/{portfolio_type_id}', 'PublicPages@reviewers');
  Route::get('reviewers', 'PublicPages@reviewers');
  Route::get('whatisventurenice', 'PublicPages@whatisventurenice');
});

// ------------ API V1 Requests. (Authed) -------------- //

// This api is for non-web use or 3rd party vendors.
Route::group([ 'prefix' => 'api/v1', 'namespace' => 'Api\V1', 'middleware' => $api_middleware ], function ()
{
  // api/v1/users
  Route::get('users', 'Users@get');
  Route::get('users/type/{user_type}', 'Users@get');
  Route::get('users/me', 'Users@me');
  Route::get('users/{id}', 'Users@id');
  Route::post('users/create', 'Users@create');
  Route::post('users/delete/{id}', 'Users@delete');
  Route::post('users/update/{id}', 'Users@update');
  Route::post('users/update_password/{id}', 'Users@update_password');
  Route::post('users/send_welcome_email/{id}', 'Users@send_welcome_email');

  // api/v1/ptypes
  Route::get('ptypes', 'PTypes@get');
  Route::get('ptypes/{id}', 'PTypes@id');
  Route::post('ptypes/delete/{id}', 'PTypes@delete');
  Route::post('ptypes/create', 'PTypes@create');
  Route::post('ptypes/update/{id}', 'PTypes@update');
  Route::get('ptypes/get_ptype_stats/{ptype_id}', 'PTypes@get_ptype_stats');
  Route::get('ptypes/get_portfolios_by_status/{ptype_id}/{status}', 'PTypes@get_portfolios_by_status');

  Route::get('ptypes/get_candidates_for_ptype/{ptypeid}', 'PTypes@get_candidates_for_ptype');
  Route::get('ptypes/get_mentors_for_ptype/{ptypeid}', 'PTypes@get_mentors_for_ptype');
  Route::get('ptypes/get_reviewers_for_ptype/{ptypeid}', 'PTypes@get_reviewers_for_ptype');
  Route::get('ptypes/get_mentors_reviewers_to_assign/{ptypeid}/{portfolioid}', 'PTypes@get_mentors_reviewers_to_assign');
  Route::post('ptypes/attach_user_to_ptype', 'PTypes@attach_user_to_ptype');
  Route::post('ptypes/remove_user_from_ptype', 'PTypes@remove_user_from_ptype');

  // api/v1/portfolios
  Route::get('portfolios/get_portfolios_for_mentor_reviewer/{user_type}', 'Portfolios@get_portfolios_for_mentor_reviewer');
  Route::get('portfolios/get_portfolios_for_mentor_reviewer/{user_type}/{user_id}', 'Portfolios@get_portfolios_for_mentor_reviewer');
  Route::get('portfolios/get_portfolios_for_dashboard', 'Portfolios@get_portfolios_for_dashboard');
  Route::get('portfolios/get_portfolio_menu/{portfolio_id}', 'Portfolios@get_portfolio_menu');
  Route::get('portfolios/get_portfolio_progress/{portfolio_id}', 'Portfolios@get_portfolio_progress');
  Route::post('portfolios/create', 'Portfolios@create');
  Route::get('portfolios', 'Portfolios@get');
  Route::get('portfolios/{id}', 'Portfolios@id');
  Route::post('portfolios/assign_user_to_portfolio', 'Portfolios@assign_user_to_portfolio');
  Route::get('portfolios/remove_user_from_portfolio/{user_id}', 'Portfolios@remove_user_from_portfolio');
  Route::post('portfolios/update_status', 'Portfolios@update_status');
  Route::get('portfolios/is_user_assigned/{id}', 'Portfolios@is_user_assigned');
  Route::post('portfolios/save_reviewer_comments/{PortfolioId}', 'Portfolios@save_reviewer_comments');
  Route::get('portfolios/mark_governance_reviewed/{PortfolioId}/{UserType}', 'Portfolios@mark_governance_reviewed');

  // api/artifacts
  Route::get('artifacts', 'Artifacts@get');
  Route::get('artifacts/get_media/{id}', 'Artifacts@get_media');
  Route::get('artifacts/{id}', 'Artifacts@id');
  Route::get('artifacts/{id}/{plugin_name}', 'Artifacts@id');
  Route::get('artifacts/get_artifacts_for_standard/{portfolioid}/{standardid}', 'Artifacts@get_artifacts_for_standard');
  Route::post('artifacts/add_media', 'Artifacts@add_media');
  Route::post('artifacts/create', 'Artifacts@create');
  Route::post('artifacts/update/{id}', 'Artifacts@update');
  Route::post('artifacts/delete/{id}', 'Artifacts@delete');
  Route::post('artifacts/check_for_portfolio_start/{ptypeid}', 'Artifacts@check_for_portfolio_start');

  // api/v1/qtypes
  Route::get('qtypes', 'QTypes@get');
  Route::post('qtypes/update/{id}', 'QTypes@update');
  Route::post('qtypes/create', 'QTypes@create');
  Route::post('qtypes/delete/{id}', 'QTypes@delete');
  Route::post('qtypes/update_order', 'QTypes@update_order');
  Route::get('qtypes/get_release_forms', 'QTypes@get_release_forms');

  //Route::get('accounts/get_account_data', 'Accounts@get_account_data');

  // api/v1/qanswers
  Route::get('qanswers', 'QAnswers@get');
  Route::post('qanswers/get_create', 'QAnswers@get_create');
  Route::post('qanswers/update/{id}', 'QAnswers@update');
  Route::post('qanswers/create', 'QAnswers@create');
  Route::post('qanswers/delete/{id}', 'QAnswers@delete');

  // api/v1/standards
  Route::get('standards', 'Standards@get');
  // Get standards for ptype
  Route::get('standards/get_standards/{ptypeid}', 'Standards@get_standards');
  Route::get('standards/{id}', 'Standards@id');
  Route::post('standards/update/{id}', 'Standards@update');
  Route::post('standards/create', 'Standards@create');
  Route::post('standards/delete/{id}', 'Standards@delete');
  Route::get('standards/standards-with-competencies/{id}', 'Standards@get_standards_with_competencies');
  Route::post('standards/update_order', 'Standards@update_order');

  // api/v1/competencies
  Route::get('competencies', 'Competencies@get');
  Route::get('competencies/get_competencies_for_artifact_edit/{ptype_id}/{portfolio_id}', 'Competencies@get_competencies_for_artifact_edit');
  Route::post('competencies/update/{id}', 'Competencies@update');
  Route::post('competencies/create', 'Competencies@create');
  Route::post('competencies/delete/{id}', 'Competencies@delete');
  Route::post('competencies/update_order', 'Competencies@update_order');

  // api/v1/artifactscompetlu
  Route::get('artifactscompetlu', 'ArtifactsCompetLu@get');

  // api/v1/artifactmedialu
  Route::get('artifactmedialu', 'ArtifactMediaLu@get');
  Route::post('artifactmedialu/delete/{id}', 'ArtifactMediaLu@delete');
  Route::post('artifactmedialu/update_order/', 'ArtifactMediaLu@update_order');

  // api/v1/releasemedialu
  Route::get('releasemedialu', 'ReleaseMediaLu@get');
  Route::post('releasemedialu/create', 'ReleaseMediaLu@create');
  Route::post('releasemedialu/delete/{id}', 'ReleaseMediaLu@delete');

  // Remove media from an artifact but don't delete the file.
  Route::post('artifactmedialu/remove/{id}', 'ArtifactMediaLu@remove');
  Route::post('artifactmedialu/save_files', 'ArtifactMediaLu@save_files');

  // api/v1/media
  Route::get('media', 'Media@get');
  Route::get('media/file/{id}/{name}', 'Media@file');
  Route::post('media/update/{id}', 'Media@update');
  Route::post('media/delete/{id}', 'Media@delete');
  Route::post('media/upload_file/{type}', 'Media@upload_file');
  Route::post('media/save_embed', 'Media@save_embed');

  // api/v1/messages
  Route::get('messages/get_my_recipients', 'Messages@get_my_recipients');
  Route::post('messages/send', 'Messages@send');
  Route::post('messages/mark_as_read', 'Messages@mark_as_read');
  Route::get('messages/get_messages/{start}/{limit}', 'Messages@get_messages');
  Route::get('messages/get/{id}', 'Messages@get');

  // api/v1/notifications
  Route::get('notifications', 'Notifications@get');
  Route::post('notifications/create', 'Notifications@create');
  Route::post('notifications/mark_as_read', 'Notifications@mark_as_read');

  // api/v1/artifactscores
  Route::post('artifactscores/score/{ArtifactsId}', 'ArtifactScores@score');
  Route::get('artifactscores/get_score_for_artifact/{ArtifactsId}/{plugin_name}', 'ArtifactScores@get_score_for_artifact');
  Route::get('artifactscores/get_scores_for_artifact/{ArtifactsId}/{plugin_name}', 'ArtifactScores@get_scores_for_artifact');
  Route::get('artifactscores/get_scores_for_portfolio/{PortfolioId}/{PTypesId}', 'ArtifactScores@get_scores_for_portfolio');
  Route::get('artifactscores/check_for_scores/{PortfolioId}', 'ArtifactScores@check_for_scores');
  Route::get('artifactscores/reviewer_mark_scoring_complete/{PortfolioId}', 'ArtifactScores@reviewer_mark_scoring_complete');

  // api/v1/contact
  Route::post('contact/create', 'Contact@create');
  Route::get('contact/get', 'Contact@get');

  // api/v1/accounts
  Route::get('accounts/get_account_data', 'Accounts@get_account_data');
});

// ------------ API V1 Requests. (Non-Authed) -------------- //
Route::group([ 'prefix' => 'public/api/v1', 'namespace' => 'Api\V1', 'middleware' => ['web'] ], function ()
{
  Route::get('ptypes/get_reviewers_for_ptype/{ptypeid}', 'PTypes@get_reviewers_for_ptype');
  Route::get('ptypes/get_ptype/{ptypeid}', 'PTypes@get_ptype');
});

// ------- App Routes -------------------- //

// Web app Requests. (Authed)
Route::group([ 'middleware' => [ 'web', 'auth' ] ], function ()
{
  // Default
  Route::get('/', 'AppController@template');

  // dashboard
  Route::get('/dashboard', 'AppController@template');

  // portfolios
  Route::get('/portfolios', 'AppController@template');
  Route::get('/portfolios/overview/{id}', 'AppController@template');
  Route::get('/portfolios/aboutme/{portfolioid}', 'AppController@template');
  Route::get('/portfolios/scores/{portfolioid}', 'AppController@template');

  // artifacts
  // id: portfolioid
  // section: standardsid
  Route::get('/portfolios/artifact/{id}/{section}', 'AppController@template');
  Route::get('/portfolios/artifact/{id}/{section}/{artifactid}', 'AppController@template');
  Route::get('/portfolios/artifact-edit/{ptypeid}/{id}/{section}', 'AppController@template');
  Route::get('/portfolios/artifact/add', 'AppController@template');

  // portfolio-types
  Route::get('/portfolio-types/portfolio-manager', 'AppController@template');
  Route::get('/portfolio-types/portfolio-add-edit', 'AppController@template');
  Route::get('/portfolio-types/portfolio-add-edit/{id}', 'AppController@template');
  Route::get('/portfolio-types/standards-competencies-add-edit/{id}', 'AppController@template');
  Route::get('/portfolio-types/artifact-elements-add-edit/{id}', 'AppController@template');
  Route::get('/portfolio-types/portfolio-view/{id}', 'AppController@template');
  Route::get('/portfolio-types/portfolio-people/{id}', 'AppController@template');
  Route::get('/portfolio-types/portfolio-people/{id}/{user_type}', 'AppController@template');
  Route::get('/portfolio-types/portfolio-people/{id}/{user_type}/{user_id}', 'AppController@template');

  // admin
  Route::get('/admin/account-manager', 'AppController@template');
  Route::get('/admin/account-add-edit', 'AppController@template');

  // users
  Route::get('/users', 'AppController@template');
  Route::get('/users/view/{id}', 'AppController@template');
  Route::get('/users/add', 'AppController@template');
  Route::get('/users/edit/{id}', 'AppController@template');
  Route::get('/users/profile/{id}', 'AppController@template');

  // file manager
  Route::get('/files', 'AppController@template');

  // help and tutorials
  Route::get('/help', 'AppController@template');
  Route::get('/help/contact-list', 'AppController@template');
  Route::get('/help/reviewer-training', 'AppController@template');

  // Contact
  Route::get('/contact', 'AppController@template');

  // Special (secret) url to allow you to auto login as a user.
  Route::get('b6d32a6ed04c4908755fd3626ed7a81f/login/{id}', 'AppController@admin_login');
});

// ------------ Centcom Routes -------------- //
Route::group([ 'prefix' => 'centcom', 'namespace' => 'Centcom', 'middleware' => [  ] ], function ()
{
  // centcom/accounts
  Route::get('/accounts/register', 'Accounts@register');
  Route::post('/accounts/register_post', 'Accounts@register_post');

});

/* End File */
