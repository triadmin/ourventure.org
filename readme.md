## ourventure.org


# Gulp 

https://laravel.com/docs/5.2/elixir

Make sure you have gulp installed

npm install --global gulp-cli

Install the NPM packages

npm install

If you want to concat all css and js just run "gulp"

If you want to auto run gulp whenever something changes run this "gulp watch"

If you want to add a new css / js file you add it in "gulpfile.js".


Side Note: This has only been working with v6.14.4 some helpful commands for OSX

```brew unlink node```

```brew install node@6```

```brew link --overwrite --force node@6```

