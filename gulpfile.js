var elixir = require('laravel-elixir');

elixir(function(mix) {

  mix.styles([
    'app/bower/bootstrap/dist/css/bootstrap.min.css',
    'app/font-awesome/css/font-awesome.css',
    'app/css/animate.css',
    'app/css/style.css',
    'app/css/plugins/toastr/toastr.min.css',
    'app/css/plugins/chosen/chosen.css',
    'app/css/plugins/datepicker/angular-datepicker.min.css',
    'app/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css',
    'app/css/plugins/textAngular/textAngular.css',
    'app/css/plugins/ng-sortable/ng-sortable.css',
    'app/css/styles_app.css'
  ], 'public/assets/css/app.css', 'public/');

  mix.scripts([ 
    'app/bower/jquery/dist/jquery.min.js',
    'app/plugins/pdf/pdf.js',
    'app/bower/angular/angular.js',
    'app/bower/angular-route/angular-route.min.js',
    'app/bower/angular/angular-animate.min.js',
    'app/bower/ng-file-upload-shim/ng-file-upload-shim.min.js',
    'app/bower/ng-file-upload/ng-file-upload.js',
    'app/bower/bootstrap/dist/js/ui-bootstrap-tpls-1.1.2.min.js',
    'app/inspinia/inspinia.js',
    'app/plugins/textAngular/textAngular-rangy.min.js',
    'app/plugins/textAngular/textAngular-sanitize.min.js',
    'app/plugins/textAngular/textAngular.min.js',
    'app/plugins/ng-sortable/ng-sortable.min.js',
    'app/bower/moment/min/moment.min.js',
    'app/bower/moment-timezone/builds/moment-timezone.min.js',
    'app/plugins/toastr/toastr.min.js',
    'app/config/app.js',
    'app/config/routes.js',
    'app/config/text-angular.js',
    'app/filters/filters.js',
    'app/services/services.js',
    'app/directives/qtypes.js',
    'app/directives/directives.js',
    'app/directives/directives-forms.js',
    'app/bower/angular-moment/angular-moment.min.js',
    'app/helpers/helpers.js',
    
    'app/controllers/users.js',
    'app/controllers/user-profile.js',
    'app/controllers/dashboard.js',
    'app/controllers/common.js',
    'app/controllers/modals.js',
    'app/controllers/accounts.js',
    'app/controllers/portfolio.js',
    'app/controllers/portfolio-overview.js',
    'app/controllers/portfolio-manager.js',
    'app/controllers/portfolio-view.js',
    'app/controllers/file-manager.js',
    'app/controllers/messages.js',
    'app/controllers/notifications.js',
    'app/controllers/users-resume.js',
    'app/controllers/users-select-assign.js',
    'app/controllers/custom-questions.js',
    'app/controllers/artifact.js',
    'app/controllers/artifact-add-edit.js',
    'app/controllers/scoring.js',
    'app/controllers/help.js',
    'app/jquery/app-jquery.js'
  ], 'public/assets/js/app.js', 'public/');

  mix.copy('public/app/images', 'public/build/assets/images');

  mix.copy('public/app/fonts', 'public/build/assets/fonts');

  mix.copy('public/app/font-awesome/fonts', 'public/build/assets/fonts');

  mix.version([ 'assets/css/app.css', 'assets/js/app.js' ], 'public/build');

});
