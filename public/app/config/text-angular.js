//
// textAngular toolbar options. See...
// https://github.com/fraywing/textAngular/wiki/Setting-Defaults
//
app.config(['$provide', function( $provide ) {
  $provide.decorator('taOptions', ['$delegate', function(taOptions)
  {
    // Suppresses default tab key behavior so that the focus trap is fixed.
    taOptions.keyMappings = [
            { commandKeyCode: 'TabKey', testForKey: function (event) { return false; }
            },

            { commandKeyCode: 'ShiftTabKey', testForKey: function (event) {  return false; }
            }

        ],  
      
    taOptions.toolbar = [
      ['p', 'h2', 'h3', 'quote'],
      ['bold', 'italics', 'underline', 'ul', 'ol'],
      ['insertLink']
    ];

    return taOptions;
  }]);
}]);
