app.config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {

$routeProvider.
  // /dashboard
  when('/dashboard', {
    templateUrl: '/app/views/dashboard/dashboard.html',
    controller: 'DashboardCtrl'
  }).

  // /portfolios

  // /portfolios/overview - The overview screen for
  // an individual portfolio
  // NOTE: :portfolio_type could be title or id
  when('/portfolios/overview/:portfolio_id', {
    templateUrl: '/app/views/portfolios/overview.html',
    controller: 'PortfolioOverviewCtrl'
  }).

  //
  // PORTFOLIOS: these are routes that candidates will use
  //             to create their portfolios
  //
  // /portfolios/artifact - Puts us in the artifact view
  // at either the Standard level or takes us to an individual
  // artifact.
  when('/portfolios/artifact/:portfolio/:section', {
    templateUrl: '/app/views/portfolios/artifact.html',
    controller: 'ArtifactCtrl'
  }).

  when('/portfolios/artifact/:portfolio/:section/:artifactid', {
    templateUrl: '/app/views/portfolios/artifact.html',
    controller: 'ArtifactCtrl'
  }).

  when('/portfolios/artifact-edit/:ptypeid/:portfolio/:artifactid', {
    templateUrl: '/app/views/portfolios/artifact-add-edit.html',
    controller: 'ArtifactAddEditCtrl'
  }).

  when('/portfolios/aboutme/:portfolio', {
    templateUrl: '/app/views/portfolios/about-me.html',
    controller: 'UsersResumeCtrl'
  }).

  when('/portfolios/scores/:portfolio', {
    templateUrl: '/app/views/portfolios/scores.html',
    controller: 'PortfolioScoresCtrl'
  }).

  //
  // PORTFOLIO TYPES: these are routes that account owners will use
  //                  to create portfolio types.
  //
  // /portfolio-manager: Account Owner
  when('/portfolio-types/portfolio-manager', {
    templateUrl: '/app/views/portfolio-types/portfolio-manager.html',
    controller: 'PortfolioTypeMngrCtrl'
  }).

  when('/portfolio-types/portfolio-add-edit/:id', {
    templateUrl: '/app/views/portfolio-types/portfolio-add-edit.html',
    controller: 'PortfolioTypeAddEditCtrl'
  }).

  when('/portfolio-types/portfolio-add-edit', {
    templateUrl: '/app/views/portfolio-types/portfolio-add-edit.html',
    controller: 'PortfolioTypeAddEditCtrl'
  }).

  when('/portfolio-types/standards-competencies-add-edit/:id', {
    templateUrl: '/app/views/portfolio-types/standards-competencies-add-edit.html',
    controller: 'PortfolioStandardsCompetenciesCtrl'
  }).

  when('/portfolio-types/artifact-elements-add-edit/:id', {
    templateUrl: '/app/views/portfolio-types/artifact-elements-add-edit.html',
    controller: 'QTypeMngrCtrl'
  }).

  when('/portfolio-types/candidate-resume-add-edit/:id', {
    templateUrl: '/app/views/portfolio-types/candidate-resume-add-edit.html',
    controller: 'QTypeMngrCtrl'
  }).

  when('/portfolio-types/portfolio-view/:ptype_id', {
    templateUrl: '/app/views/portfolio-types/portfolio-view.html',
    controller: 'PortfolioTypeViewCtrl'
  }).

  when('/portfolio-types/portfolio-people/:ptype_id/:user_type/:user_id', {
    templateUrl: '/app/views/portfolio-types/portfolio-people.html',
    controller: 'PortfolioTypeViewCtrl'
  }).

  when('/portfolio-types/portfolio-people/:ptype_id', {
    templateUrl: '/app/views/portfolio-types/portfolio-people.html',
    controller: 'PortfolioTypeViewCtrl'
  }).

  when('/portfolio-types/assign-support/:ptype_id', {
    templateUrl: '/app/views/portfolio-types/portfolio-assign-support.html',
    controller: 'PortfolioTypeAssignSupportCtrl'
  }).

  // /users
  when('/users', {
    templateUrl: '/app/views/users/users-list.html',
    controller: 'UsersCtrl'
  }).

  // /users/add
  when('/users/add', {
    templateUrl: '/app/views/users/user-add-edit.html',
    controller: 'UserAddEditCtrl'
  }).

  when('/users/edit/:id', {
    templateUrl: '/app/views/users/user-add-edit.html',
    controller: 'UserAddEditCtrl'
  }).

  // /users/profile/:id
  when('/users/profile/:id', {
    templateUrl: '/app/views/users/user-profile.html',
    controller: 'UserProfileCtrl'
  }).

  // Admin Specific Routes

  // /accounts
  when('/admin/account-manager', {
    templateUrl: '/app/views/admin/account-manager.html',
    controller: 'AccountsCtrl'
  }).

  when('/admin/account-add-edit', {
    templateUrl: '/app/views/admin/account-add-edit.html',
    controller: 'AccountsCtrl'
  }).

  // File Manager
  when('/files', {
    templateUrl: '/app/views/files/file-manager.html',
    controller: 'FilesCtrl'
  }).

  // Help and Tutorials
  when('/help', {
    templateUrl: '/app/views/help/index.html',
    controller: 'HelpCtrl'
  }).
  when('/contact', {
    templateUrl: '/app/views/help/index.html',
    controller: 'HelpCtrl'
  }).
  when('/help/contact-list', {
    templateUrl: '/app/views/help/contact-list.html',
    controller: 'HelpContactListCtrl'
  }).
  when('/help/reviewer-training', {
    templateUrl: '/app/views/help/reviewer-training.html',
    controller: 'HelpCtrl'
  }).

  // Logout
  when('/logout', {
    templateUrl: '',
    controller: 'LogoutCtrl'
  }).

  otherwise({ redirectTo: '/dashboard' });

// HTML 5 Mode
$locationProvider.html5Mode(true);

}]);
