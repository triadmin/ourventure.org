 var app = angular.module('app', [
  'ngRoute',
  'ui.bootstrap',
  'angularMoment',
  'ngAnimate',
  'textAngular',
  'ngFileUpload',
  'toaster',
  'as.sortable'
]);

/*
// Not sure why this is here.
app.constant('angularMomentConfig', {
  timezone: 'America/Los_Angeles|PST PDT|80 70|0101|1Lzm0 1zb0 Op0'
});
*/

//
// CONSTANTS
//
app.constant( 'CONSTANTS', {
  tplDir: 'app/views/common/templates/',
  keyCodes: {
    esc: 27,
    space: 32,
    enter: 13,
    tab: 9,
    backspace: 8,
    shift: 16,
    ctrl: 17,
    alt: 18,
    capslock: 20,
    numlock: 144
  },
  questionTypes: [
    // The html files for these question_types are located in
    // app/directives/qtypes
    { type: 'heading', displayName: 'Heading' },
    { type: 'spacer', displayName: 'Spacer' },
    { type: 'textarea', displayName: 'Textbox (Multi-line)' },
    { type: 'textbox', displayName: 'Textbox (One line)' },
    { type: 'select', displayName: 'Select Menu' },
    { type: 'checkbox', displayName: 'Checkboxes' },
    { type: 'checkbox-other', displayName: 'Checkboxes with Other' },
    { type: 'radio', displayName: 'Radio Buttons' },
    { type: 'radio-other', displayName: 'Radio Buttons with Other' },
    { type: 'date', displayName: 'Date Field' },
    { type: 'file-upload', displayName: 'File Upload' },
    { type: 'files-upload', displayName: 'File Upload (multi)' },
    { type: 'video', displayName: 'Video' },

    // PType custom question types
    { type: 'who-i-serve', displayName: 'Who I Serve: Intervener Certification' },
    { type: 'release-form', displayName: 'Release Form Upload' }
  ]
});

//
// Site wide controller.
//
app.controller('SiteWideCtrl', function ($scope, $rootScope, $http, toaster, $window, $location, $anchorScroll) {
  // This is where the user object will be
  $rootScope.me = {};
  $scope.standards = {};
  $rootScope.competencies = {};
  $rootScope.messages_unread = 0;
  $rootScope.new_notifications = 0;
  $rootScope.scoring_plugins = site.scoring_plugins;
  $rootScope.masquerade_secret_url = site.masquerade_secret_url;

  $scope.refresh_loggedin_user = function()
  {
    // This is where we will do AJAX call to update user
    // in case they change their user data.
    // This functionw will be ther receiver for a broadcast event.

    // This is where we're also get any other persistenct client data (e.g. standards
    // competencies, etc.)

    // The Angular way is to put these in services but then we'd have to
    // inject the service any time we wanted to use the data. Not a big
    // deal but thinking rootScope is simpler.

    // Get me object
    $http.get('/api/v1/users/me/' + '?account_id=' + site.account_id).success(function (json) {
      $rootScope.me = json.data;
      $scope.$broadcast( 'me_loaded' );

      // Should we load some account data?
      if ( $rootScope.me.IsAdmin == 'Yes' )
      {
        $scope.get_account_data();
      }
    });
  }

  $scope.get_account_data = function()
  {
    $http.get('/api/v1/accounts/get_account_data/' + '?account_id=' + site.account_id).success(function (json) {
      $rootScope.account = json.data;
    });
  }

  $rootScope.go_to_user_account = function( usersid )
  {
    $window.location.href = '/' + site.masquerade_secret_url + '/login/' + usersid;
  }

  $scope.refresh_loggedin_user();

  // Required for skip to main content since Angular hijacks the link
  $scope.scrollTo = function(target_link){
    $anchorScroll(target_link);
  }

});
