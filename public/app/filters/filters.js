//
// Convert to date object.
//
app.filter('dateToISO', function() {
  return function(input) {

    if ( typeof input === 'undefined' )
    {
      return '';
    }

    input = new Date(input).toISOString();
    return input;
  };
});

app.filter('embedUrl', function ( $sce ) {
  return function( embedURL ) {
    return $sce.trustAsResourceUrl( embedURL );
  };
});

app.filter('getYouTubeVideoId', function() {
  return function(yt_url) {

  var regExp = /.*(?:youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=)([^#\&\?]*).*/;
  var match = yt_url.match( regExp );
  return ( match && match[1].length == 11) ? match[1] : false;
 }
});

//
// Filters out all duplicate items from an array by checking the specified key
// @param [key] {string} the name of the attribute of each object to compare for uniqueness
// if the key is empty, the entire object will be compared
// if the key === false then no filtering will be performed
// @return {array}
//
app.filter('unique', function () {

  return function (items, filterOn) {

    if (filterOn === false) {
      return items;
    }

    if ((filterOn || angular.isUndefined(filterOn)) && angular.isArray(items)) {
      var hashCheck = {}, newItems = [];

      var extractValueToCompare = function (item) {
        if (angular.isObject(item) && angular.isString(filterOn)) {
          return item[filterOn];
        } else {
          return item;
        }
      };

      angular.forEach(items, function (item) {
        var valueToCheck, isDuplicate = false;

        for (var i = 0; i < newItems.length; i++) {
          if (angular.equals(extractValueToCompare(newItems[i]), extractValueToCompare(item))) {
            isDuplicate = true;
            break;
          }
        }
        if (!isDuplicate) {
          newItems.push(item);
        }

      });
      items = newItems;
    }
    return items;
  };
});