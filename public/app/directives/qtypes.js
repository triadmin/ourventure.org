//
// Directive for displaying qtypes throughout the app.
//
app.directive('qtypes', function() {

  return {
    restrict: 'EA',

    scope: {
      type: '@',
      ptype: '@',
      disabled: '=',
      artifactid: '@',
      userid: '@'
    },

    templateUrl: '/app/directives/qtypes/index.html',

    // Constroller for the directive.
    controller: function ($scope, $rootScope, $http, $interval, toaster, Upload) {
      $scope.files = []
      $scope.answers = {}
      $scope.release_forms = {}
      $scope.file_is_uploading = false;
      $scope.uploading_files = [];
      $scope.questions = [];

      // Watch for file drag and drop
      $scope.$watch('files', function () {
        $scope.uploadFiles($scope.files);
      });

      // For file upload.
      $scope.uploadFiles = function (files, QTypesId, QAnswersId, QTypesType, Multi, index)
      {
        if(typeof $scope.answers[QTypesId] != "undefined")
        {
          $scope.answers[QTypesId].QAnswersValues.image = { value: 'Uploading' }

          if( index == 0 && Multi == 'Yes' && files != 'delete' )
          {
            //$scope.file_is_uploading = true;
          }
        }

        // Is this a delete call?
        if(files == 'delete')
        {
          if(Multi == 'No')
          {
            $scope.answers[QTypesId].QAnswersValues.image = { url: '', name: '', id: '' }
          } else
          {
            $scope.answers[QTypesId].QAnswersValues.images.splice(index, 1);
          }

          $scope.qtype_change(QTypesId, QAnswersId, QTypesType);
          return true;
        }

        // Loop through the files and upload them.
        if(files && files.length)
        {
          for( var i = 0; i < files.length; i++ )
          {
            // Add to the list of files we are uploading in the UI.
            files[i].progress = 0;
            $scope.uploading_files.push(files[i]);

            $scope.file_is_uploading = QAnswersId;

            // Upload file
            var upload = Upload.upload({
              url: '/api/v1/media/upload_file/other',
              data: { file: files[i] },
              index: i
            }).then(function (qfiles)
            {
              if(Multi == 'No')
              {
                $scope.answers[QTypesId].QAnswersValues.image = { url: qfiles.data.data.Url, name: qfiles.data.data.Name, id: qfiles.data.data.Id  }
              } else
              {
                if(! $scope.answers[QTypesId].QAnswersValues.images)
                {
                  $scope.answers[QTypesId].QAnswersValues.images = []
                }

                $scope.answers[QTypesId].QAnswersValues.images.push({ url: qfiles.data.data.Url, name: qfiles.data.data.Name, id: qfiles.data.data.Id });
              }

              // Store file.
              $scope.qtype_change(QTypesId, QAnswersId, QTypesType);
              $scope.file_is_uploading = false;

            }, function (resp)
            {
              //console.log(resp);
            }, function (evt)
            {
              //console.log(evt);
            });
          }
        }
      }

      // On change. This is where we do all of our
      // answer saving. Blank answers are created already
      // by the answers_check method below.
      $scope.qtype_change = function (QTypesId, QAnswersId, QTypesType)
      {
        // Deal with any special cases like...
        // Videos
        switch(QTypesType)
        {
          case 'video':
            var media_obj = helpers.format_video_embed( $scope.answers[QTypesId].QAnswersValues.value );
            $scope.answers[QTypesId].QAnswersValues.value = media_obj.embed_url;
          break;
        }

        var post = {
          QAnswersValues: $scope.answers[QTypesId].QAnswersValues,
          account_id: site.account_id
        }

        // Send update to server.
        $http.post('/api/v1/qanswers/update/' + QAnswersId, post).success(function (json) {
          toaster.pop({ type: 'success', body: $scope.type + ' saved.', toasterId: 'toaster-activity' });
        });
      }

      // Check answers to see if we need to create them
      // This is where dragons live!!!!
      // Somewhere in here is where we're getting the billions of records created.
      $scope.answers_check = function (QTypesId)
      {
        var post = {
          QAnswersQTypesId: QTypesId,
          QAnswersUserId: $scope.userid,
          account_id: site.account_id,
          QAnswersValues: $scope.answers[QTypesId],
          QAnswersArtifactsId: 0
        }

        if($scope.type == 'Artifacts')
        {
          post.QAnswersArtifactsId = $scope.artifactid;
        }

        $http.post('/api/v1/qanswers/get_create?col_QAnswersQTypesId=' + QTypesId + '&col_QAnswersUserId=' + $scope.userid + '&account_id=' + site.account_id + '&col_QAnswersArtifactsId=' + $scope.artifactid, post).success(function (json)
        {
          $scope.answers[json.data.QAnswersQTypesId] = json.data;
        });
      }

      // Load the questions
      // Here there be dragons! This might be where we're seeing
      // users get the wrong responses to their About Me page stuff.
      // BUG!!! We're not getting scope.ptype here for some reason.
      // July 2017 - this has been resolved.
      $scope.load_questions = function ()
      {
        /*
          Here we are ensuring that we have BOTH
          a ptype and artifactid before loading questions.
          This solves the problem of loading questions from 
          all portfolios in the system if we have ptype == 0;
        */
        if($scope.ptype <= 0)
        {
          return;
        }

        /*
          However, if we're on the About Me page, we're not
          expecting an artifactid so put a conditional here.
        */

        if($scope.artifactid <= 0 && $scope.type == 'Artifacts')
        {
          return;
        }

        $http.get('/api/v1/qtypes/?col_QTypesWhich=' + $scope.type + '&col_QTypesPTypesId=' + $scope.ptype + '&order=QTypesOrder&sort=asc' + '&account_id=' + site.account_id).success(function (json)
        {
          $scope.questions = json.data;
          var questions = json.data;

          for ( var i = 0; i < questions.length; i++ )
          {
            var value = '';
            var values = [];

            if ( typeof questions[i].Values == 'object' )
            {
              for (var k = 0; k < questions[i].Values.length; k++)
              {
                values[k] = 'No';
              }

              if ( values.length )
              {
                value = questions[i].Values[0];
              }
            }

            // No need to run answers_check against QTypes of 'spacer' or 'heading'
            if ( questions[i].QTypesType !== 'spacer' && questions[i].QTypesType !== 'heading' )
            {
              $scope.answers[questions[i].QTypesId] = { other: '',  value: value, values: values, QAnswersValues: { images: [] } }
              $scope.answers_check( questions[i].QTypesId );
            }
          }
        });
      }

      /*
       * Stuff for Who I Serve Release Forms.
       * THIS IS NOT DRY - WE NEED TO MOVE THIS AND THE
       * CODE IN artifact_add_edit.js TO A COMMON
       * CONTROLLER.
       */
      $scope.get_release_forms = function ()
      {
        $http.get('/api/v1/qtypes/get_release_forms').success(function (json)
        {
          $scope.release_forms = json.data.ReleaseForms;
        });
      }

      $scope.release_form_delete = function (row)
      {
        $http.post('/api/v1/releasemedialu/delete/' + row.ReleaseMediaLuId, { Id: row.ReleaseMediaLuId }).success(function (json) {
          $scope.get_release_forms();
        });
      }

      // Data to load on start.
      $scope.$watch('artifactid', function(value) {
        if(value.length)
        {
          $scope.load_questions();
          $scope.get_release_forms();
        }
      });

      $scope.$watch('ptype', function(value) {
        if(value.length)
        {
          $scope.load_questions();
          $scope.get_release_forms();
        }
      });

      $rootScope.$on('refresh_release_form_qtype_files', function (event, args) {
        $scope.get_release_forms();
      });

      $scope.load_questions();
      $scope.get_release_forms();      
    }
  };

});
