angular
  .module('app')

  .directive( 'fieldError', function( CONSTANTS )
  {
    return {
      restrict: 'AE',
      templateUrl: CONSTANTS.tplDir + 'tpl-form-field-error.html',
      replace: true,
      scope: {
        message: '@'
      }
    }
  })

  .directive('backgroundImg', function()
  {
    return function(scope, element, attrs) {
      attrs.$observe('backgroundImg', function(value) {
        element.css({
          'background-image': 'url(' + value +')',
          'background-size' : 'cover'
        });
      });
    };
  })

  // Remove DOM element on click
  .directive("removeMe", function($rootScope) {
    return {
      link:function(scope, element, attrs)
      {
        element.bind("click",function() {
          element.remove();
        });
      }
    }
  })

  //
  // Bind keys to form elements.
  // Use Example: key-bind="{ enter: 'update_standards(row)', tab: 'update_standards(row)' }
  //
  .directive('keyBind', function (  CONSTANTS ) {
    var keyCodes = CONSTANTS.keyCodes;
    function map(obj) {
      var mapped = {};
      for (var key in obj) {
        var action = obj[key];
        if (keyCodes.hasOwnProperty(key)) {
          mapped[keyCodes[key]] = action;
        }
      }
      return mapped;
    }

    return function (scope, element, attrs) {
      var bindings = map(scope.$eval(attrs.keyBind));
      element.bind("keydown", function (event) {
        if (bindings.hasOwnProperty(event.which)) {
          scope.$apply(function() {
            scope.$eval(bindings[event.which]);
          });
        }
      });
    };
  });
