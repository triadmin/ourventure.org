var app_jquery = {};

app_jquery.init = function()
{
  // Change row color on checkbox click
  $('body').on('click', '.toggle-row-class', function() {
    var $this = $(this);
    if ( $this.is(':checked') )
    {
      $this.closest('tr').addClass('highlight-row');
    } else {
      $this.closest('tr').removeClass('highlight-row');
    }
  });

  // Change row color on checkbox click
  $('body').on('click', '.toggle-file-select', function() {
    var $this = $(this);
    if ( $this.hasClass('highlight-box') )
    {
      $this.removeClass('highlight-box');
      $this.find('.check').addClass('hide');
    } else {
      $this.addClass('highlight-box');
      $this.find('.check').removeClass('hide');
    }
  });
}
