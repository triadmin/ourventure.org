var helpers = {};

helpers.cap_first_letter = function( str )
{
  return str[0].toUpperCase() + str.slice(1);
}

// format_video_embed: takes a video URL and formats it to
// go in an embed iframe.
helpers.format_video_embed = function( video_url )
{
  // Which kind of URL do we have? YouTube or Vimeo.
  var pat_vimeo = /(?:http?s?:\/\/)?(?:www\.)?(?:vimeo\.com)\/?(.+)/g;
  var pat_yt = /(?:http?s?:\/\/)?(?:www\.)?(?:youtube\.com|youtu\.be)\/(?:watch\?v=)?(.+)/g;
  var embed_url = '', media_name = '', media_type = '';

  if( pat_vimeo.test( video_url ) )
  {
    var repl = '//player.vimeo.com/video/$1';
    embed_url = video_url.replace(pat_vimeo, repl);
    media_name = 'Vimeo video';
    media_type = 'video/vimeo';
  } else if ( pat_yt.test( video_url ) ) {
    var repl = 'https://www.youtube.com/embed/$1';
    embed_url = video_url.replace(pat_yt, repl);
    media_name = 'YouTube video';
    media_type = 'video/youtube';
  } else {
    // Catch error here
    return false;
  }

  var media_obj = {
    embed_url: embed_url,
    media_name: media_name,
    media_type: media_type
  };
  return media_obj;
}
