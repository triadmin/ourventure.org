//
// UsersSelectCtrl: select users to be associated with
// a PType: candidates, mentors, reviewers.
//
app.controller('UsersSelectCtrl', function( $scope, $rootScope, $http, toaster ) {
  $scope.selected = {};

  $scope.save_selected_users = function( user_type, ptype_id )
  {
    if ( user_type == 'candidates' )
    {
      // Candidates get a Portfolio created for them.
      angular.forEach( $scope.selected, function(value, key)
      {
        $scope.fields = {
          PortfoliosUserId: key,
          PortfoliosPTypeId: ptype_id
        }
        // Save selected candidates (create portfolios for them)
        $http.post( '/api/v1/portfolios/create?account_id=' + site.account_id, $scope.fields ).success(function (json) {
          $rootScope.$emit( 'close_modal' );
          $rootScope.$emit( 'refresh_candidates', { ptypeid: ptype_id } );

          toaster.pop({ type: 'success', body: 'Candidate portfolios were added.', toasterId: 'toaster-activity' });
        });
      });
    } else {
      // Reviewers and Mentors get an entry in the PTypeUserLu table.
      angular.forEach( $scope.selected, function(value, key)
      {
        if ( user_type == 'mentors' )
          var PTypeUserLuUserType = 'Mentor';

        if ( user_type == 'reviewers' )
          var PTypeUserLuUserType = 'Reviewer';

        $scope.fields = {
          PTypeUserLuUserId: key,
          PTypeUserLuPTypeId: ptype_id,
          PTypeUserLuUserType: PTypeUserLuUserType
        }
        // Save selected users.
        $http.post( '/api/v1/ptypes/attach_user_to_ptype?account_id=' + site.account_id, $scope.fields ).success(function (json) {
          if ( json.data.new_select_entry === true )
          {
            $rootScope.$emit( 'close_modal' );
            $rootScope.$emit( 'refresh_' + user_type );

            toaster.pop({ type: 'success', body: helpers.cap_first_letter(user_type) + ' were added to this portfolio type.', toasterId: 'toaster-activity' });
          } else {
            // TODO: display error message.
            console.log('you tried to add a mentor that was already selected for this portfolio type.');
          }
        });
      });
    };
  };
});

//
// UsersAssignCtrl: Assign mentors and reviewers to candidates.
//
app.controller('UsersAssignCtrl', function( $scope, $rootScope, $http, toaster ) {
  $scope.selected_mentors = {};
  $scope.selected_reviewers = {};
  $scope.save_assigned_users = function( portfolio_id )
  {
    angular.forEach( $scope.selected_mentors, function(value, key)
    {
      if ( value === true )
      {
        $scope.fields = {
          PortfolioUserLuUserId: key,
          PortfolioUserLuPortfolioId: portfolio_id,
          PortfolioUserLuUserType: 'mentor'
        };
        console.log($scope.fields);
        save_to_server( $scope.fields );
      }
    });

    angular.forEach( $scope.selected_reviewers, function(value, key)
    {
      if ( value === true )
      {
        $scope.fields = {
          PortfolioUserLuUserId: key,
          PortfolioUserLuPortfolioId: portfolio_id,
          PortfolioUserLuUserType: 'reviewer'
        };
        console.log($scope.fields);
        save_to_server( $scope.fields );
      }
    });
  };

  function save_to_server( fields )
  {
    // Save selected assignments (mentors and reviewers)
    $http.post( '/api/v1/portfolios/assign_user_to_portfolio?account_id=' + site.account_id, fields ).success(function (json) {
      if ( json.data.new_assignment === true )
      {
        $rootScope.$broadcast( 'close_modal' );
        $rootScope.$broadcast( 'refresh_candidates' );
        $rootScope.$broadcast( 'refresh_mentors' );
        $rootScope.$broadcast( 'refresh_reviewers' );

        toaster.pop({ type: 'success', body: 'Mentor/Reviewer assigned to candidate.', toasterId: 'toaster-activity' });
      } else {
        // TODO: display error message.
        console.log('this user is already assigned to this candidate.');
      }
    });
  }
});
