//
// Portfolio Manager
//
app.controller('PortfolioTypeMngrCtrl', function ($scope, $http, $rootScope, $window)
{
  $scope.ptypes = [];

  // Listen for delete
  $rootScope.$on('delete_portfolio_type', function(event, data)
  {
    // Send delete request to server
    $http.post('/api/v1/ptypes/delete/' + data.Id + '?account_id=' + site.account_id).success(function (json) {
      $scope.refresh();
    });

    // Redirect to portfolio type manager page.
    $window.location.href = '/portfolio-types/portfolio-manager';
  });

  // Get ptypes
  $scope.refresh = function ()
  {
    // Get pytypes
    $http.get('/api/v1/ptypes?order=PTypesTitle&sort=asc&account_id=' + site.account_id).success(function (json) {
      $scope.ptypes = json.data;
    });
  }

  $scope.refresh();

});

//
// Add View.
//
app.controller('PortfolioTypeAddEditCtrl', function ($scope, $rootScope, $http, $location, toaster, $routeParams)
{
  $scope.errors = {}
  $scope.edit = false;

  // Starter data.
  $scope.fields = {
    PTypesTitle: '',
    PTypesBody: '',
    PTypesScoringPlugin: '',
    PTypesStartDate: '',
    PTypesEndDate: '',
    PTypesIOAThreshold: '80',
    PTypesMentor: false,
    PTypesViewScores: false,
    PTypesSharing: false,
    PTypesMinimumPrompts: '',
    PTypesAllowVideo: false
  }

  $scope.header = {};
  $scope.dates = {};

  // Are we in edit mode?
  if($routeParams.id)
  {
    // We are in edit mode.
    $scope.edit = true;

    // Get data on server.
    $http.get('/api/v1/ptypes/' + $routeParams.id + '&account_id=' + site.account_id).success(function (json) {
      $scope.fields = json.data;

      // Update header variables
      $scope.header.page_title = 'Update Portfolio Type: ' + $scope.fields.PTypesTitle;
      $scope.header.ptype_id = $routeParams.id;
      $scope.header.ptype_title = $scope.fields.PTypesTitle;

      // Because the stupid date picker requires a JS Date object
      var start_date = $scope.fields.PTypesStartDate;
      var end_date = $scope.fields.PTypesEndDate;
      if ( start_date != '0000-00-00' )
      {
        $scope.dates.start_date = new Date( start_date );
      }
      if ( end_date != '0000-00-00' )
      {
        $scope.dates.end_date = new Date( end_date );
      }

      // Becuase the stupid ui has buttons.
      if($scope.fields.PTypesMentor == 'Yes')
      {
        $scope.fields.PTypesMentor = true;
      } else
      {
        $scope.fields.PTypesMentor = false;
      }

      if($scope.fields.PTypesViewScores == 'Yes')
      {
        $scope.fields.PTypesViewScores = true;
      } else
      {
        $scope.fields.PTypesViewScores = false;
      }

      if($scope.fields.PTypesSharing == 'Yes')
      {
        $scope.fields.PTypesSharing = true;
      } else
      {
        $scope.fields.PTypesSharing = false;
      }

      if($scope.fields.PTypesAllowVideo == 'Yes')
      {
        $scope.fields.PTypesAllowVideo = true;
      } else
      {
        $scope.fields.PTypesAllowVideo = false;
      }
    });
  } else
  {
    $scope.header.page_title = 'Create New Portfolio Type';
  }

  // Submit.
  $scope.submit = function ()
  {
    // Clear past errors
    $scope.errors = {}

    // Figure out url
    if(! $scope.edit)
    {
      var url = '/api/v1/ptypes/create?account_id=' + site.account_id;
      var toaster_message = 'New portfolio type has been created.';
      var return_path = '/portfolio-types/portfolio-manager';
    } else
    {
      var url = '/api/v1/ptypes/update/' + $routeParams.id + '?account_id=' + site.account_id;
      var toaster_message = 'Portfolio type has been updated.';
      var return_path = '/portfolio-types/portfolio-manager';
      var return_path = '/portfolio-types/portfolio-view/' + $routeParams.id;
    }

    // Add in our datepickers
    $scope.fields.PTypesStartDate = $scope.dates.start_date;
    $scope.fields.PTypesEndDate = $scope.dates.end_date;

    // Add extra stuff.
    if(! $scope.fields.PTypesScoringPlugin)
    {
      $scope.fields.PTypesScoringPlugin = '';
    }

    // Send data to server.
    $http.post(url, $scope.fields).success(function (json) {

      // Deal with errors.
      if(! json.status)
      {
        for(var i = 0; i < json.errors.length; i++)
        {
          $scope.errors[json.errors[i].field] = json.errors[i].error;
        }
      } else
      {
        toaster.pop({ type: 'success', body: toaster_message, toasterId: 'toaster-activity' });
        $location.path( return_path );
      }

    });
  }
});

//
// Manage Standards and Competencies.
//
app.controller('PortfolioStandardsCompetenciesCtrl', function ($scope, $http, $routeParams, toaster)
{
  $scope.errors = {}
  $scope.standards = [];
  $scope.new_standard = '';
  $scope.new_competency = '';
  $scope.active_competency = false;
  $scope.new_competencies_error = '';
  $scope.competencies = [];

  // Handle sort order changes for Standards
  $scope.sortableOptionsStandards = {
    orderChanged: function (event) {
      $http.post('/api/v1/standards/update_order?account_id=' + site.account_id, { Standards: $scope.standards }).success(function (json) {
      });
    }
  };

  // Handle sort order changes for Competencies
  $scope.sortableOptionsComp = {
    containment: '',
    itemMoved: function (event) {
    },
    orderChanged: function (event) {
      $http.post('/api/v1/competencies/update_order?account_id=' + site.account_id, { Competencies: $scope.competencies }).success(function (json) {
      });
    },
  };

  // Submit new standard
  $scope.new_standard_submit = function ()
  {
    $scope.errors = {}

    $http.post('/api/v1/standards/create?account_id=' + site.account_id, { StandardsPTypesId: $routeParams.id, StandardsTitle: $scope.new_standard }).success(function (json) {

      // Deal with errors.
      if(! json.status)
      {
        for(var i = 0; i < json.errors.length; i++)
        {
          $scope.errors[json.errors[i].field] = json.errors[i].error;
        }
      } else
      {
        $scope.new_standard = '';
        $scope.refresh_standards();
        toaster.pop({ type: 'success', body: 'New standard created.', toasterId: 'toaster-activity' });
      }
    });
  }

  // Update standards
  $scope.update_standards = function (row)
  {
    // Send request to server.
    $http.post('/api/v1/standards/update/' + row.StandardsId + '?account_id=' + site.account_id, { StandardsTitle: row.StandardsTitle }).success(function (json) {

      // Deal with errors.
      if(! json.status)
      {
        row.StandardsTitle_error = json.errors[0].error;
      } else
      {
        $scope.refresh_standards();
        toaster.pop({ type: 'success', body: 'Standard has been updated.', toasterId: 'toaster-activity' });
      }

    });
  }

  // Delete a standard.
  $scope.standard_delete = function (row)
  {
    // Send delete request to server
    $http.post('/api/v1/standards/delete/' + row.StandardsId + '?account_id=' + site.account_id).success(function (json) {
      $scope.active_competency = false;
      $scope.refresh_standards();
      toaster.pop({ type: 'info', body: 'Standard has been deleted.', toasterId: 'toaster-activity' });
    });
  }

  // Add new competency
  $scope.add_competency = function ()
  {
    $scope.new_competencies_error = '';

    // Send request to server.
    $http.post('/api/v1/competencies/create?account_id=' + site.account_id, { CompetenciesStandardsId: $scope.active_competency.StandardsId, CompetenciesTitle: $scope.new_competency }).success(function (json) {

      // Deal with errors.
      if(! json.status)
      {
        $scope.new_competencies_error = json.errors[0].error;
      } else
      {
        $scope.new_competency = '';
        $scope.refresh_competencies();
        toaster.pop({ type: 'success', body: 'New competency created.', toasterId: 'toaster-activity' });
      }
    });
  }

  // Update update_competency
  $scope.update_competency = function (row)
  {
    // Send request to server.
    $http.post('/api/v1/competencies/update/' + row.CompetenciesId + '?account_id=' + site.account_id, { CompetenciesTitle: row.CompetenciesTitle }).success(function (json) {

      // Deal with errors.
      if(! json.status)
      {
        row.CompetenciesTitle_error = json.errors[0].error;
      } else
      {
        $scope.refresh_competencies();
        toaster.pop({ type: 'success', body: 'Competency has been updated.', toasterId: 'toaster-activity' });
      }

    });
  }

  // Delete a competency.
  $scope.competency_delete = function (row)
  {
    // Send delete request to server
    $http.post('/api/v1/competencies/delete/' + row.CompetenciesId + '?account_id=' + site.account_id).success(function (json) {
      $scope.refresh_competencies();
      toaster.pop({ type: 'info', body: 'Competency has been deleted.', toasterId: 'toaster-activity' });
    });
  }

  // Set active active competency
  $scope.set_active_competency = function (row)
  {
    $scope.new_competencies_error = '';
    $scope.active_competency = row;
    $scope.refresh_competencies();
  }

  // Refresh Competencies
  $scope.refresh_competencies = function ()
  {
    // Get the competencies from the API.
    $http.get('/api/v1/competencies?order=CompetenciesOrder&sort=asc&&col_CompetenciesStandardsId=' + $scope.active_competency.StandardsId + '&account_id=' + site.account_id).success(function (json) {
      $scope.competencies = json.data;
    });
  }

  // Refresh standards
  $scope.refresh_standards = function ()
  {
    $http.get('/api/v1/standards?order=StandardsOrder&sort=asc&col_StandardsPTypesId=' + $routeParams.id + '&account_id=' + site.account_id).success(function (json) {
      $scope.standards = json.data;
    });
  }

  // Get portfolio data on server.
  $scope.header = {};
  $http.get('/api/v1/ptypes/' + $routeParams.id + '&account_id=' + site.account_id).success(function (json) {
    $scope.portfolio = json.data;

    // Update header variables
    $scope.header.page_title = 'Update Standards & Competencies: ' + $scope.portfolio.PTypesTitle;
    $scope.header.ptype_id = $routeParams.id;
    $scope.header.ptype_title = $scope.portfolio.PTypesTitle;
  });

  // Refresh data.
  $scope.refresh_standards();
});

//
// Qtype Mngr Ctrl
//
app.controller('QTypeMngrCtrl', function ($scope, $http, $routeParams, CONSTANTS, $filter, toaster)
{
  $scope.errors = {};
  $scope.portfolio = {};
  $scope.elements = [];
  $scope.header = {};

  $scope.questionTypes = CONSTANTS.questionTypes;

  $scope.new = {
    QTypesPTypesId: $routeParams.id,
    QTypesLabel: '',
    QTypesType: '',
    QTypesValues: '',
    QTypesRequired: 'No'
  }

  // Handle sort order changes for Questions
  $scope.sortableOptionsQuestionsArtifact = {
    orderChanged: function (event) {
      $http.post('/api/v1/qtypes/update_order?account_id=' + site.account_id, { QTypes: $scope.q_artifact }).success(function (json) {
      });
    }
  };

  $scope.sortableOptionsQuestionsResume = {
    orderChanged: function (event) {
      $http.post('/api/v1/qtypes/update_order?account_id=' + site.account_id, { QTypes: $scope.q_resume }).success(function (json) {
      });
    }
  };

  // Add a new element
  $scope.add_new_question= function (which)
  {
    $scope.errors = {}

    $scope.new.QTypesWhich = which;

    $http.post('/api/v1/qtypes/create?account_id=' + site.account_id, $scope.new).success(function (json) {

      // Deal with errors.
      if(! json.status)
      {
        for(var i = 0; i < json.errors.length; i++)
        {
          $scope.errors[json.errors[i].field] = json.errors[i].error;
        }
      } else
      {
        $scope.new = {
          QTypesPTypesId: $routeParams.id,
          QTypesRequired: 'No',
          QTypesLabel: '',
          QTypesType: '',
          QTypesValues: '',
          QTypesWhich: ''
        }

        $scope.refresh_elements();
        toaster.pop({ type: 'success', body: 'Question has been added.', toasterId: 'toaster-activity' });
      }
    });
  }

  // Get the refresh_elements data.
  $scope.refresh_elements = function ()
  {
    $http.get('/api/v1/qtypes?order=QTypesOrder&sort=asc&col_QTypesPTypesId=' + $routeParams.id + '&account_id=' + site.account_id).success(function (json) {
      $scope.elements = json.data;
      $scope.q_artifact = $filter('filter')( $scope.elements, 'Artifacts', 'QTypesWhich');
      $scope.q_resume = $filter('filter')( $scope.elements, 'Resume', 'QTypesWhich');
    });
  }

  // Update the update_element
  $scope.update_element = function (row)
  {
    var fields = {
      QTypesRequired: row.QTypesRequired,
      QTypesLabel: row.QTypesLabel,
      QTypesType: row.QTypesType,
      QTypesValues: row.QTypesValues
    };

    // Send request to server.
    $http.post('/api/v1/qtypes/update/' + row.QTypesId + '?account_id=' + site.account_id, fields ).success(function (json) {

      // Deal with errors.
      if(! json.status)
      {
        for(var i = 0; i < json.errors.length; i++)
        {
          row['error_' + json.errors[i].field] = json.errors[i].error;
        }
      } else
      {
        $scope.refresh_elements();
        toaster.pop({ type: 'success', body: 'Question has been updated.', toasterId: 'toaster-activity' });
      }

    });
  }

  // Delete a artifactextraelements.
  $scope.element_delete = function (row)
  {
    // Send delete request to server
    $http.post('/api/v1/qtypes/delete/' + row.QTypesId + '?account_id=' + site.account_id).success(function (json) {
      $scope.refresh_elements();
    });
  }

  // Get portfolio data on server.
  $http.get('/api/v1/ptypes/' + $routeParams.id + '&account_id=' + site.account_id).success(function (json) {
    $scope.portfolio = json.data;

    // Update header variables
    $scope.header.page_title = 'Update Questions & Prompts: ' + $scope.portfolio.PTypesTitle;
    $scope.header.ptype_id = $routeParams.id;
    $scope.header.ptype_title = $scope.portfolio.PTypesTitle;
  });

  // Load data on page load.
  $scope.refresh_elements();
});

//
// Assign support staff
//
app.controller('PortfolioTypeAssignSupportCtrl', function ($scope, $http, $routeParams, toaster)
{
});
