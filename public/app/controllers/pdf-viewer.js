/* angular */
(function (angular) {
    'use strict';
    angular.module('pdfviewer', [
        'angular-pdfjs'
    ])
    .controller('pdfCtrl', function ($scope, $timeout) {
      var url_array = window.location.href.split('?');
      $scope.pdfUrl = url_array[1];
      // Get the parent iFrame width so we can resize canval element.
      var container_width = angular.element( parent.document.getElementById('pdf-iframe'))[0].clientWidth;
    });
}(angular));
