//
// Artifacts - View
//
app.controller('ArtifactCtrl', function ($scope, $http, $routeParams, $timeout, $location, $rootScope)
{
  $scope.artifact = {}
  $scope.portfolio = {}
  $scope.artifacts = []
  $scope.selected_artifact = null;
  $scope.has_artifacts = true;
  $scope.selected_standard = $routeParams.section;
  $scope.open_comment_box = false;
  $scope.me = $scope.$parent.me;

  // Editing
  $scope.user_can_edit = false;

  // Scoring: Can this user score this artifact?
  $scope.user_can_score = false;
  if ( angular.equals({}, $scope.me) )
  {
    $scope.$on('me_loaded', function (event, args) {
      $scope.me = $scope.$parent.me;
      user_can_score();
    });
  } else {
    user_can_score();
  }

  function user_can_score()
  {
    if ( $scope.me.IsReviewer == 'Yes' )
    {
      angular.forEach($scope.me.Candidates, function(c) {
        if ( c.PortfoliosId == $routeParams.portfolio )
        {
          $scope.user_can_score = true;
        }
      });
    }
  }

  // End Scoring Determination

  // Commenting
  $scope.user_can_comment = true;

  // Listen for broadcasts
  $rootScope.$on('delete_artifact', function(event, data) {
    $http.post('/api/v1/artifacts/delete/' + data.Id + '?account_id=' + site.account_id, {}).success(function(json) {
      $scope.get_artifacts();
    });
  });

  // Get standards
  $scope.get_standards = function( ptypeid )
  {
    $http.get('/api/v1/standards/get_standards/' + ptypeid + '?account_id=' + site.account_id).success(function (json) {
      $scope.standards = json.data;
    });
  }

  // Create new artifact
  $scope.create_new = function ()
  {
    // Send request and get the new id.
    $http.post('/api/v1/artifacts/create' + '?account_id=' + site.account_id, { ArtifactsTitle: 'New Artifact', ArtifactsStandardId: $routeParams.section, ArtifactsPortfolioId: $routeParams.portfolio }).success(function (json) {
      $location.path('/portfolios/artifact-edit/' + $scope.portfolio.PTypesId + '/' + $routeParams.portfolio + '/' + json.data.ArtifactsId);
    });
  }

  // Get artifacts for standard
  $scope.get_artifacts = function()
  {
    // Load the portfolio data
    $http.get('/api/v1/portfolios/' + $routeParams.portfolio + '?account_id=' + site.account_id).success(function (json) {
      $scope.portfolio = json.data;

      $scope.get_standards( $scope.portfolio.PortfoliosPTypeId );

      // Can this user even view this portfolio?
      if ( $scope.me.UsersId !== $scope.portfolio.PortfoliosUserId && $scope.me.IsAdmin !== 'Yes' )
      {
        $http.get('/api/v1/portfolios/is_user_assigned/' + $routeParams.portfolio + '?account_id=' + site.account_id).success(function (json) {
          if ( json.data.IsAssigned == false )
          {
            window.location = 'dashboard';
          }
        });
      }

      // Can this user edit this artifact or add a new one?
      var portfolio_status = $scope.portfolio.PortfoliosStatus;

      if (
          $scope.me.UsersId == $scope.portfolio.PortfoliosUserId &&
          (
            portfolio_status == 'In Progress' ||
            portfolio_status == 'Completed' ||
            portfolio_status == 'Not Started' ||
            portfolio_status == 'Reopened for Editing'
          )
         )
      {
        $scope.user_can_edit = true;
      }

      // Load the standard data
      $http.get('/api/v1/standards/' + $routeParams.section + '?account_id=' + site.account_id).success(function (json) {
        $scope.standard = json.data;
      });

      // Load the artifacts for the standard page.
      $http.get('/api/v1/artifacts/get_artifacts_for_standard/' + $routeParams.portfolio + '/' + $routeParams.section + '?account_id=' + site.account_id).success(function (json) {
        // Scoring view 3rd reviewer issue:  
        // We need to loop over each artifact. If there are more than 2 scores,
        // then make sure score array is ordered by IOA so the scores display correctly.
        // e.g. IOA YES, IOA YES, IOA NO.  IOA NO needs to be 3rd so it can be greyed out or 
        // turned off depending on user type.
        var data = json.data;
        for ( var a = 0; a <= data.length - 1; a++ )
        {
          if ( data[a].Scores.length > 2 )
          {
            // Rearrange Scores array such that IOA = YES|YES|NO
            // Not YES|NO|YES or NO|YES|YES
            for ( var c = 0; c <= data[a].Scores.length - 1; c++ )
            {
              if ( data[a].Scores[c].ArtifactScoresIOA == 'No' )
              {
                // Then move this dude to the end of the array.
                console.log('move to end');
                data[a].Scores.push(data[a].Scores.splice(c, 1)[0]);
              }
            }
          }
        }
        console.log(data);

        $scope.artifacts = data;

        if ( $scope.artifacts.length == 0 )
        {
          $scope.has_artifacts = false;
        }

        // Load up the first artifact in the list for this standard
        // if we weren't passed an artifactid
        // Load artifact data if we're passed an artifactid
        if ( $routeParams.artifactid )
        {
          $scope.get_artifact( $routeParams.artifactid );
          $scope.selected_artifact = $routeParams.artifactid;
        } else {
          if ( $scope.artifacts.length > 0 )
          {
            $scope.get_artifact( $scope.artifacts[0].ArtifactsId );
            $scope.selected_artifact = $scope.artifacts[0].ArtifactsId;
          }
        }
      });
    });
  }

  $scope.get_artifact = function( artifact_id )
  {
    $scope.selected_artifact = artifact_id;

    // Load the artifact for the page (we do this instead of feeding off the call above because a get_by_id is likely to include more data)
    $http.get('/api/v1/artifacts/' + artifact_id + '/' + $scope.portfolio.PTypesScoringPlugin + '?account_id=' + site.account_id).success(function (json) {
      $scope.artifact = json.data;

      // Broadcast to let our scoring and messages panel know which data to load.
      $timeout(function() {
        $rootScope.$broadcast('load_messages', { ArtifactsId: artifact_id });
        if ( $scope.portfolio.PTypesScoringPlugin )
        {
          $rootScope.$broadcast('load_scores', { ArtifactsId: artifact_id, PTypesScoringPlugin: $scope.portfolio.PTypesScoringPlugin });
        }
      }, 1000);
    });

    // Load the artifact media
    $http.get('/api/v1/artifacts/get_media/' + artifact_id + '?order=ArtifactMediaLuCreatedAt&sort=asc' + '&account_id=' + site.account_id).success(function (json) {
      $scope.artifact_documents = json.data;
    });
  }

  // Load page data
  $scope.get_artifacts();
});
