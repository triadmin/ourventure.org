//
// Generic controller to handle all scoring plugins
//
app.controller('ScoringCtrl', function ($scope, $rootScope, $http, toaster)
{
  $scope.scoring = {};
  $scope.artifactid = 0;
  $scope.open_scoring_box = false;

  // Submit a new score.
  $scope.submit = function ( plugin_name, portfolio_id, standard_id )
  {
    $scope.scoring.plugin_name = plugin_name;
    $scope.scoring.portfolio_id = portfolio_id;
    $scope.scoring.standard_id = standard_id;

    // Send post data.
    $http.post('/api/v1/artifactscores/score/' + $scope.artifact.ArtifactsId + '?account_id=' + site.account_id, $scope.scoring ).success(function (json) {
      if(json.status)
      {
        // Send something back to template (like the score)
        $scope.scoring.score = json.data.score;
        toaster.pop({ type: 'success', body: 'Artifact score saved.', toasterId: 'toaster-activity' });
      }
    });
  }

  $scope.$on('load_scores', function(event, data) {
    if ( typeof data !== 'undefined' )
    {
      $scope.artifactid = data.ArtifactsId;
      $scope.plugin_name = data.PTypesScoringPlugin;
    }
    $scope.get_score_for_actifact();
  });

  //
  // Get score for artifact
  // This is for the reviewer - data here populates their
  // scoring panel form when they land on an artifact.
  //
  $scope.get_score_for_actifact = function()
  {
    $http.get('/api/v1/artifactscores/get_score_for_artifact/' + $scope.artifactid + '/' + $scope.plugin_name).success(function (json) {
      if(json.status && json.count > 0)
      {
        // Set the scoring variables.
        $scope.scoring = angular.fromJson( json.data[0].ArtifactScoresBody );
        $scope.scoring.score = json.data[0].ArtifactScoresDisplayScore;
      } else {
        // Clear the scoring form
        $scope.clear_scoring_form();
      }
    });
  }

  $scope.clear_scoring_form = function()
  {
    $scope.scoring = {};
  }

  //
  // Get all scores for artifact.
  // This method retreives all scoring data for an artifact
  // (all reviewers) and displays data at the top of the artifact
  //
  $scope.get_all_scores_for_actifact = function()
  {
    $http.get('/api/v1/artifactscores/get_scores_for_artifact/' + $scope.artifactid + '/' + $scope.plugin_name).success(function (json) {
      if(json.status && json.count > 0)
      {
        // Set the scoring variables.
        //$scope.scoring = angular.fromJson( json.data[0].ArtifactScoresBody );
        //$scope.scoring.score = json.data[0].ArtifactScoresScore;
      }
    });
  }

  // Open and close methods so we can do stuff
  // when the scoring window opens or closes.
  $scope.close = function()
  {
    $scope.open_scoring_box = false;
  }

  $scope.open = function()
  {
    $scope.open_scoring_box = true;
  }
});

app.controller('ScoringContentCtrl', function( $scope, $uibModal, $log )
{
  // Open
  $scope.open = function ( plugin_name ) {
    var modalInstance = $uibModal.open({
      animation: true,
      templateUrl: '/app/views/scoring/' + plugin_name + '/help_modal_content.html',
      controller: 'ScoringContentInstanceCtrl',
      size: 'lg',
      resolve: {}
    });
  }
});

app.controller('ScoringContentInstanceCtrl', function( $scope, $uibModalInstance )
{
  $scope.cancel = function() {
    $uibModalInstance.dismiss('cancel');
  };
});

app.controller('ScoringTestCtrl', function( $scope, $var )
{
  console.log('Hi there I am a controller');
});
