//
// Portfolio Overview
// Handles: views/portfolios/overview.html
//
app.controller('PortfolioOverviewCtrl', function ($scope, $rootScope, $http, $routeParams, $timeout)
{
  $scope.active_tab = {
    messages: true
  }
  $scope.portfolio_user_id = null;
  $scope.user_can_submit_for_review = false;
  $scope.user_has_submitted_for_review = false;
  $scope.portfolio_submit_success = false;

  $timeout(function() {
    $rootScope.$broadcast('load_messages');
  }, 1000);

  $scope.$on('user_submitted_portfolio', function(event, data) {
    $scope.user_can_submit_for_review = false;
    $scope.user_has_submitted_for_review = true;
    $scope.portfolio_submit_success = true;
  });

  // Get portfolio data: standards, competencies, completion, etc.
  $scope.get_portfolio_data = function ()
  {
    $http.get('/api/v1/portfolios/get_portfolio_menu/' + $routeParams.portfolio_id + '?account_id=' + site.account_id).success(function (json) {
      $scope.portfolio_data = json.data;
      $scope.portfolio_user_id = json.data.PortfolioType.PortfoliosUserId;

      // Can we submit this portfolio for review?
      // Check portfolio status first. If user has already submitted portfolio,
      // then don't even display the SUBMIT button.
      var portfolio_status = $scope.portfolio_data.PortfolioType.PortfoliosStatus;

      if (
        $scope.me.UsersId == $scope.portfolio_user_id &&
        (
          portfolio_status == 'Submitted for Review' ||
          portfolio_status == 'In Review' ||
          portfolio_status == 'Scored' ||
          portfolio_status == 'Scored and Released to Candidate'
        )
      )
      {
        $scope.user_has_submitted_for_review = true;
      }

      if (
          // Removed portfolio_status == 'In Progress' || from conditional.
          $scope.me.UsersId == $scope.portfolio_user_id &&
          ( portfolio_status == 'Completed' || portfolio_status == 'Reopened for Editing' )
         )
      {
        $scope.user_can_submit_for_review = true;
      }

      // Then check completion status of each standard.
      if ( $scope.user_can_submit_for_review === false )
      {
        var can_submit = true;
        angular.forEach( $scope.portfolio_data.data, function(value, key)
        {
          if ( value.Completion !== 'complete' && can_submit === true )
          {
            can_submit = false;
          }
        });

        $scope.user_can_submit_for_review = can_submit;
      }
    });

    $scope.portfolio_id = $routeParams.portfolio_id;
  }

  $scope.get_portfolio_data();
});
