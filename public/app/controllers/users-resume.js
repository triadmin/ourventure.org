//
// Users Resume
// This controller gets, renders, and updates user resume question like:
// About Me, Who do I Serve...anything an account owner puts into the
// PType Resume Questions section.
//
app.controller('UsersResumeCtrl', function ($scope, $http, $routeParams)
{
  $scope.edit_mode = false;
  $scope.is_about_me = 'Yes';

  // Load the portfolio data
  $http.get('/api/v1/portfolios/' + $routeParams.portfolio + '?account_id=' + site.account_id).success(function (json) {
    $scope.portfolio = json.data;
    $scope.artifactid = 0;

    // Can this user even view this portfolio?
    if ( $scope.me.UsersId !== $scope.portfolio.PortfoliosUserId && $scope.me.IsAdmin !== 'Yes' )
    {
      $http.get('/api/v1/portfolios/is_user_assigned/' + $routeParams.portfolio + '?account_id=' + site.account_id).success(function (json) {
        if ( json.data.IsAssigned == false )
        {
          window.location = 'dashboard';
        }
      });
    }

    $scope.get_standards( $scope.portfolio.PortfoliosPTypeId );
  });

  // Get standards
  $scope.get_standards = function( ptypeid )
  {
    $http.get('/api/v1/standards/get_standards/' + ptypeid + '?account_id=' + site.account_id).success(function (json) {
      $scope.standards = json.data;
    });
  }

  // Load the portfolio type data.
  /*
  $http.get('/api/v1/portfolios/' + $routeParams.portfolio + '?account_id=' + site.account_id).success(function (json) {
    $scope.portfolio = json.data;
  });
  */

  $scope.toggle_edit = function()
  {
    $scope.edit_mode = ! $scope.edit_mode;
  }
});
