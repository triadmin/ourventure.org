//
// Notifications
//
app.controller('NotificationsCtrl', function ($scope, $http, $routeParams, $rootScope)
{
  $scope.has_notifications = false;

  // Listen for emits
  $rootScope.$on('refresh_notifications', function() {
    $scope.get_notifications();
  });

  $scope.get_notifications = function()
  {
    $http.get('/api/v1/notifications?order=NotificationsCreatedAt&sort=desc&account_id=' + site.account_id).success(function (json) {
      $rootScope.new_notifications = 0;

      $scope.notifications = json.data;

      if($scope.notifications.length)
      {
        $scope.has_notifications = true;
        angular.forEach($scope.notifications, function(n) {
          if ( n.NotificationsHasRead == 'No' )
          {
            $rootScope.new_notifications++;
          }
        });
      }

    });
  }

  //
  // Mark a notification read
  //
  $scope.mark_notification_read = function( users_id, notification_id, index )
  {
    $rootScope.new_notifications--;

    $scope.notifications[index].NotificationsHasRead = 'Yes';

    // Send update request
    var fields = {
      UsersId: users_id,
      NotificationsId: notification_id
    }

    $http.post('/api/v1/notifications/mark_as_read?account_id=' + site.account_id, fields).success(function (json) {
    });
  }

  $scope.get_notifications();
});
