var app = angular.module('app', [ ]);

//
// Login Controller
//
app.controller('AuthLoginCtrl', function ($scope, $http)
{
  $scope.error_msg = false;

  $scope.fields = {
    UsersEmail: '',
    UsersPassword: ''
  }

  // Login.
  $scope.login_submit = function ()
  {
    $http.post('/login_post', $scope.fields).success(function (json) {

      if(json.status)
      {
        $scope.error_msg = false;
        window.location = 'dashboard';
      } else
      {
        $scope.error_msg = true;
      }

    });
  }
});


//
// Forgot Password Controller
//
app.controller('AuthForgotPasswordCtrl', function ($scope, $http)
{
  $scope.error_msg = '';
  $scope.success_msg = false;

  $scope.fields = {
    UsersEmail: ''
  }

  // Submit password.
  $scope.reset_submit = function ()
  {
    $http.post('/reset_password_post', $scope.fields).success(function (json) {
      if(json.status)
      {
        $scope.error_msg = '';
        $scope.fields.UsersEmail = '';
        $scope.success_msg = true;
      } else
      {
        //$scope.error_msg = json.errors.UsersEmail;
        $scope.error_msg = true;
      }

    });
  }   
});

//
// Login Controller
//
app.controller('AuthSetPasswordCtrl', function ($scope, $http)
{
  $scope.error_msg = '';

  $scope.fields = {
    UsersPassword: ''
  }

  // Submit password.
  $scope.submit = function ()
  {
    $http.post('/set_password_post/' + hash, $scope.fields).success(function (json) {
      if(json.status)
      {
        $scope.error_msg = '';
        window.location = 'login/setpassword';
      } else
      {
        $scope.error_msg = json.errors.UsersPassword;
      }

    });
  }  

  
});
