//
// Help Controller
//
app.controller('HelpCtrl', function ($scope, $http, $routeParams, $timeout, toaster, $rootScope)
{
  $scope.message_success = false;
  $scope.submit = function()
  {
    $http.post('/api/v1/contact/create?account_id=' + site.account_id, {
      'ContactBody': $scope.contact_body,
      'ContactUA': _get_user_agent_info()
    }).success(function (json)
    {
      $scope.message_success = true;
      $scope.contact_body = null;
    });
  }
});

//
// HelpContactListCtrl
//
app.controller('HelpContactListCtrl', function ($scope, $http, $routeParams, $timeout, toaster, $rootScope)
{
  $scope.get_contact_entries = function()
  {
    $http.get('/api/v1/contact/get?account_id=' + site.account_id).success(function (json)
    {
      $scope.messages = json.data;
    });
  }

  $scope.get_contact_entries();
});

//
// Get user agent info.
//
function _get_user_agent_info()
{
  var ua = navigator;
  var ua_string  = '<h3>Browser Information</h3>';
      ua_string += 'appCodeName: ' + ua.appCodeName + '<br />';
      ua_string += 'appName: ' + ua.appName + '<br />';
      ua_string += 'appVersion: ' + ua.appVersion + '<br />';
      ua_string += 'platform: ' + ua.platform + '<br />';
      ua_string += 'product: ' + ua.product + '<br />';
      ua_string += 'userAgent: ' + ua.userAgent + '<br />';
  return ua_string;
}
