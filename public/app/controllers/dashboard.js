//
// Dashboard Landing
//
app.controller('DashboardCtrl', function ($scope, $rootScope, $http, $timeout, toaster)
{
  $scope.mentor_candidates = [];
  $scope.review_candidates = [];

  // Who are we?
  $scope.$on('me_loaded', function (event, args) {
    $scope.me = $scope.$parent.me;
  });

  $timeout(function() {
    $rootScope.$broadcast('load_messages');
  }, 1000);

  // Get the portfolios for this user.
  $scope.get_portfolios = function ()
  {
    $scope.has_portfolios = false;

    $http.get('/api/v1/portfolios/get_portfolios_for_dashboard?account_id=' + site.account_id).success(function (json) {
      $scope.portfolios = json.data;

      if($scope.portfolios.length)
      {
        $scope.has_portfolios = true;
      }
    });
  }

  // If we're a mentor or reviewer, get candidates to which I'm assigned
  $scope.get_my_candidates = function ( user_type )
  {
    $http.get('/api/v1/portfolios/get_portfolios_for_mentor_reviewer/' + user_type + '?account_id=' + site.account_id).success(function (json) {
      if ( user_type == 'Mentor' )
      {
        $scope.mentor_candidates = json.data;
      }
      if ( user_type == 'Reviewer' )
      {
        $scope.reviewer_candidates = json.data;
      }
    });
  }

  $rootScope.$on('reviewer_mark_scoring_complete', function (event, args) {
    var portfolio_id = args.PortfoliosId;

    // Loop over reviewer_candidates collection and mark scoring complete = 'Yes'
    angular.forEach($scope.reviewer_candidates, function(value, key) {
      if ( value.PortfoliosId == portfolio_id )
      {
        value.PortfolioUserLuScoringComplete = 'Yes';
      }
    });
  });

  // Load data for page.
  $scope.get_my_candidates( 'Mentor' );
  $scope.get_my_candidates( 'Reviewer' );
  $scope.get_portfolios();
});
