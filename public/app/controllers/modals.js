//
// ModalMarkScoringCompleteCtrl - Controller for reviewer marking portfoio review complete.
//
app.controller('ModalMarkScoringCompleteCtrl', function( $scope, $uibModal, $log )
{
  // Open
  $scope.open = function ( portfolio_id ) {
    $scope.item_obj = {
      portfolio_id: portfolio_id
    };
    var modalInstance = $uibModal.open({
      animation: true,
      templateUrl: '/app/views/common/modals/modal-mark-scoring-complete.html',
      controller: 'ModalMarkScoringCompleteInstanceCtrl',
      size: 'lg',
      resolve: {
        item_obj: function () {
          return $scope.item_obj;
        }
      }
    });
  };
});

app.controller('ModalMarkScoringCompleteInstanceCtrl', function ($scope, $rootScope, $http, toaster, $uibModalInstance, item_obj)
{
  var portfolio_id = item_obj.portfolio_id;
  $scope.cancel = function() {
    $uibModalInstance.dismiss('cancel');
  };

  $scope.submit = function() {
    // Send update to server.
    $http.get('/api/v1/artifactscores/reviewer_mark_scoring_complete/' + portfolio_id +'?account_id=' + site.account_id).success(function (json) {
      toaster.pop({ type: 'success', body: 'You have marked this portfolio as scored.', toasterId: 'toaster-activity' });

      // Refresh our notifications panels.
      $rootScope.$broadcast('refresh_notifications');

      // Turn off Mark Scoring Complete button.
      $rootScope.$broadcast('reviewer_mark_scoring_complete', { PortfoliosId: portfolio_id });

    });
    $scope.cancel();
  }
});

//
// ModalReopenForEditingCtrl - Controller for showing warning modal when reopening a 
// portfolio for editing by the candidate.
//
app.controller('ModalReopenForEditingCtrl', function( $scope, $uibModal, $log )
{
  // Open
  $scope.open = function ( portfolio_id ) {
    $scope.item_obj = {
      portfolio_id: portfolio_id
    };
    var modalInstance = $uibModal.open({
      animation: true,
      templateUrl: '/app/views/common/modals/modal-reopen-for-editing.html',
      controller: 'ModalReopenForEditingInstanceCtrl',
      size: 'lg',
      resolve: {
        item_obj: function () {
          return $scope.item_obj;
        }
      }
    });
  };
});

//
// ModalReopenForEditingInstanceCtrl
//
app.controller('ModalReopenForEditingInstanceCtrl', function ($scope, $rootScope, $http, toaster, $uibModalInstance, item_obj)
{
  $scope.cancel = function() {
    $uibModalInstance.dismiss('cancel');
  };

  $scope.reopen = function() {
    $http.post( '/api/v1/portfolios/update_status' + '?account_id=' + site.account_id, {PortfoliosId: item_obj.portfolio_id, PortfoliosStatus: 'Reopened for Editing'} ).success(function (json) {
      toaster.pop({ type: 'success', body: 'Portfolio reopened for editing.', toasterId: 'toaster-activity' });
      $rootScope.$broadcast('refresh_candidates');
      $uibModalInstance.dismiss('cancel');
    });
  }
});

//
// ModalSubmitForReviewCtrl - Controller for submit portfolio for review modal window
//
app.controller('ModalSubmitForReviewCtrl', function ( $scope, $uibModal, $log )
{
  // Open
  $scope.open = function ( portfolio_data ) {
    $scope.has_submitted_for_review = false;
    $scope.item_obj = {
      portfolio_id: portfolio_data.PortfolioType.PortfoliosId,
      portfolio_data: portfolio_data
    };

    var modalInstance = $uibModal.open({
      animation: true,
      templateUrl: '/app/views/common/modals/modal-submit-for-review.html',
      controller: 'ModalSubmitForReviewInstanceCtrl',
      size: 'lg',
      resolve: {
        item_obj: function () {
          return $scope.item_obj;
        }
      }
    });
  };
});

//
// ModalSubmitForReviewInstanceCtrl
//
app.controller('ModalSubmitForReviewInstanceCtrl', function ($scope, $rootScope, $http, toaster, $uibModalInstance, item_obj)
{
  $scope.portfolio_data = item_obj.portfolio_data;
  $scope.cancel = function() {
    $uibModalInstance.dismiss('cancel');
  };

  $scope.submit = function() {
    $scope.submitting_portfolio = true;
    $http.post('/api/v1/portfolios/update_status?account_id=' + site.account_id, {'PortfoliosId': item_obj.portfolio_id, 'PortfoliosStatus': 'Submitted for Review'}).success(function (json) {
      toaster.pop({ type: 'success', body: 'Your portfolio has been submitted for review.', toasterId: 'toaster-activity' });

      // Update modal window with success message.
      $scope.has_submitted_for_review = true;

      // Refresh our notifications panels.
      $rootScope.$broadcast('refresh_notifications');
      $rootScope.$broadcast('user_submitted_portfolio');
    });
    //$scope.cancel();
  }
});

//
// ModalReleaseScoresCtrl - Controller for releasing scores to candidate modal window
// user_data is a full portfolio record for a user. We need portfolio_id and a name
// at minimum.
//
app.controller('ModalReleaseScoresCtrl', function ( $scope, $rootScope, $uibModal, $log )
{
  // Open
  $rootScope.$on('scores_released', function() {
    $scope.scores_released = true;
  });
  $scope.open = function ( user_data ) {
    $scope.item_obj = {
      portfolio_id: user_data.PortfoliosId,
      portfolio_data: user_data
    };

    console.log($scope.scores_released);
    console.log(user_data);

    var modalInstance = $uibModal.open({
      animation: true,
      templateUrl: '/app/views/common/modals/modal-release-scores-candidate.html',
      controller: 'ModalReleaseScoresCtrlInstanceCtrl',
      size: 'lg',
      resolve: {
        item_obj: function () {
          return $scope.item_obj;
        }
      }
    });
  };
});

//
// ModalReleaseScoresCtrlInstanceCtrl
//
app.controller('ModalReleaseScoresCtrlInstanceCtrl', function ($scope, $rootScope, $http, toaster, $uibModalInstance, item_obj)
{
  $scope.portfolio_id = item_obj.portfolio_id;
  $scope.portfolio_data = item_obj.portfolio_data;
  $scope.candidate_name = item_obj.portfolio_data.UsersFirstName + ' ' + item_obj.portfolio_data.UsersLastName;

  $scope.cancel = function() {
    $uibModalInstance.dismiss('cancel');
  };

  $scope.submit = function() {
    $http.post('/api/v1/portfolios/update_status?account_id=' + site.account_id, {'PortfoliosId': item_obj.portfolio_id, 'PortfoliosStatus': 'Scored and Released to Candidate'}).success(function (json) {
      toaster.pop({ type: 'success', body: 'You have released portfolio scores to ' + $scope.candidate_name + '.', toasterId: 'toaster-activity' });

      // Refresh our notifications panels.
      $rootScope.$broadcast('refresh_notifications');
      $rootScope.$broadcast('scores_released');
    });

    $scope.cancel();
  }
});

//
// ModalDeleteCtrl - Controller for delete modal window
//
app.controller('ModalDeleteCtrl', function ( $scope, $uibModal, $log )
{
  // Open
  $scope.open = function ( item_type, item_id, item_index ) {
    $scope.item_obj = {
      item_type: item_type,
      item_id: item_id,
      item_index: item_index
    }

    var modalInstance = $uibModal.open({
      animation: true,
      templateUrl: '/app/views/common/modals/modal-delete.html',
      controller: 'ModalInstanceCtrl',
      size: 'lg',
      resolve: {
        item_obj: function () {
          return $scope.item_obj;
        }
      }
    });
  };
});

//
// ModalRemoveCtrl - Controller for modal window to remove a
// user from a PType
//
app.controller('ModalRemoveCtrl', function ( $scope, $uibModal, $log )
{
  // Open
  $scope.open = function ( user_id, user_type, item_id, item_index ) {
    $scope.item_obj = {
      user_id: user_id,
      user_type: user_type,
      item_id: item_id,
      item_index: item_index
    }

    var modalInstance = $uibModal.open({
      animation: true,
      templateUrl: '/app/views/common/modals/modal-remove.html',
      controller: 'ModalInstanceCtrl',
      size: 'lg',
      resolve: {
        item_obj: function () {
          return $scope.item_obj;
        }
      }
    });
  };

});

//
// ModalUserSelectCtrl - Controller for displaying user search/select modal.
//
app.controller('ModalUserSelectCtrl', function ( $scope, $uibModal, $http )
{
  $scope.open = function ( user_type, ptype_id, title ) {
    // user_type = all, candidates, mentors, reviewers, admins
    // attach_to = ptype, candidate

    // Get users of user_type
    $http.get('/api/v1/users/type/' + user_type + '?account_id=' + site.account_id).success(function (json) {
      $scope.users = json.data;

      // See if this user is already selected for this ptype
      // Gotta loop through the object backward 'cause we're
      // removing items as we go.
      for ( var i = $scope.users.length - 1; i >= 0; i-- )
      {
        var user = $scope.users[i];
        angular.forEach(user.PTypes, function(p) {
          if ( p.PTypeUserLuPTypeId == ptype_id || p.PortfoliosPTypeId == ptype_id )
          {
            // Then we need to pop this user off of the list object
            var index = $scope.users.indexOf( user );
            $scope.users.splice(index, 1);
          }
        });
      };

      $scope.item_obj = {
        user_type: user_type,
        ptype_id: ptype_id,
        title: title,
        users: $scope.users
      }

      var modalInstance = $uibModal.open({
        animation: true,
        templateUrl: '/app/views/common/modals/modal-user-select.html',
        controller: 'ModalInstanceCtrl',
        size: 'lg',
        resolve: {
          item_obj: function () {
            return $scope.item_obj;
          }
        }
      });
    });
  };

});

//
// ModalAssignPeopleToCandidateCtrl - Controller for displaying combined mentor/reviewer select for assignment modal.
//
app.controller('ModalAssignPeopleToCandidateCtrl', function ( $scope, $uibModal, $http )
{
  $scope.open = function ( ptype_id, user_id, portfolio_id ) {
    // Get mentors and reviewers for this portfolio type.
    $http.get( '/api/v1/ptypes/get_mentors_reviewers_to_assign/' + ptype_id + '/' + user_id + '?account_id=' + site.account_id ).success(function (json) {
      $scope.mentors = json.data.Mentors;
      $scope.reviewers = json.data.Reviewers;

      $scope.item_obj = {
        ptype_id: ptype_id,
        user_id: user_id,
        portfolio_id: portfolio_id,
        mentors: $scope.mentors,
        reviewers: $scope.reviewers
      }

      var modalInstance = $uibModal.open({
        animation: true,
        templateUrl: '/app/views/common/modals/modal-candidate-assign.html',
        controller: 'ModalAssignPeopleToCandidateInstanceCtrl',
        size: 'lg',
        resolve: {
          item_obj: function () {
            return $scope.item_obj;
          }
        }
      });
    });
  };
});

//
// ModalAssignPeopleToCandidateInstanceCtrl
//
app.controller('ModalAssignPeopleToCandidateInstanceCtrl', function ( $scope, $rootScope, $http, $uibModalInstance, item_obj )
{
  $scope.ptype_id = item_obj.ptype_id;
  $scope.user_id = item_obj.user_id;
  $scope.mentors = item_obj.mentors;
  $scope.reviewers = item_obj.reviewers;
  $scope.portfolio_id = item_obj.portfolio_id;

  // Close modal from another controller.
  $rootScope.$on('close_modal', function (event, args) {
    $uibModalInstance.close();
  });

  $scope.cancel = function () {
    $uibModalInstance.dismiss('cancel');
  };

});

//
// ModalUserAssignCtrl - Controller for displaying mentor/reviewer select for assignment modal.
//
app.controller('ModalUserAssignCtrl', function ( $scope, $uibModal, $http )
{
  $scope.open = function ( user_id, user_type, ptype_id ) {
    // Get candidates (portfolios) for this portfolio type.
    $http.get( '/api/v1/ptypes/get_candidates_for_ptype/' + ptype_id + '?account_id=' + site.account_id ).success(function (json) {
      $scope.users = json.data;

      // Exclude candidates who are already assigned to this user_type and user_id
      for ( var i = $scope.users.length - 1; i >= 0; i-- )
      {
        var assigned = $scope.users[i].Assigned_users;
        var user = $scope.users[i];

        angular.forEach(assigned, function(a) {
          if (
              (user_type == 'mentor' &&
              a.PortfolioUserLuUserType == 'Mentor' &&
              a.UsersId == user_id) ||
              (user_type == 'reviewer' &&
              a.PortfolioUserLuUserType == 'Reviewer' &&
              a.UsersId == user_id)
            ) {
              // Then we need to pop this candidate off of the list object
              var index = $scope.users.indexOf( user );
              $scope.users.splice(index, 1);
            }
        });
      };

      $scope.item_obj = {
        user_id: user_id,
        title: user_type,
        users: $scope.users,
        user_type: user_type
      }

      var modalInstance = $uibModal.open({
        animation: true,
        templateUrl: '/app/views/common/modals/modal-user-assign.html',
        controller: 'ModalInstanceCtrl',
        size: 'lg',
        resolve: {
          item_obj: function () {
            return $scope.item_obj;
          }
        }
      });
    });
  };
});

//
// ModalFilesCtrl - Controller for displaying files to select for artifact.
//
app.controller('ModalFilesCtrl', function ( $scope, $uibModal, $http )
{
  $scope.open = function ( artifact_id ) {

    // Get files for this user
    $http.get('/api/v1/media?account_id=' + site.account_id).success(function (json) {
      $scope.files = json.data;

      var item_obj = {
        files: $scope.files,
        artifact_id: artifact_id
      }

      var modalInstance = $uibModal.open({
        animation: true,
        templateUrl: '/app/views/common/modals/modal-portfolio-files.html',
        controller: 'ModalFilesInstanceCtrl',
        size: 'lg',
        resolve: {
          item_obj: function () {
            return item_obj;
          }
        }
      });
    });
  };
});

//
// ModalReleaseFormFilesCtrl - Controller for displaying files to select for release form.
//
app.controller('ModalReleaseFormFilesCtrl', function ( $scope, $uibModal, $http )
{
  $scope.open = function ( artifact ) {
    // TODO: In the case of the Who Do I Serve QType, artifact will be the
    // qType object.  The methods here need to be refactored to take this
    // into account.
    var modalInstance = $uibModal.open({
      animation: true,
      templateUrl: '/app/views/common/modals/modal-release-form-files.html',
      controller: 'ModalReleaseFormFilesInstanceCtrl',
      size: 'lg',
        resolve: {
          artifact: artifact
        }
    });

  }

});

//
// ModalReleaseFormFilesInstanceCtrl - Modal controller for release forms
//
app.controller('ModalReleaseFormFilesInstanceCtrl', function ($scope, $rootScope, $http, $routeParams, toaster, $uibModalInstance, Upload, artifact) {
  $scope.files = [];
  //$scope.artifact_id = item_obj.artifact_id;
  $scope.selected_file = '';
  $scope.selected_file_id = 0;
  $scope.people = { names: '' }
  $scope.select_state = true;
  $scope.uploading_files = [];

  // For multiple files:
  $scope.uploadFiles = function (files)
  {
    // Loop through the files and upload them.
    if(files && files.length)
    {
      for(var i = 0; i < files.length; i++)
      {
        // Add to the list of files we are uploading in the UI.
        files[i].progress = 0;
        $scope.selected_file = files[i].name;
        $scope.uploading_files.push({ name: files[i].name, progress: 0 });

        // Upload file
        Upload.upload({
          url: '/api/v1/media/upload_file/artifacts',
          data: { file: files[i], MediaReleaseForm: 'Yes' },
          index: i
        }).then(function (resp)
        {
          // Start up the add people state
          $scope.selected_file_id = resp.data.data.Id;
          $scope.select_state = false;
          $scope.uploading_files.splice(resp.config.index, 1);
        }, function (resp)
        {

        }, function (evt)
        {
          var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
          $scope.uploading_files[evt.config.index].progress = progressPercentage;
        });
      }
    }
  }

  // Attach form by sending Ajax call to server
  $scope.attach_form = function ()
  {
    // TODO: If our artifact object contains ArtifactMediaLuId,
    // then we're dealing with artifact documentation.
    if ( artifact.ArtifactMediaLuId )
    {
      var post = {
        ReleaseMediaLuMediaId: $scope.selected_file_id,
        ReleaseMediaLuArtifactDocId: artifact.ArtifactMediaLuId
      }
    }
    // TODO: If our artifct object contains QTypesWhich, then
    // we're dealing with the Add Release Forms QType.
    if ( artifact.QTypesWhich == 'Resume' )
    {
      var post = {
        ReleaseMediaLuMediaId: $scope.selected_file_id,
        ReleaseMediaLuResumeId: site.user_id
      }
    }

    // Update the people
    $http.post('/api/v1/media/update/' + $scope.selected_file_id, { MediaPeople: $scope.people.names }).success(function (json) {
    });

    // Save the look up.
    $http.post('/api/v1/releasemedialu/create', post).success(function (json) {
      $rootScope.$emit('refresh_artifact_files');
      toaster.pop({ type: 'success', body: 'Release Form Uploaded.', toasterId: 'toaster-activity' });

      if ( artifact.QTypesWhich == 'Resume' )
      {
        $rootScope.$emit('refresh_release_form_qtype_files');
      } else {
        $scope.get_files();
      }
      $uibModalInstance.dismiss('cancel');
    });
  }

  // Delete media file.
  $scope.delete = function (media_id, name)
  {
    $http.post('/api/v1/media/delete/' + media_id, { Id: media_id }).success(function (json) {
      console.log(json);
      $scope.get_files();
    });
  }

  // Add attached form
  $scope.add = function (media_id, name, people)
  {
    $scope.selected_file_id = media_id;
    $scope.selected_file = name;
    $scope.people = { names: people }
    $scope.attach_form();
  }

  // Cancel overlay
  $scope.cancel = function () {
    $uibModalInstance.dismiss('cancel');
  }

  // Get files
  $scope.get_files = function ()
  {
    $http.get('/api/v1/media?col_MediaReleaseForm=Yes&order=MediaName&account_id=' + site.account_id).success(function (json) {
      $scope.files = json.data;
    });
  }

  // Load data.
  $scope.get_files();
});

//
// ModalFilesInstanceCtrl - Modal controller for files modal
//
app.controller('ModalFilesInstanceCtrl', function ($scope, $rootScope, $http, toaster, $uibModalInstance, item_obj) {
  $scope.files = item_obj.files;
  $scope.artifact_id = item_obj.artifact_id;
  $scope.selected_files = [];

  $scope.toggle_file_select = function( file )
  {
    file.selected = !file.selected;
  }

  $scope.add_selected_files = function()
  {
    // Loop over $scope.files and see which one's we've selected
    // then pass these to the server.
    angular.forEach( $scope.files, function( value, key )
    {
      if ( value.selected === true )
      {
        var file_obj = {
          MediaId: value.MediaId,
          ArtifactId: $scope.artifact_id
        }
        $scope.selected_files.push( file_obj );
      }
    });

    $http.post('/api/v1/artifactmedialu/save_files?account_id=' + site.account_id, {'Files': $scope.selected_files}).success(function (json) {
      $scope.cancel();
      $rootScope.$emit('refresh_artifact_files');
      $scope.selected_files = [];
      toaster.pop({ type: 'success', body: 'Files have been saved to your artifact.', toasterId: 'toaster-activity' });
    });
  }

  $scope.cancel = function () {
    $uibModalInstance.dismiss('cancel');
  };
});

//
// ModalInstanceCtrl - General Modal Controller
//
app.controller('ModalInstanceCtrl', function ($scope, $rootScope, $http, toaster, $uibModalInstance, item_obj) {
  // For deleting and removing
  $scope.item_type = item_obj.item_type;
  $scope.action = item_obj.action;

  // For YouTube Embed
  $scope.artifact_id = item_obj.artifact_id;

  // For users selection
  $scope.users = item_obj.users;
  $scope.user_type = item_obj.user_type;
  $scope.ptype_id = item_obj.ptype_id;
  $scope.user_id = item_obj.user_id;
  $scope.item_id = item_obj.item_id;

  // Modal title
  $scope.modal_title = item_obj.title;

  $scope.cancel = function () {
    $uibModalInstance.dismiss('cancel');
  };

  // Close modal from another controller.
  $rootScope.$on('close_modal', function (event, args) {
    $uibModalInstance.close();
  });

  $scope.delete = function ()
  {
    switch( item_obj.item_type )
    {
      case 'user':
        $rootScope.$emit('delete_user', { UsersId: item_obj.item_id, ItemIndex: item_obj.item_index });
      break;

      case 'portfolio type':
        $rootScope.$emit('delete_portfolio_type', { Id: item_obj.item_id, ItemIndex: item_obj.item_index });
      break;

      default:
        $rootScope.$emit('delete_' + item_obj.item_type, { Id: item_obj.item_id, ItemIndex: item_obj.item_index });
      break;
    }

    // Close the delete modal.
    $uibModalInstance.close();
  };

  $scope.remove = function ()
  {
    $rootScope.$emit('remove_user', {
      UsersId: $scope.user_id,
      ItemId: $scope.item_id,
      UserType: $scope.user_type,
      ItemIndex: item_obj.item_index
    });

    // Close the delete modal.
    $uibModalInstance.close();
  };
});

//
// ModalGovernanceCtrl - Controller for governance messages to Mentor and Reviewers when they click on View Portfolio button.
//
app.controller('ModalGovernanceCtrl', function ( $scope, $uibModal, $http )
{
  $scope.open = function ( user_type, user_id, portfolio_id, message ) {
    $scope.item_obj = {
      user_type: user_type,
      user_id: user_id,
      portfolio_id: portfolio_id,
      message: message
    };

    // Check to see if this mentor/reviewer has already agreed to governance message for this portfolio.

    var modalInstance = $uibModal.open({
      animation: true,
      templateUrl: '/app/views/common/modals/modal-mentor-reviewer-governance.html',
      controller: 'ModalGovernanceInstanceCtrl',
      size: 'lg',
      resolve: {
        item_obj: function () {
          return $scope.item_obj;
        }
      }
    });
  };
});

//
// ModalAssignPeopleToCandidateInstanceCtrl
//
app.controller('ModalGovernanceInstanceCtrl', function ( $scope, $rootScope, $http, $uibModalInstance, item_obj )
{
  $scope.message = item_obj.message;
  $scope.user_id = item_obj.user_id;
  $scope.portfolio_id = item_obj.portfolio_id;
  $scope.user_type = item_obj.user_type;

  var location_url = '/portfolios/scores/' + $scope.portfolio_id;
  if ( $scope.user_type == 'Mentor' )
  {
    var location_url = '/portfolios/aboutme/' + $scope.portfolio_id;
  }

  // Close modal from another controller.
  $rootScope.$on('close_modal', function (event, args) {
    $uibModalInstance.close();
  });

  $scope.submit = function() {
    // Update PortfolioUserLu record.
    $http.get('/api/v1/portfolios/mark_governance_reviewed/' + $scope.portfolio_id + '/' + $scope.user_type).success(function (json) {
      window.location = location_url;
      $uibModalInstance.dismiss('cancel');
    });
  }

  $scope.cancel = function () {
    $uibModalInstance.dismiss('cancel');
  };
});
