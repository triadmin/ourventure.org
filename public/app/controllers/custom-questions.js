//
// Custom Questions Controller
//
app.controller('CustomQuestionsViewCtrl', function ($scope, $http)
{
  /**
   * We should be able to render custom question responses anywhere in the site
   * by padding: ptype_id, portfolio_id, section_type (artifact,resume), section_id
   * (e.g. artifact_id)
   * */
  $scope.get_questions = function( ptype_id, portfolio_id, section_type, section_id )
  {
    $scope.questions = '';
  }
});

/**
 * To save a question response to the API, we would need:
 * ptype_id, portfolio_id, section_type (artifact, resume), section_id
 * question_id, response_value
 *
 * This controller also handles any JS for our custom questions.
 */
app.controller('CustomQuestionsSaveCtrl', function ($scope, $http)
{
  // Save question

  // Custom Question Funtions

  // Clone a question type into the DOM and add a DELETE button.
  $scope.duplicate_question = function( element_id )
  {
    /*
    var fields = angular.element( document.querySelector( '#' + element_id ) );

    fields.append(angular.copy( fields ));
    */
    var fields = $('#' + element_id).html();
    $( fields ).insertAfter( '#' + element_id );
    //$('#' + element_id).append( fields );
  }
});
