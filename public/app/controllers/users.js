//
// Users Landing
//
app.controller('UsersCtrl', function ($scope, $rootScope, $http, toaster)
{
  // Listen for emits
  $rootScope.$on('delete_user', function( event, data ) {
    $scope.delete_user( data.UsersId, data.ItemIndex );
  });

  $scope.users = [];
  $scope.show_user = {};

  // Show user information on click.
  $scope.client_show = function (row)
  {
    $scope.show_user = row;
  }

  // Delete a user.
  $scope.delete_user = function ( users_id, index )
  {
    // Send delete request to server
    $http.post('/api/v1/users/delete/' + users_id + '?account_id=' + site.account_id).success(function (json) {
      // Nothing to do here.
    });

    $scope.users.splice( index, 1 );
  }

  // Get the users to display on page.
  $scope.get_users = function ()
  {
    $http.get('/api/v1/users?account_id=' + site.account_id).success(function (json) {
      $scope.users = json.data;

      if($scope.users.length)
      {
        $scope.show_user = $scope.users[0];
      }
    });
  }

  // Resend user welcome email.
  $scope.send_welcome_email = function( users_id )
  {
    // Send delete request to server
    $http.post('/api/v1/users/send_welcome_email/' + users_id + '?account_id=' + site.account_id).success(function (json) {
      toaster.pop({ type: 'success', body: 'Welcome email was sent to user.', toasterId: 'toaster-activity' });
    });
  }

  // Load data for page.
  $scope.get_users();
});

//
// Show one user
//
app.controller('UserViewCtrl', function ($scope, $http, $routeParams)
{
  $scope.user = { };

  // Get the user to display
  $scope.get_user = function ()
  {
    $http.get('/api/v1/users/' + $routeParams.id + '&account_id=' + site.account_id).success(function (json) {
      $scope.user = json.data;
    });
  }

  // Load data for page.
  $scope.get_user();
});

//
// Add/Edit a user.
//
app.controller('UserAddEditCtrl', function ($scope, $http, $routeParams, $location, toaster, States)
{
  $scope.errors = {}
  $scope.ptypes = [];
  $scope.ptypes_selected = {};
  $scope.edit = false;
  $scope.states = States;
  $scope.page_title = 'Create New User';
  $scope.button_title = 'Create User';
  var toast_message = 'New user added.';
  var api_endpoint = '/api/v1/users/create?account_id=' + site.account_id;

  $scope.fields = {
    UsersFirstName: '',
    UsersLastName: '',
    UsersEmail: '',
    UsersState: '',
    UsersTitle: '',
    UsersInstitution: '',
    UsersAvatarId: '',
    AcctUsersLuCandidate: 'No',
    AcctUsersLuMentor: 'No',
    AcctUsersLuReviewer: 'No',
    AcctUsersLuAdmin: 'No',
    AcctUsersLuAdminReceiveMessagesCandidate: 'No',
    AcctUsersLuAdminReceiveMessagesMentor: 'No',
    AcctUsersLuAdminReceiveMessagesReviewer: 'No',
    PTypesAssigned: {}
  }

  $scope.password_success = false;
  $scope.password_text = '';
  $scope.password_error = '';
  $scope.password_confirm_text = '';

  // Are we in edit mode?
  if( $routeParams.id )
  {
    // We are in edit mode.
    $scope.edit = true;
    toast_message = 'User has been updated.';

    // API Endpoint
    api_endpoint = '/api/v1/users/update/' + $routeParams.id + '?account_id=' + site.account_id;

    // Get user data
    $http.get('/api/v1/users/' + $routeParams.id + '&account_id=' + site.account_id).success(function (json) {
      $scope.fields = json.data;
      $scope.fields.PTypesAssigned = {};

      // Is this user assigned to any PTypes?
      angular.forEach( json.data.PTypes, function(obj, key) {
        var ptype_id = obj.PortfoliosPTypeId;
        $scope.fields.PTypesAssigned[ptype_id] = 'Yes';
      });

      // Update page title
      $scope.page_title = 'Edit User: ' + $scope.fields.UsersFirstName + ' ' + $scope.fields.UsersLastName;
      $scope.button_title = 'Edit User';
    });
  }

  // Update password
  $scope.submit_password = function ()
  {
    $scope.password_error = '';

    // Make sure the passwords match.
    if($scope.password_text != $scope.password_confirm_text)
    {
      $scope.password_error = 'Password did not match.';
      return false;
    }

    // Make sure the passwords is not empty.
    if(! $scope.password_text.length)
    {
      $scope.password_error = 'Password can not be empty.';
      return false;
    }

    // Send the password update.
    $http.post('/api/v1/users/update_password/' + $routeParams.id, { Password: $scope.password_text }).success(function (json) {
      $scope.password_success = true;
      $scope.password_text = '';
      $scope.password_error = '';
      $scope.password_confirm_text = '';
    });
  }

  // Add/update user data on server.
  $scope.submit = function ()
  {
    // Saving.
    $scope.saving = true;

    // Clear past error.
    $scope.errors = {}

    // Send request to server
    $http.post( api_endpoint, $scope.fields ).success(function (json) {
      // Error?
      if(! json.status)
      {
        for(var i in json.errors)
        {
          $scope.errors[json.errors[i].field] = json.errors[i].error;
        }
      } else
      {
        var users_id = json.data.Id;

        // Update user ptype Assignment
        angular.forEach( $scope.fields.PTypesAssigned, function(state, ptype_id) {
          if ( state == 'No' )
          {
            // We only get here if a user has a portfolio but is being removed.
            // Get their existing portfolio information from fields.PTypes
            angular.forEach( $scope.fields.PTypes, function(ptype, key) {
              if ( ptype.PortfoliosPTypeId == ptype_id )
              {
                var portfolios_id = ptype.PortfoliosId;
                $http.post( '/api/v1/portfolios/update_status?account_id=' + site.account_id,
                  { PortfoliosId: portfolios_id, PortfoliosStatus: 'Inactive' }
                ).success(function (json) {
                  // Something might happen here.
                });
              }
            });

            toaster.pop({ type: 'success', body: 'Removed portfolio for user.', toasterId: 'toaster-activity' });
          } else if ( state == 'Yes' )
          {
            // Create portfolio for candidate.
            $http.post( '/api/v1/portfolios/create?account_id=' + site.account_id,
              { PortfoliosUserId: users_id, PortfoliosPTypeId: ptype_id }
            ).success(function (json) {
              if ( json.data.new_portfolio !== false )
              {
                toaster.pop({ type: 'success', body: 'Added portfolio for user.', toasterId: 'toaster-activity' });
              }
            });
          }
        });

        $scope.saving = false;
        toaster.pop({ type: 'success', body: toast_message, toasterId: 'toaster-activity' });
        $location.path('/users');
      }

    });
  }
});
