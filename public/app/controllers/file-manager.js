//
// File Manager
//
app.controller('FilesCtrl', function ($scope, $http, $routeParams)
{
  // See if we have a me object
  $scope.show_user = function()
  {
    console.log( $scope.$parent.me );
  }
});

//
// Filetype template controller stuff
//
app.controller('FilesTplCtrl', function ($scope, $http, $routeParams)
{
  // See if we have a me object
  $scope.get_pdf_src = function( pdf_url )
  {
    console.log('getting PDF file');
    return 'app/views/common/templates/document-types/pdf-viewer.html?' + pdf_url;
  }
});
