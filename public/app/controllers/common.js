//
// common.js - controllers used throughout the site
//

//
// DatepickerCtrl - Controller for datepicker popups
//
app.controller('DatepickerCtrl', function ($scope) {
  $scope.clear = function() {
    $scope.dt = null;
  };

  $scope.datePickerIsOpen = false;

  $scope.datePickerOpen = function( $event )
  {
    $scope.datePickerIsOpen = true;
  }
});

//
// TopNavCtrl - Controller for nav bar menu items.
//
app.controller('TopNavCtrl', function ($scope) {

  $scope.$on('me_loaded', function (event, args) {
    $scope.me = $scope.$parent.me;

    var user_types = [];

    if ( $scope.me.IsAdmin == 'Yes' )
      user_types.push('Admin');  

    if ( $scope.me.IsCandidate == 'Yes' )
      user_types.push('Candidate');

    if ( $scope.me.IsMentor == 'Yes' )
      user_types.push('Mentor');

    if ( $scope.me.IsReviewer == 'Yes' )
      user_types.push('Reviewer');

    $scope.user_type_string = user_types.join(', ');
  });
});

//
// Logout
//
app.controller('LogoutCtrl', function($scope, $http, $location) {
  $location.path('/logout');
});
