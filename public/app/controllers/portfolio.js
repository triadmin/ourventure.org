//
// Portfolios
//
app.controller('PortfolioCtrl', function ($scope, $http)
{
});

app.controller('PortfolioProgressCtrl', function ($scope, $http)
{
  $scope.get_progress = function()
  {
    $http.get('/api/v1/portfolios/get_portfolio_menu/' + $scope.portfolioid + '?account_id=' + site.account_id).success(function (json) {
      $scope.portfolio_standards = json.data.data;
      $scope.portfolio = json.data.PortfolioType;
      $scope.portfolio_progress_percentage = json.data.PortfolioProgressPercentage;
      $scope.portfolio_has_about_me = json.data.PortfolioHasAboutMePage;
      $scope.portfolio_view_scores = json.data.PortfolioType.PTypesViewScores;
    });
  }
});

app.controller('PortfolioScoresCtrl', function ($scope, $http, $routeParams, toaster)
{
  $scope.is_score_page = 'Yes';
  $scope.fields = {};
  $scope.portfolio_id = $routeParams.portfolio;
  $scope.is_my_portfolio = false;

  // Scoring: Can this user score this artifact?
  // TODO: We have this code in 2 places now: here and in artifacts.js
  // Do we want to abstract this into a helper?
  $scope.user_can_score = false;
  if ( angular.equals({}, $scope.me) )
  {
    $scope.$on('me_loaded', function (event, args) {
      $scope.me = $scope.$parent.me;
      user_can_score();
    });
  } else {
    user_can_score();
  }

  function user_can_score()
  { 
    // If I'm a reviewer, can I score this portfolio?
    if ( $scope.me.IsReviewer == 'Yes' )
    {
      angular.forEach($scope.me.Candidates, function(c) {
        if ( c.PortfoliosId == $scope.portfolio_id )
        {
          $scope.user_can_score = true;
        }
      });
    }
  }

  // Load the portfolio data
  $http.get('/api/v1/portfolios/' + $scope.portfolio_id + '?account_id=' + site.account_id).success(function (json) {
    $scope.portfolio = json.data;

    // Check for scores for this portfolio
    $scope.ptypes_id = $scope.portfolio.PTypesId;
    $scope.check_for_scores();
    $scope.get_standards($scope.ptypes_id);

    // Set comment fields
    $scope.fields = {
      ReviewerId: '',
      CommentsStrengths: '',
      CommentsAreasImprovement: '',
      CommentsOverall: ''
    };

    // Is this MY portfolio scoring page?
    if ( $scope.me.IsCandidate == 'Yes' )
    {
      // Loop through my available portfolios and see if one of them
      // matches the portfolio_id here.
      angular.forEach($scope.me.Portfolios, function(p) {
        if ( p.PortfoliosId == $scope.portfolio_id )
        {
          $scope.is_my_portfolio = true;
        }
      });
    }

    if ( $scope.user_can_score == true )
    {
      // I am a reviewer.  Just display my comments in the form.
      angular.forEach($scope.portfolio.ReviewerComments, function(c) {
        if ( c.PortfolioReviewerCommentsUsersId == $scope.me.UsersId )
        {
          $scope.fields = {
            ReviewerId: $scope.me.UsersId,
            CommentsStrengths: c.PortfolioReviewerCommentsStrengths,
            CommentsAreasImprovement: c.PortfolioReviewerCommentsAreasOfImprovement,
            CommentsOverall: c.PortfolioReviewerCommentsOverall
          };
        }
      });
    } else {
      // Display all comments from all reviewers in read-only mode.
    }
  });

  // Get standards
  $scope.get_standards = function( ptypeid )
  {
    $http.get('/api/v1/standards/get_standards/' + ptypeid + '?account_id=' + site.account_id).success(function (json) {
      $scope.standards = json.data;
    });
  }

  $scope.check_for_scores = function()
  {
    // Load any scores for this portfolio
    $http.get('/api/v1/artifactscores/check_for_scores/' + $scope.portfolio_id + '?account_id=' + site.account_id).success(function (json) {
      if ( json.data.length > 0 )
      {

        $scope.has_scores = true;
      } else {
        $scope.has_scores = false;
      }

      $scope.get_score_data_for_portfolio();
    });
  }

  $scope.get_score_data_for_portfolio = function()
  {
    $http.get('/api/v1/artifactscores/get_scores_for_portfolio/' + $scope.portfolio_id + '/' + $scope.ptypes_id + '?account_id=' + site.account_id).success(function (json) {
      // 3rd reviewer issue:  
      // We need to loop over each artifact. If there are more than 2 scores,
      // then make sure score array is ordered by IOA so the scores display correctly.
      // e.g. IOA YES, IOA YES, IOA NO.  IOA NO needs to be 3rd so it can be greyed out or 
      // turned off depending on user type.
      var data = json.data;
      for ( var a = 0; a <= data.length - 1; a++ )
      {
        //console.log(data[a].Artifacts);
        for ( var b = 0; b <= data[a].Artifacts.length - 1; b++ )
        {
          if ( data[a].Artifacts[b].Scores.length > 2 )
          {
            // Rearrange Scores array such that IOA = YES|YES|NO
            // Not YES|NO|YES or NO|YES|YES
            for ( var c = 0; c <= data[a].Artifacts[b].Scores.length - 1; c++ )
            {
              if ( data[a].Artifacts[b].Scores[c].ArtifactScoresIOA == 'No' )
              {
                // Then move this dude to the end of the array.
                data[a].Artifacts[b].Scores.push(data[a].Artifacts[b].Scores.splice(c, 1)[0]);
              }
            }
          }
        }
      }


      $scope.data = json.data;
    });
  }

  $scope.save_comments = function()
  {
    $http.post('/api/v1/portfolios/save_reviewer_comments/' + $scope.portfolio_id + '?account_id=' + site.account_id, $scope.fields).success(function (json) {
      //$scope.data = json.data;
      toaster.pop({ type: 'success', body: 'Your Reviewer Comments were saved.', toasterId: 'toaster-activity' });
    });
  }

  // I think this is where we need to work.
  $scope.is_user_mentor_for_portfolio = function()
  {
    var is_mentor = false;
    for ( var a = 0; a <= $scope.me.Mentees.length - 1; a++ )
    {
      if ( $scope.portfolio_id == $scope.me.Mentees[a].PortfoliosId )
      {
        is_mentor = true;
      }
    }

    return is_mentor;
  }
});
