//
// Portfolio Manager View Controller
//
app.controller('PortfolioTypeViewCtrl', function ($scope, $http, $rootScope, $routeParams, toaster)
{
  $scope.ptype_id = $routeParams.ptype_id;

  // Get ptype information
  $http.get('/api/v1/ptypes/' + $scope.ptype_id + '&account_id=' + site.account_id).success(function (json) {
    $scope.ptype = json.data;
  });
});

app.controller('PortfolioTypeViewUsersCtrl', function ($scope, $http, $rootScope, $routeParams, toaster)
{
  $scope.tabMentors = {};
  $scope.tabCandidates = {};
  $scope.tabReviewers = {};

  // In case we're coming here from a user link somewhere else in the app.
  $scope.user_type = $routeParams.user_type;
  $scope.user_id = $routeParams.user_id;

  switch ( $scope.user_type )
  {
    case 'candidate':
      $scope.tabCandidates.active = true;
      break;
    case 'mentor':
      $scope.tabMentors.active = true;
      break;
    case 'reviewer':
      $scope.tabReviewers.active = true;
      break;
    default:
      $scope.tabCandidates.active = true;
  }

  $scope.has_candidates = false;
  $scope.has_reviewers = false;
  $scope.has_mentors = false;
  $scope.show_user = {};

  // Show user information on click.
  $scope.client_show = function (row)
  {
    $scope.show_user = row;
  }

  $scope.activeTab = function()
  {
    $rootScope.$emit('tab-switch');
  }

  // Set up search filters
  $scope.searchcandidates = function( item )
  {
    if (!$scope.querycandidates || (item.UsersFirstName.toLowerCase().indexOf($scope.querycandidates) != -1) || (item.UsersLastName.toLowerCase().indexOf($scope.querycandidates.toLowerCase()) != -1) ){
      return true;
    }
    return false;
  }
  $scope.searchreviewers = function( item )
  {
    if (!$scope.queryreviewers || (item.UsersFirstName.toLowerCase().indexOf($scope.queryreviewers) != -1) || (item.UsersLastName.toLowerCase().indexOf($scope.queryreviewers.toLowerCase()) != -1) ){
      return true;
    }
    return false;
  }
  $scope.searchmentors = function( item )
  {
    if (!$scope.querymentors || (item.UsersFirstName.toLowerCase().indexOf($scope.querymentors) != -1) || (item.UsersLastName.toLowerCase().indexOf($scope.querymentors.toLowerCase()) != -1) ){
      return true;
    }
    return false;
  }

  $scope.get_candidates = function()
  {
    $http.get( '/api/v1/ptypes/get_candidates_for_ptype/' + $routeParams.ptype_id + '?account_id=' + site.account_id ).success(function (json) {
      $scope.candidates = json.data;
      if ( json.data.length > 0 )
        $scope.has_candidates = true;
        $rootScope.$emit('got_candidates');

        // If we've passed in a user_id, then show that user.
        if ( $scope.user_id )
        {
          angular.forEach( $scope.candidates, function(value, key) {
            if ( $scope.user_id == value.UsersId )
            {
              $scope.show_user = value;
            }
          });
        } else {
          // Else, show first candidate details
          // Nah, let's not - it's strange.
          //$scope.show_user = $scope.candidates[0];
          $scope.show_user = false;
        }
    });
  }

  $scope.get_mentors = function()
  {
    $http.get( '/api/v1/ptypes/get_mentors_for_ptype/' + $routeParams.ptype_id + '?account_id=' + site.account_id ).success(function (json) {
      $scope.mentors = json.data;
      if ( json.data.length > 0 )
      {
        $scope.has_mentors = true;

        // Show first mentor details
        // Nah let's not
        //$scope.show_user = $scope.mentors[0];
        $scope.show_user = false;
      }
    });
  }

  $scope.get_reviewers = function()
  {
    $http.get( '/api/v1/ptypes/get_reviewers_for_ptype/' + $routeParams.ptype_id + '?account_id=' + site.account_id ).success(function (json) {
      $scope.reviewers = json.data;
      if ( json.data.length > 0 )
      {
        $scope.has_reviewers = true;

        // Show first candidate details
        // Nah let's not
        //$scope.show_user = $scope.reviewers[0];
        $scope.show_user = false;
      }
    });
  }

  $scope.reopen_portfolio = function(portfolio_id)
  {
    $http.post( '/api/v1/portfolios/update_status' + '?account_id=' + site.account_id, {PortfoliosId: portfolio_id, PortfoliosStatus: 'Reopened for Editing'} ).success(function (json) {
      toaster.pop({ type: 'success', body: 'Portfolio reopened for editing.', toasterId: 'toaster-activity' });
      $scope.get_candidates();
    });
  }

  $scope.get_candidates();
  $scope.get_mentors();
  $scope.get_reviewers();

  $rootScope.$on( 'refresh_candidates', function (event, args) {
    $scope.get_candidates();
  });
  $rootScope.$on( 'refresh_mentors', function (event, args) {
    $scope.get_mentors();
  });
  $rootScope.$on( 'refresh_reviewers', function (event, args) {
    $scope.get_reviewers();
  });
  $rootScope.$on( 'remove_user', function (event, args) {
    $scope.remove_selected( args );
  });

  $scope.remove_assigned = function( portfolioUserLuId )
  {
    $http.get( '/api/v1/portfolios/remove_user_from_portfolio/' + portfolioUserLuId + '?account_id=' + site.account_id ).success(function (json) {
      $scope.get_candidates();
      $scope.get_mentors();
      $scope.get_reviewers();
    });
  }

  $scope.remove_selected = function( user_obj )
  {
    $http.post( '/api/v1/ptypes/remove_user_from_ptype' + '?account_id=' + site.account_id, user_obj ).success(function (json) {
      if ( user_obj.UserType == 'candidate' )
      {
        $scope.candidates.splice( user_obj.ItemIndex, 1 );
        $scope.get_candidates();
      }
      if ( user_obj.UserType == 'mentor' )
      {
        $scope.mentors.splice( user_obj.ItemIndex, 1 );
        $scope.get_mentors();
      }
      if ( user_obj.UserType == 'reviewer' )
      {
        $scope.reviewers.splice( user_obj.ItemIndex, 1 );
        $scope.get_reviewers();
      }
    });
  }
});

app.controller('PortfolioTypeViewStatsCtrl', function ($scope, $http, $rootScope, $routeParams, toaster)
{
  $scope.get_ptype_stats = function( ptype_id )
  {
    $http.get( '/api/v1/ptypes/get_ptype_stats/' + ptype_id + '?account_id=' + site.account_id ).success(function (json) {
      $scope.data = json.data;
      $scope.data.PTypesId = ptype_id;
    });
  }

  $rootScope.$on( 'refresh_candidates', function (event, args) {
    //$scope.get_ptype_stats( args.ptypeid );
  });
});

//
// PortfolioTypeViewForPortfolioStatusCtrl: big name for a controller
// that esentially pulls all portfolios of Ptype AND Status
//
app.controller('PortfolioTypeViewForPortfolioStatusCtrl', function ($scope, $http, $rootScope, $routeParams)
{
  $scope.get_portfolios_by_status = function( ptype_id, status )
  {
    $http.get( '/api/v1/ptypes/get_portfolios_by_status/' + ptype_id + '/' + status + '?account_id=' + site.account_id ).success(function (json) {
      $scope.portfolios = json.data;
    });
  }
});
