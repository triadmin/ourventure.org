var app = angular.module('app', ['ngRoute']);

//
// Reviewer List Controller
//
app.controller('ReviewerListCtrl', function ($scope, $http, $routeParams, $location)
{
  $scope.get_reviewers = function( ptypeid )
  {
    // Get reviewers for this ptype
    $http.get('/public/api/v1/ptypes/get_reviewers_for_ptype/' + ptypeid).success(function (json) {
      $scope.reviewers = json.data;

      if($scope.reviewers.length)
      {
        $scope.has_reviewers = true;
      }
    });
  }

  $scope.get_ptype_info = function( ptypeid )
  {
    // Get ptype information
    $http.get('/public/api/v1/ptypes/get_ptype/' + ptypeid).success(function (json) {
      $scope.ptype_info = json.data;
    });
  }

  $scope.get_reviewers(1);
  $scope.get_ptype_info(1);
});
