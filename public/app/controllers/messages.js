//
// Messages
//
app.controller('MessagesCtrl', function ($scope, $http, $timeout, $routeParams, $rootScope, toaster)
{
  $scope.select_recipients = false;
  $scope.has_recips = false;
  $scope.has_messages = false;
  $scope.selected_recips = [];
  $scope.replybox = [];
  $scope.artifactid = 0;

  $scope.me = $scope.$parent.me;

  $scope.save_recipients = function()
  {
    $scope.select_recipients = false;
  }

  //
  // Get our possible message recipients
  //
  $scope.get_recipients = function()
  {
    $http.get('/api/v1/messages/get_my_recipients?account_id=' + site.account_id).success(function (json) {
      $scope.recips = json.data;

      if($scope.recips.length)
      {
        console.log('Recipients: ' + $scope.recips.length);
        $scope.has_recips = true;
      }
    });
  }

  $scope.get_messages = function()
  {
    $http.get('/api/v1/messages/get_messages/0/500?artifactid=' + $scope.artifactid + '&account_id=' + site.account_id).success(function (json) {
      // Set unread message count to 0.
      $rootScope.messages_unread = 0;

      $scope.messages = json.data;

      if($scope.messages.length)
      {
        $scope.has_messages = true;
        angular.forEach($scope.messages, function(m) {
          if ( m.MessageRecipientLuHasRead == 'No' )
          {
            $rootScope.messages_unread++;
          }
        });
      }
    });
  }

  $scope.update_recip_selection = function( $event, row )
  {
    // Get the checkbox that needs to change.
    // If the user clicks on the checkbox itself, first option. Row, second option.
    if($event.target.name === 'selected_recips'){
      var checkbox = $event.target;
    }else{
      // Stops function from being fired twice when clicking on label
      if($event.target.localName === "label"){
        $event.preventDefault();
      }
      var checkbox = $event.currentTarget.childNodes[1].childNodes[1].children[0];

    // Update checkbox
    checkbox.checked = !checkbox.checked
    }
    if ( checkbox.checked )
    {
      var recip_obj = {
        UsersId: row.UsersId,
        UsersName: row.UsersFirstName + ' ' + row.UsersLastName,
        CheckId: checkbox.id,
        RowId: $event.currentTarget.id
      };

      var in_array = false;

      $scope.selected_recips.forEach (function (cur_recip) {
        if(cur_recip.UsersId === recip_obj.UsersId){
          in_array = true;      // Recipient found in the list
        }
      })

      // If recipient isn't in the list, add them
      if(!in_array){
        $scope.selected_recips.push( recip_obj );
      }
      // Add highlighting to selected recipient
      var box = angular.element( document.querySelector( '#'+recip_obj.RowId ) );
      box.addClass('highlight-row');
    } else {
      // Remove recipient from the list
      $scope.selected_recips.forEach (function (cur_recip) {
        if(cur_recip.CheckId === checkbox.id){
          $scope.selected_recips.splice($scope.selected_recips.indexOf(cur_recip), 1);
          // Remove highlighting to selected recipient
          var box = angular.element( document.querySelector( '#'+cur_recip.RowId ) );
          box.removeClass('highlight-row');
        }
      })
    }
  }

  //
  // Send our message
  //
  $scope.send_message = function()
  {
    $scope.has_recipients = true;
    $scope.has_message = true;

    var recipient_array = [];
    angular.forEach($scope.selected_recips, function(r) {
      recipient_array.push(r.UsersName);
    });
    var recipient_string = recipient_array.join(', ');

    var fields = {
      MessageRecipients: $scope.selected_recips,
      MessagesPTypeId: '',
      MessagesArtifactId: $scope.artifactid,
      MessagesTitle: '',
      MessagesBody: $scope.messagebox,
      MessagesReplyToId: '',
      MessagesToString: recipient_string
    };

    if ( recipient_array.length == 0 )
    {
      $scope.has_recipients = false;
    } else if ( typeof $scope.messagebox == 'undefined' ) {
      $scope.has_message = false;
    } else {
      // Send message to server
      $http.post('/api/v1/messages/send?account_id=' + site.account_id, fields).success(function (json) {
        $scope.get_messages();
        $scope.messagebox = '';
        $scope.selected_recips = [];

        // Clear checkboxes
        angular.forEach($scope.recips, function (recip) {
          recip.selected = false;
        });
        toaster.pop({ type: 'success', body: 'Your message has been sent.', toasterId: 'toaster-activity' });
      });
    }
  }

  //
  // Send a reply
  //
  $scope.send_reply = function( message_id, index, data )
  {
    var message_body = $scope.replybox[index];
    $scope.replybox[index] = '';

    var fields = {
      MessageRecipients: [],
      MessagesBody: message_body,
      MessagesReplyToId: message_id
    };

    data.Replies.push({
      MediaUrl: $scope.me.MediaUrl,
      MessagesBody: message_body,
      UsersFirstName: $scope.me.UsersFirstName,
      UsersLastName: $scope.me.UsersLastName
    });

    // Send delete request to server
    $http.post('/api/v1/messages/send?account_id=' + site.account_id, fields).success(function (json) {
      toaster.pop({ type: 'success', body: 'Your message has been sent.', toasterId: 'toaster-activity' });
    });
  }

  //
  // Mark a message read
  //
  $scope.mark_message_read = function( users_id, message_id, index )
  {
    $rootScope.messages_unread--;

    $scope.messages[index].MessageRecipientLuHasRead = 'Yes';

    // Send update request
    var fields = {
      UsersId: users_id,
      MessagesId: message_id
    }

    $http.post('/api/v1/messages/mark_as_read?account_id=' + site.account_id, fields).success(function (json) {
    });
  }


  $scope.get_recipients();

  $scope.$on('load_messages', function(event, data) {
    if ( typeof data !== 'undefined' )
    {
      $scope.artifactid = data.ArtifactsId;
    }
    $scope.get_messages();
  });
});

// configure moment relative time
moment.locale('en', {
  relativeTime: {
    future: "in %s",
    past: "%s ago",
    s: "a moment",
    m: "a minute",
    mm: "%d minutes",
    h: "an hour",
    hh: "%d hours",
    d: "a day",
    dd: "%d days",
    M: "a month",
    MM: "%d months",
    y: "a year",
    yy: "%d years"
  }
});
