//
// Users Profile
//
app.controller('UserProfileCtrl', function ($scope, $rootScope, $http, $routeParams, Upload, toaster, $timeout)
{  
  $scope.edit = false;

  // Get the user to display
  $scope.get_user = function ()
  {
    $http.get('/api/v1/users/' + $routeParams.id).success(function (json) {
      $scope.user = json.data;      
    });

    // Emit to let our messages panel know which messages to load.
    $timeout(function() {
      $rootScope.$broadcast('load_messages');
    }, 1000);
  }

  $scope.edit_user = function()
  {
    $scope.edit = true;
    if($scope.user.UsersGetEmails == 'Yes' || $scope.user.UsersGetEmails == true)
    {
      $scope.user.UsersGetEmails = true;
    } else
    {
      $scope.user.UsersGetEmails = false;
    }
  }

  $scope.cancel_edit = function()
  {
    $scope.edit = false;
  }

  // Load data for page.
  $scope.get_user();

  // Updating user
  $scope.uploadAvatar = function ( file )
  {
    if(file)
    {
      Upload.upload({
          url: '/api/v1/media/upload_file/users',
          data: {file: file}
      }).then(function (resp) {
        // Update user record in database.
        $scope.user.UsersAvatarId = parseInt(resp.data.data.Id);
        $scope.user.UsersAvatarImageUrl = resp.data.data.Url;

        $http.post( '/api/v1/users/update/' + $routeParams.id + '?account_id=' + site.account_id, $scope.user ).success(function (json)
        {
          $scope.refresh_loggedin_user();
          
          // Update image.
          $http.get('/api/v1/users/' + $routeParams.id).success(function (json) {
            $scope.user.MediaUrl = json.data.MediaUrl;      
          });          
          
          toaster.pop({ type: 'success', body: 'Your photo was updated.', toasterId: 'toaster-activity' });
        });
      }, function (resp) {
          console.log('Error status: ' + resp.status);
      }, function (evt) {
          var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
      });
    }
  }
  
  // Update password
  $scope.submit_password = function ()
  {
    $scope.password_error = '';

    // Make sure the passwords match.
    if($scope.password_text != $scope.password_confirm_text)
    {
      $scope.password_error = 'Password did not match.';
      return false;
    }

    // Make sure the passwords is not empty.
    if(! $scope.password_text.length)
    {
      $scope.password_error = 'Password can not be empty.';
      return false;
    }

    // Send the password update.
    $http.post('/api/v1/users/update_password/' + $routeParams.id, { Password: $scope.password_text }).success(function (json) {
      $scope.password_success = true;
      $scope.password_text = '';
      $scope.password_error = '';
      $scope.password_confirm_text = '';
    });
  }  

  $scope.update_user = function()
  {    
    if($scope.user.UsersGetEmails == false)
    {
      $scope.user.UsersGetEmails = 'No';
    } else
    {
      $scope.user.UsersGetEmails = 'Yes';
    }
    $http.post( '/api/v1/users/update/' + $routeParams.id + '?account_id=' + site.account_id, $scope.user ).success(function (json)
    {
      $scope.refresh_loggedin_user();
      toaster.pop({ type: 'success', body: 'Profile has been updated.', toasterId: 'toaster-activity' });
      $scope.edit = false;
    });
  }
});

app.controller('UserProfileEditCtrl', function ($scope, $http, $routeParams, Upload, toaster, $timeout)
{  
  $scope.fields = {
    UsersFirstName: '',
    UsersLastName: '',
    UsersEmail: '',
    UsersTitle: '',
    UsersInstitution: '',
    UsersGetEmails: false
  }

  $scope.uploadAvatar = function ( file )
  {
    if(file)
    {
      Upload.upload({
          url: '/api/v1/media/upload_file/users',
          data: {file: file}
      }).then(function (resp) {
        // Update user record in database.
        console.log(resp.data.data);
      }, function (resp) {
          console.log('Error status: ' + resp.status);
      }, function (evt) {
          var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
          console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
      });
    }
  }

  $scope.update_user = function()
  {
    console.log('Saving');
  }
});
