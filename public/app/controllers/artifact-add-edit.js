//
// Artifacts - Add / Edit
//
app.controller('ArtifactAddEditCtrl', function ($scope, $http, $routeParams, $rootScope, $location, Upload, toaster, $timeout)
{
  $scope.standards = []
  $scope.competencies = []
  $scope.artifact = {}
  $scope.portfolio = {}
  $scope.artifacts = []
  $scope.artifact_documents = []
  $scope.uploading_files = []
  $scope.competencies_list = []
  $scope.selected_competency = {};
  $scope.video_embed = false;
  $scope.google_embed = false;
  $scope.dropbox_embed = false;
  $scope.ptypeid = $routeParams.ptypeid;

  // Watch for file drag and drop
  $scope.$watch('files', function () {
    $scope.uploadFiles($scope.files);
  });

  // For multiple files:
  $scope.uploadFiles = function (files)
  {
    // Loop through the files and upload them.
    if(files && files.length)
    {
      for(var i = 0; i < files.length; i++)
      {
        // Add to the list of files we are uploading in the UI.
        files[i].progress = 0;
        $scope.uploading_files.push(files[i]);

        // Upload file
        Upload.upload({
          url: '/api/v1/media/upload_file/artifacts',
          data: { file: files[i] },
          index: i
        }).then(function (resp)
        {
          // Add send AJAX request to server to store this.
          $http.post('/api/v1/artifacts/add_media', { MediaId: resp.data.data.Id, ArtifactsId: $routeParams.artifactid }).success(function (json) {
            toaster.pop({ type: 'success', body: 'Document uploaded.', toasterId: 'toaster-activity' });
            $scope.load_documents();
            $scope.update_portfolio_status();
          });
          $scope.uploading_files.splice(resp.config.index, 1);
        }, function (resp)
        {

        }, function (evt)
        {
          var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
          $scope.uploading_files[evt.config.index].progress = progressPercentage;
        });
      }
    }
  }

  // Auto save
  $scope.save = function ()
  {
    // Add in the Competencies
    $scope.artifact.Competencies = [];

    for(var i in $scope.competencies_list)
    {
      $scope.artifact.Competencies.push($scope.competencies_list[i].CompetenciesId);
    }

    // Send ajax call to the server
    $http.post('/api/v1/artifacts/update/' + $routeParams.artifactid + '?account_id=' + site.account_id, $scope.artifact).success(function (json) {
      toaster.pop({ type: 'success', body: 'Saved.', toasterId: 'toaster-activity' });
      $scope.update_portfolio_status();
    });
  }

  // Add competency
  $scope.save_competency = function (row)
  {
    if(! $scope.selected_competency)
    {
      return false;
    }

    $scope.competencies_list.push({ CompetenciesTitle: $scope.selected_competency.CompetenciesTitle, CompetenciesId: $scope.selected_competency.CompetenciesId });

    $scope.save();
  }

  // Add a video embed
  $scope.save_video_url = function()
  {
    var media_obj = helpers.format_video_embed( $scope.video_url );

    var embed_url = '', media_name = '', media_type = '';
    if ( media_obj !== false )
    {
      embed_url = media_obj.embed_url;
      media_name = media_obj.media_name;
      media_type = media_obj.media_type;
    }

    var fields = {
      MediaName: media_name,
      MediaType: media_type,
      MediaPath: embed_url,
      ArtifactsId: $scope.artifact.ArtifactsId
    };

    $http.post('/api/v1/media/save_embed' + '?account_id=' + site.account_id, fields).success(function (json) {
      toaster.pop({ type: 'success', body: 'Video saved.', toasterId: 'toaster-activity' });
      $scope.video_embed = false;
      $scope.update_portfolio_status();
      $scope.load_documents();
      $scope.video_url = '';
    });
  }

  $scope.close_video_url = function()
  {
    $scope.video_embed = false;
  }

  // Add a Google Drive embed fle
  $scope.save_google_url = function()
  {
    var fields = {
      MediaName: 'Google Drive File',
      MediaType: 'embed/googledrive',
      MediaPath: $scope.google_url,
      ArtifactsId: $scope.artifact.ArtifactsId
    };

    $http.post('/api/v1/media/save_embed' + '?account_id=' + site.account_id, fields).success(function (json) {
      toaster.pop({ type: 'success', body: 'Google Drive file saved.', toasterId: 'toaster-activity' });
      $scope.google_embed = false;
      $scope.update_portfolio_status();
      $scope.load_documents();
      $scope.google_url = '';
    });
  }

  $scope.close_google_url = function()
  {
    $scope.google_embed = false;
  }

  $scope.update_portfolio_status = function()
  {
    var fields = {
      PortfoliosId: $scope.portfolio.PortfoliosId,
      PortfoliosStatus: 'In Progress'
    };
    $http.post('/api/v1/portfolios/update_status?account_id=' + site.account_id, fields).success(function (json) {});
  }

  // Delete Document
  $scope.delete_document = function (index, row)
  {
    $scope.artifact_documents.splice(index, 1);

    $http.post('/api/v1/artifactmedialu/remove/' + row.ArtifactMediaLuId + '?account_id=' + site.account_id).success(function (json) {
      toaster.pop({ type: 'danger', body: 'Document removed.', toasterId: 'toaster-activity' });
    });
  }

  // Move document (change the order)
  $scope.move_document = function (direction, index, row)
  {
    // Moving DOWN adds 1 to the index
    // Moving UP subtracts 1 from the index
    var new_index;
    if ( direction == 'up' )
    {
      new_index = parseInt( index - 1 );
    } else {
      new_index = parseInt( index + 1 );
    }
    var el = $scope.artifact_documents[ index ];

    $scope.artifact_documents.splice( index, 1 );
    $scope.artifact_documents.splice( new_index, 0, row );

    $http.post('/api/v1/artifactmedialu/update_order?account_id=' + site.account_id, { Documents: $scope.artifact_documents }).success(function (json) {
      toaster.pop({ type: 'success', body: 'Document moved.', toasterId: 'toaster-activity' });
    });
  }

  // Delete competency
  $scope.delete_competency = function (row, index)
  {
    $scope.competencies_list.splice(index, 1);
    $scope.save();
  }

  // Custom filter for competencies
  $scope.filter_competencies = function()
  {
    return function(row)
    {
      for(var i in $scope.competencies_list)
      {
        if($scope.competencies_list[i].CompetenciesId == row.CompetenciesId)
        {
          return false;
        }
      }

      return true;
    };
  };

  // Standard change.
  $scope.standard_change = function ()
  {
    $scope.competencies_list = [];
    get_selected_standard();
    $scope.save();
  }

  // Load the page data.
  $scope.load_data = function ()
  {
    // Load the portfilio
    $http.get('/api/v1/portfolios/' + $routeParams.portfolio + '?account_id=' + site.account_id).success(function (json) {
      $scope.portfolio = json.data;

      $http.get('/api/v1/standards/get_standards/' + $scope.portfolio.PortfoliosPTypeId + '?account_id=' + site.account_id).success(function (json) {
        $scope.standards = json.data;
        get_selected_standard();
      });

      // Get competencies
      $http.get('/api/v1/competencies/get_competencies_for_artifact_edit/' + $scope.portfolio.PortfoliosPTypeId + '/' + $routeParams.portfolio + '?account_id=' + site.account_id).success(function (json) {
        $scope.competencies = json.data;

        // Did we come here with a competencyId?
        if ( $routeParams.competency )
        {
          angular.forEach($scope.competencies, function(c) {
            if ( c.CompetenciesId == $routeParams.competency )
            {
              $scope.selected_competency = c;
              $scope.save_competency();
            }
          });
        }
      });
    });

    // Load the artifact for the page (we do this instead of feeding off the call above because a get_by_id is likely to include more data)
    $http.get('/api/v1/artifacts/' + $routeParams.artifactid + '?account_id=' + site.account_id).success(function (json) {
      $scope.artifact = json.data;
      $scope.artifactid = $routeParams.artifactid;
    });

    // Load the competencies assigned to this page.
    $http.get('/api/v1/artifactscompetlu?order=CompetenciesTitle&sort=asc&col_ArtifactsCompetLuArtifactId=' + $routeParams.artifactid + '&account_id=' + site.account_id).success(function (json) {

      $scope.competencies_list = [];

      for(var i in json.data)
      {
        $scope.competencies_list.push({ CompetenciesTitle: json.data[i].CompetenciesTitle, CompetenciesId: json.data[i].CompetenciesId });
      }

    });
  }

  // Delete release form.
  $scope.release_form_delete = function (row)
  {
    $http.post('/api/v1/releasemedialu/delete/' + row.ReleaseMediaLuId, { Id: row.ReleaseMediaLuId }).success(function (json) {
      $scope.load_documents();
    });
  }

  // Load documents
  $scope.load_documents = function ()
  {
    // Load the artifact media
    $http.get('/api/v1/artifacts/get_media/' + $routeParams.artifactid + '?account_id=' + site.account_id).success(function (json) {
      $scope.artifact_documents = json.data;
    });
  }

  // Cancel request.
  $scope.cancel = function()
  {
    window.history.back();
  }

  // If this is users' first artifact, let's notify
  // admin that they have started their portfolio.
  $scope.check_for_portfolio_start = function()
  {

  }

  function get_selected_standard()
  {
    for ( a=0; a<=$scope.standards.length-1; a++ )
    {
      if ( $scope.standards[a].StandardsId == $scope.artifact.ArtifactsStandardId )
      {
        $scope.selected_standard = $scope.standards[a].StandardsTitle;
      }
    }
  }

  // Load data.
  $scope.load_data();
  $scope.load_documents();

  // Refresh artifact documents when we add files through the
  // file modal.
  $rootScope.$on('refresh_artifact_files', function (event, args) {
    $scope.load_documents();
  });
});
