<?php

return [

  // Scoring Plugins
  'scoring_plugins' => [
    'IntervenerCertification' => 'Intervener Certification',
    'TestPlugin' => 'Test Plugin'
  ],
  'masquerade_secret_url' => 'b6d32a6ed04c4908755fd3626ed7a81f',
  'email_support' => 'LEANNE.COOK@UCDENVER.EDU',
  'contact_form_recipients' => [
    'LEANNE.COOK@UCDENVER.EDU',
    'chiefupstart@gmail.com'
  ]
];

/* End File */
