<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Portfolios03 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('Portfolios', function($table)
      {
        $table->dropColumn('PortfoliosMentor');
        $table->dropColumn('PortfoliosViewScores');
        $table->dropColumn('PortfoliosSharing');
        $table->dropColumn('PortfoliosStart');
        $table->dropColumn('PortfoliosEnd');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('Portfolios', function($table)
      {
        $table->enum('PortfoliosMentor', [ 'Yes', 'No' ])->default('Yes');
        $table->enum('PortfoliosViewScores', [ 'Yes', 'No' ])->default('Yes');
        $table->enum('PortfoliosSharing', [ 'Yes', 'No' ])->default('No');
        $table->date('PortfoliosStart');
        $table->date('PortfoliosEnd');
      });
    }
}
