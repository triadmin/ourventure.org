<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PTypes01 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('PTypes', function (Blueprint $table) {
      	$table->increments('PTypesId');
      	$table->integer('PTypesAccountId')->index('PTypesAccountId'); 
      	$table->string('PTypesName');
      	$table->timestamp('PTypesUpdatedAt'); 
      	$table->timestamp('PTypesCreatedAt');  
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		  Schema::dropIfExists('PTypes');
    }
}
