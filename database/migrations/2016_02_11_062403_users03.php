<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Users03 extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('Users', function($table)
		{  	    		       
      $table->dropColumn('UsersAvatar');  		   		 		
      $table->integer('UsersAvatarId')->after('UsersEmail');                                  		
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('Users', function($table)
		{      
    	$table->dropColumn('UsersAvatarId'); 
      $table->string('UsersAvatar')->after('UsersEmail');     		    	   	      	   	    	    	     	    	  	    	    	   
		});
	}
}
