<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Messages01 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('Messages', function (Blueprint $table) {
      	$table->increments('MessagesId');
      	$table->integer('MessagesAccountId')->index('MessagesAccountId');
        $table->integer('MessagesPTypeId')->index('MessagesPTypeId');
        $table->integer('MessagesArtifactId')->index('MessagesArtifactId');
        $table->integer('MessagesUserId')->index('MessagesUserId');
      	$table->string('MessagesTitle');
      	$table->text('MessagesBody');
        $table->integer('MessagesReplyToId');
      	$table->timestamp('MessagesUpdatedAt');
      	$table->timestamp('MessagesCreatedAt');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Messages');
    }
}
