<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class scoringplugins01 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('ScoringPlugins', function (Blueprint $table) {
      	$table->increments('ScoringPluginsId');
        $table->integer('ScoringPluginsAccountId')->index('ScoringPluginsAccountId');       	
      	$table->string('ScoringPluginsTitle', 250);
      	$table->mediumText('ScoringPluginsBody');
      	$table->string('ScoringPluginsFile', 250);
      	$table->timestamp('UsersUpdatedAt');
      	$table->timestamp('UsersCreatedAt');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::dropIfExists('ScoringPlugins');
    }
}
