<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Contact01 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('Contact', function (Blueprint $table) {
        $table->increments('ContactId');
        $table->integer('ContactAccountId')->index('ContactAccountId');
        $table->integer('ContactUsersId');
        $table->string('ContactBody');
        $table->enum('ContactResolved', [ 'Yes', 'No' ]);     	  	
        $table->timestamp('ContactUpdatedAt');
        $table->timestamp('ContactCreatedAt');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Contact');
    }
}
