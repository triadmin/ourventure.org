<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Applog01 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('AppLog', function (Blueprint $table) {
            $table->increments('AppLogId');
            $table->integer('AppLogAccountId')->index('AppLogAccountId');
            $table->integer('AppLogUserId')->default(0)->index('AppLogUserId');
            $table->string('AppLogType');
            $table->string('AppLogText');
            $table->timestamp('AppLogUpdatedAt');
            $table->timestamp('AppLogCreatedAt');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('AppLog');
    }
}
