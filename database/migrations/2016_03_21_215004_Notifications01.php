<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Notifications01 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Notifications', function (Blueprint $table) {
      	$table->increments('NotificationsId');
        $table->integer('NotificationsAccountId')->index('NotificationsAccountId');
        $table->integer('NotificationsUserId')->default(0)->index('NotificationsUserId');
        $table->string('NotificationsTitle');
        $table->mediumText('NotificationsBody');
      	$table->timestamp('NotificationsUpdatedAt');
      	$table->timestamp('NotificationsCreatedAt');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Notifications');
    }
}
