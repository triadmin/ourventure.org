<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Portfolios09 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('Portfolios', function($table)
      {
        $table->enum('PortfoliosGovernanceConfirmedCandidate', [
          'Yes',
          'No'
        ])->after('PortfoliosIOAPercentage')->default('No');

        $table->enum('PortfoliosGovernanceConfirmedMentor', [
          'Yes',
          'No'
        ])->after('PortfoliosGovernanceConfirmedCandidate')->default('No');

        $table->enum('PortfoliosGovernanceConfirmedReviewer', [
          'Yes',
          'No'
        ])->after('PortfoliosGovernanceConfirmedCandidate')->default('No');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('Portfolios', function($table)
      {
        $table->dropColumn('PortfoliosGovernanceConfirmedCandidate');
        $table->dropColumn('PortfoliosGovernanceConfirmedMentor');
        $table->dropColumn('PortfoliosGovernanceConfirmedReviewer');
      });
    }
}
