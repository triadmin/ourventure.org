<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PortfolioUserLu02 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('PortfolioUserLu', function($table)
      {
        $table->enum('PortfolioUserLuUserType', [
          'Mentor',
          'Reviewer'
        ])->after('PortfolioUserLuUserId')->default('Mentor');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::table('PortfolioUserLu', function($table)
        {
          $table->dropColumn('PortfolioUserLuUserType');
        });
    }
}
