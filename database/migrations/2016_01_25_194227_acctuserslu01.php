<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Acctuserslu01 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('AcctUsersLu', function (Blueprint $table) {
      	$table->increments('AcctUsersLuId');
      	$table->integer('AcctUsersLuAcctId');
      	$table->integer('AcctUsersLuUserId');
      	$table->timestamp('AcctUsersLuUpdatedAt'); 
      	$table->timestamp('AcctUsersLuCreatedAt');  
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		  Schema::dropIfExists('AcctUsersLu');
    }
}
