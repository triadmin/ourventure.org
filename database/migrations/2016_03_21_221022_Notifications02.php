<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Notifications02 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('Notifications', function($table)
        {
          $table->enum('NotificationsHasRead', [
            'Yes',
            'No'
          ])->after('NotificationsBody')->default('No');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('Notifications', function($table)
        {
          $table->dropColumn('NotificationsHasRead');
        });
    }
}
