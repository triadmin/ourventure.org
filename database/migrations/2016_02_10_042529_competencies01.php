<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Competencies01 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('Competencies', function (Blueprint $table) {
      	$table->increments('CompetenciesId');
        $table->integer('CompetenciesAccountId')->index('CompetenciesAccountId');       	
        $table->integer('CompetenciesStandardsId')->index('CompetenciesStandardsId');
        $table->string('CompetenciesTitle');
        $table->mediumText('CompetenciesBody');
      	$table->integer('CompetenciesOrder')->index('CompetenciesOrder');
      	$table->timestamp('CompetenciesUpdatedAt');
      	$table->timestamp('CompetenciesCreatedAt');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::dropIfExists('Competencies');
    }
}
