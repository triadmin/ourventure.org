<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PTypes04 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('PTypes', function($table)
        {
          $table->integer('PTypesMinimumPrompts')->after('PTypesScoringId');
          $table->enum('PTypesAllowVideo', [
            'Yes',
            'No'
          ])->after('PTypesMinimumPrompts')->default('No');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('PTypes', function($table)
        {
          $table->dropColumn('PTypesMinimumPrompts');
          $table->dropColumn('PTypesAllowVideo');
        });
    }
}
