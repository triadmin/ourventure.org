<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Media05 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('Media', function($table)
      {
        $table->text('MediaPeople')->after('MediaPath');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('Media', function($table)
      {
        $table->dropColumn('MediaPeople');
      });
    }
}
