<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Artifacts03 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('Artifacts', function($table)
      {
        $table->integer('ArtifactsStandardId')->after('ArtifactsPortfolioId');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('Artifacts', function($table)
      {
       $table->dropColumn('ArtifactsStandardId');
      });
    }
}
