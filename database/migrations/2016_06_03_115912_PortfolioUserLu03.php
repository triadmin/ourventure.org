<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PortfolioUserLu03 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('PortfolioUserLu', function($table)
      {
        $table->enum('PortfolioUserLuScoringComplete', [
          'Yes',
          'No'
        ])->after('PortfolioUserLuUserType')->default('No');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('PortfolioUserLu', function($table)
      {
        $table->dropColumn('PortfolioUserLuScoringComplete');
      });
    }
}
