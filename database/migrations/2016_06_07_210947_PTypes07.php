<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PTypes07 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('PTypes', function($table)
      {
        $table->mediumText('PTypesGovernanceCandidate')->after('PTypesAllowVideo');
        $table->mediumText('PTypesGovernanceMentor')->after('PTypesGovernanceCandidate');
        $table->mediumText('PTypesGovernanceReviewer')->after('PTypesGovernanceMentor');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      $table->dropColumn('PTypesGovernanceCandidate');
      $table->dropColumn('PTypesGovernanceMentor');
      $table->dropColumn('PTypesGovernanceReviewer');
    }
}
