<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Ptypes05 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('Accounts', function($table)
        {
          $table->dropColumn('AccountsScoringPlugins');
        });

        Schema::table('PTypes', function($table)
        {
          $table->dropColumn('PTypesScoringId');
          $table->string('PTypesScoringPlugin')->after('PTypesBody')->default('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('Accounts', function($table)
        {
          $table->string('AccountsScoringPlugins')->after('AccountsDisplayName')->default('IntervenerCertification');
        });

        Schema::table('PTypes', function($table)
        {
          $table->dropColumn('PTypesScoringPlugin');
        });
    }
}
