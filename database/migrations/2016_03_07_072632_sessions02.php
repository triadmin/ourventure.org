<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Sessions02 extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('Sessions', function($table)
		{  	    		       	   		 		
      $table->integer('user')->after('user_id')->index('user');                                  		
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('Sessions', function($table)
		{      
    	$table->dropColumn('user');    		    	   	      	   	    	    	     	    	  	    	    	   
		});
	}
}
