<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Media01 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('Media', function (Blueprint $table) {
      	$table->increments('MediaId');
      	$table->integer('MediaAccountId')->index('MediaAccountId');
      	$table->string('MediaName');
      	$table->string('MediaType');
      	$table->integer('MediaSize');
      	$table->string('MediaPath'); 
      	$table->string('MediaThumb');       	     	      	  	
      	$table->timestamp('MediaUpdatedAt'); 
      	$table->timestamp('MediaCreatedAt');  
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		  Schema::dropIfExists('Media');
    }
}
