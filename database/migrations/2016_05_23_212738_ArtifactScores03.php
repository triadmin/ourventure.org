<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ArtifactScores03 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('ArtifactScores', function($table)
      {
        $table->integer('ArtifactScoresStandardId')->after('ArtifactScoresArtifactId')->index('ArtifactScoresStandardId');

        $table->enum('ArtifactScoresIOA', [
          'Yes',
          'No'
        ])->after('ArtifactScoresScore')->default('No');

        $table->enum('ArtifactScoresStatus', [
          'Active',
          'Disabled'
        ])->after('ArtifactScoresIOA')->default('Active');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('ArtifactScores', function($table)
      {
        $table->dropColumn('ArtifactScoresStandardId');
        $table->dropColumn('ArtifactScoresIOA');
        $table->dropColumn('ArtifactScoresStatus');
      });
    }
}
