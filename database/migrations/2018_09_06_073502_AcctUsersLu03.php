<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AcctUsersLu03 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('AcctUsersLu', function($table)
		{  	    		          		 
            $table->enum('AcctUsersLuAdminReceiveMessagesCandidate', [ 'Yes', 'No' ])->after('AcctUsersLuAdmin')->default('No'); 
            $table->enum('AcctUsersLuAdminReceiveMessagesMentor', [ 'Yes', 'No' ])->after('AcctUsersLuAdminReceiveMessagesCandidate')->default('No'); 
            $table->enum('AcctUsersLuAdminReceiveMessagesReviewer', [ 'Yes', 'No' ])->after('AcctUsersLuAdminReceiveMessagesMentor')->default('No'); 
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('AcctUsersLu', function($table)
		{      
            $table->dropColumn('AcctUsersLuAdminReceiveMessagesCandidate');
            $table->dropColumn('AcctUsersLuAdminReceiveMessagesMentor');
            $table->dropColumn('AcctUsersLuAdminReceiveMessagesReviewer');
		});
    }
}
