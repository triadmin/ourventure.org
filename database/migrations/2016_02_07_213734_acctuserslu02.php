<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Acctuserslu02 extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('AcctUsersLu', function($table)
		{  	    		          		 
      $table->enum('AcctUsersLuAdmin', [ 'Yes', 'No' ])->after('AcctUsersLuUserId')->default('No'); 
      $table->enum('AcctUsersLuCandidate', [ 'Yes', 'No' ])->after('AcctUsersLuUserId')->default('No');  
      $table->enum('AcctUsersLuMentor', [ 'Yes', 'No' ])->after('AcctUsersLuUserId')->default('No'); 
      $table->enum('AcctUsersLuReviewer', [ 'Yes', 'No' ])->after('AcctUsersLuUserId')->default('No');                                  		
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('AcctUsersLu', function($table)
		{      
    	$table->dropColumn('AcctUsersLuAdmin'); 
    	$table->dropColumn('AcctUsersLuCandidate');  
    	$table->dropColumn('AcctUsersLuMentor'); 
    	$table->dropColumn('AcctUsersLuReviewer');      	   		    	   	      	   	    	    	     	    	  	    	    	   	
		});
	}
}
