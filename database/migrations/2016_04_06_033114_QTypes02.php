<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class QTypes02 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('QTypes', function($table)
        {
          $table->enum('QTypesRequired', [
            'Yes',
            'No'
          ])->after('QTypesValues')->default('No');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('QTypes', function($table)
        {
          $table->dropColumn('QTypesRequired');
        });
    }
}
