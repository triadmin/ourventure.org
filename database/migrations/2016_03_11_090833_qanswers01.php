<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Qanswers01 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('QAnswers', function (Blueprint $table) {
      	$table->increments('QAnswersId');
        $table->integer('QAnswersAccountId')->index('QAnswersAccountId');
        $table->integer('QAnswersQTypesId')->index('QAnswersQTypesId');
        $table->integer('QAnswersUserId')->index('QAnswersUserId');
        $table->mediumText('QAnswersValues');
      	$table->timestamp('QAnswersUpdatedAt');
      	$table->timestamp('QAnswersCreatedAt');
      });     
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::dropIfExists('QAnswers');    
    } 
}
