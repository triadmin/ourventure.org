<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MessageRecipientLu02 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('MessageRecipientLu', function($table)
        {
          $table->enum('MessageRecipientLuHasRead', [
            'Yes',
            'No'
          ])->after('MessageRecipientLuUserId')->default('No');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('MessageRecipientLu', function($table)
        {
          $table->dropColumn('MessageRecipientLuHasRead');
        });
    }
}
