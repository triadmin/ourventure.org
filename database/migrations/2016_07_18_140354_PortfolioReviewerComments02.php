<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PortfolioReviewerComments02 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('PortfolioReviewerComments', function($table)
      {
        $table->integer('PortfolioReviewerCommentsPortfolioId')->after('PortfolioReviewerCommentsUsersId');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('PortfolioReviewerComments', function($table)
      {
        $table->dropColumn('PortfolioReviewerCommentsPortfolioId');
      });
    }
}
