<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Media06 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('Media', function($table)
      {
        $table->enum('MediaReleaseForm', [ 'Yes', 'No' ])->default('No')->after('MediaPeople');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('Media', function($table)
      {
        $table->dropColumn('MediaReleaseForm');
      });
    }
}
