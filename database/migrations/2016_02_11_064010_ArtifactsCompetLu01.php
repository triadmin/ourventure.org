<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ArtifactsCompetLu01 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('ArtifactsCompetLu', function (Blueprint $table) {
      	$table->increments('ArtifactsCompetLuId');
      	$table->integer('ArtifactsCompetLuAccountId')->index('ArtifactsCompetLuAccountId');      	
      	$table->integer('ArtifactsCompetLuArtifactId');
      	$table->integer('ArtifactsCompetLuCompetId');
      	$table->timestamp('ArtifactsCompetLuUpdatedAt'); 
      	$table->timestamp('ArtifactsCompetLuCreatedAt');  
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		  Schema::dropIfExists('ArtifactsCompetLu');
    }
}
