<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MessageRecipientLu01 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('MessageRecipientLu', function (Blueprint $table) {
      	$table->increments('MessageRecipientLuId');
      	$table->integer('MessageRecipientLuAccountId')->index('MessageRecipientLuAccountId');
        $table->integer('MessageRecipientLuMessageId')->index('MessageRecipientLuMessageId');
        $table->integer('MessageRecipientLuUserId')->index('MessageRecipientLuUserId');
      	$table->timestamp('MessageRecipientLuUpdatedAt');
      	$table->timestamp('MessageRecipientLuCreatedAt');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('MessageRecipientLu');
    }
}
