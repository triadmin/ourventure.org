<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Portfolios13 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('Portfolios', function($table)
      {
        $table->date('PortfoliosDateCertify')->after('PortfoliosGovernanceConfirmedMentor');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('Portfolios', function($table)
      {
        $table->dropColumn('PortfoliosDateCertify');
      });
    }
}
