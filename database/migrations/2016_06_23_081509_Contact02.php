<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Contact02 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('Contact', function($table)
      {
        $table->mediumText('ContactUserAgent')->after('ContactBody');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('Contact', function($table)
      {
        $table->dropColumn('ContactUserAgent');
      });
    }
}
