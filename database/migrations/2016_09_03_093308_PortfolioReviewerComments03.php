<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PortfolioReviewerComments03 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::query("
          ALTER TABLE PortfolioReviewerComments
          MODIFY
          PortfolioReviewerCommentsStrengths MEDIUMTEXT NULL
        ");
        DB::query("
          ALTER TABLE PortfolioReviewerComments
          MODIFY
          PortfolioReviewerCommentsAreasOfImprovement MEDIUMTEXT NULL
        ");
        DB::query("
          ALTER TABLE PortfolioReviewerComments
          MODIFY
          PortfolioReviewerCommentsOverall MEDIUMTEXT NULL
        ");  
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
