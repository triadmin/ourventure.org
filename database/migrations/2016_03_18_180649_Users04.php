<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Users04 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('Users', function($table)
        {
          $table->enum('UsersStatus', [
            'Active',
            'Disabled'
          ])->after('UsersPassword')->default('Active');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('Users', function($table)
        {
          $table->dropColumn('UsersStatus');
        });
    }
}
