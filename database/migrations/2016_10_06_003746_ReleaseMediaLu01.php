<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ReleaseMediaLu01 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('ReleaseMediaLu', function (Blueprint $table) {
      	$table->increments('ReleaseMediaLuId');
      	$table->integer('ReleaseMediaLuAccountId')->index('ReleaseMediaLuAccountId');      	
      	$table->integer('ReleaseMediaLuArtifactDocId');
      	$table->integer('ReleaseMediaLuMediaId');   	
      	$table->timestamp('ReleaseMediaLuUpdatedAt'); 
      	$table->timestamp('ReleaseMediaLuCreatedAt');  
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		  Schema::dropIfExists('ReleaseMediaLu');
    }
}
