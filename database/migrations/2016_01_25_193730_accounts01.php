<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Accounts01 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('Accounts', function (Blueprint $table) {
      	$table->increments('AccountsId');
      	$table->integer('AccountsOwnerId');
      	$table->string('AccountsDisplayName');
      	$table->timestamp('AccountsUpdatedAt'); 
      	$table->timestamp('AccountsCreatedAt');  
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		  Schema::dropIfExists('Accounts');
    }
}
