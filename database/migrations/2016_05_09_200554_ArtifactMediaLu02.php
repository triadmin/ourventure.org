<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ArtifactMediaLu02 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('ArtifactMediaLu', function($table)
      {
        $table->integer('ArtifactMediaLuOrder')->after('ArtifactMediaLuMediaId');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $table->dropColumn('ArtifactMediaLuOrder');
    }
}
