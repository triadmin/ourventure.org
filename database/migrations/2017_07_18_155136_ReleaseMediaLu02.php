<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ReleaseMediaLu02 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('ReleaseMediaLu', function($table)
      {
        $table->integer('ReleaseMediaLuResumeId')->after('ReleaseMediaLuMediaId');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('ReleaseMediaLu', function($table)
      {
        $table->dropColumn('ReleaseMediaLuResumeId');
      });
    }
}
