<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PTypes06 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('PTypes', function($table)
      {
        $table->text('PTYpesIOAThreshold')->after('PTypesStartDate');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('PTypes', function($table)
      {
        $table->dropColumn('PTYpesIOAThreshold');
      });
    }
}
