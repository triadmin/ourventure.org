<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Portfolios04 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('Portfolios', function($table)
      {
        $table->integer('PortfoliosCompletionPercentage')->after('PortfoliosPTypeId');
        $table->enum('PortfoliosStatus', [
          'Not Started',
          'In Progress',
          'Completed',
          'Submitted for Review',
          'In Review',
          'Scored'
        ])->after('PortfoliosCompletionPercentage')->default('Not Started');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('Portfolios', function($table)
      {
        $table->dropColumn('ArtifactsExtraElements');
        $table->dropColumn('PortfoliosStatus');
      });
    }
}
