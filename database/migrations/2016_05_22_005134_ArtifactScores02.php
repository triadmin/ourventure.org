<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ArtifactScores02 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      // Drop ArtifactScoresScoringPluginId
      Schema::table('ArtifactScores', function($table)
      {
        $table->dropColumn('ArtifactScoresScoringPluginId');
      });

      Schema::table('ArtifactScores', function($table)
      {
        $table->integer('ArtifactScoresReviewerId')->after('ArtifactScoresPortfolioId');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      // Drop ArtifactsScoresReviewerId
      Schema::table('ArtifactScores', function($table)
      {
        $table->dropColumn('ArtifactsScoresReviewerId');
      });

      // Add back ArtifactScoresScoringPluginId
      Schema::table('Users', function($table)
      {
        $table->integer('ArtifactScoresScoringPluginId')->after('ArtifactScoresPortfolioId');
      });
    }
}
