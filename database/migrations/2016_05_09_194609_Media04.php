<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Media04 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('Media', function($table)
      {
        $table->integer('MediaUserId')->after('MediaAccountId')->index('MediaUserId');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('Media', function($table)
      {
        $table->dropColumn('MediaUserId');
      });
    }
}
