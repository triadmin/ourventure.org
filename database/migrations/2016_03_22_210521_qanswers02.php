<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class QAnswers02 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('QAnswers', function($table)
        {
          $table->integer('QAnswersArtifactsId')->after('QAnswersQTypesId');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('QAnswers', function($table)
        {
          $table->dropColumn('QAnswersArtifactsId');
        });
    }
}
