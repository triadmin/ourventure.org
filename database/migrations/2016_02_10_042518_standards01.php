<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Standards01 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('Standards', function (Blueprint $table) {
      	$table->increments('StandardsId');
        $table->integer('StandardsAccountId')->index('StandardsAccountId');       	
        $table->integer('StandardsPTypesId')->index('StandardsPTypesId');
        $table->string('StandardsTitle');
        $table->mediumText('StandardsBody');
      	$table->integer('StandardsOrder')->index('StandardsOrder');
      	$table->timestamp('StandardsUpdatedAt');
      	$table->timestamp('StandardsCreatedAt');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::dropIfExists('Standards');
    }
}
