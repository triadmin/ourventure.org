<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Artifacts01 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('Artifacts', function (Blueprint $table) {
      	$table->increments('ArtifactsId');
      	$table->integer('ArtifactsAccountId')->index('ArtifactsAccountId');
      	$table->string('ArtifactsTitle');
      	$table->text('ArtifactsBody');   	      	  	
      	$table->timestamp('ArtifactsUpdatedAt'); 
      	$table->timestamp('ArtifactsCreatedAt');  
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		  Schema::dropIfExists('Artifacts');
    }
}
