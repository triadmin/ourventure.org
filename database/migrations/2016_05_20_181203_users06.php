<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Users06 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('Users', function($table)
      {
        $table->enum('UsersGetEmails', [
          'Yes',
          'No'
        ])->after('UsersPassword')->default('Yes');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('Users', function($table)
      {
        $table->dropColumn('UsersGetEmails');
      });
    }
}
