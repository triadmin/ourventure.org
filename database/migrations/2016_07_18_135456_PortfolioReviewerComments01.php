<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PortfolioReviewerComments01 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('PortfolioReviewerComments', function (Blueprint $table) {
        $table->increments('PortfolioReviewerCommentsId');
        $table->integer('PortfolioReviewerCommentsAccountId')->index('PortfolioReviewerCommentsAccountId');
        $table->integer('PortfolioReviewerCommentsUsersId');
        $table->string('PortfolioReviewerCommentsStrengths');
        $table->string('PortfolioReviewerCommentsAreasOfImprovement');
        $table->string('PortfolioReviewerCommentsOverall');
        $table->timestamp('PortfolioReviewerCommentsUpdatedAt');
        $table->timestamp('PortfolioReviewerCommentsCreatedAt');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('PortfolioReviewerComments');
    }
}
