<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PTypes03 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('PTypes', function($table)
        {
          $table->integer('PTypesScoringId')->after('PTypesBody');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('PTypes', function($table)
        {
          $table->dropColumn('PTypesScoringId');
        });
    }
}
