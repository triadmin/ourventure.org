<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Artifactextraelements01 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('ArtifactExtraElements', function (Blueprint $table) {
      	$table->increments('ArtifactExtraElementsId');
        $table->integer('ArtifactExtraElementsAccountId')->index('ArtifactExtraElementsAccountId');
        $table->integer('ArtifactExtraElementsPTypesId')->index('ArtifactExtraElementsPTypesId');
        $table->string('ArtifactExtraElementsLabel');
        $table->string('ArtifactExtraElementsType');
        $table->mediumText('ArtifactExtraElementsValues');
      	$table->integer('ArtifactExtraElementsOrder')->index('ArtifactExtraElementsOrder');
      	$table->timestamp('ArtifactExtraElementsUpdatedAt');
      	$table->timestamp('ArtifactExtraElementsCreatedAt');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::dropIfExists('ArtifactExtraElements');
    }
}
