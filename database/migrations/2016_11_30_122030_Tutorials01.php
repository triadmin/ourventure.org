<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Tutorials01 extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('Tutorials', function (Blueprint $table) {
      $table->increments('TutorialsId');
      $table->string('TutorialsTitle');
      $table->mediumText('TutorialsBody');
      $table->enum('TutorialsAudience', [ 'All', 'Candidate', 'Mentor', 'Reviewer' ])->default('All');
      $table->integer('TutorialsOrder')->index('TutorialsOrder');
      $table->enum('TutorialsIsRequired', [ 'Yes', 'No' ])->default('No');
      $table->timestamp('TutorialsUpdatedAt');
      $table->timestamp('TutorialsCreatedAt');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('Tutorials');
  }
}
