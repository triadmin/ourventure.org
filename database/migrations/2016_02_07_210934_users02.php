<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Users02 extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('Users', function($table)
		{  	    		          		 		
      $table->string('UsersAvatar')->after('UsersEmail'); 
      $table->string('UsersInstitution')->after('UsersEmail'); 
      $table->string('UsersTitle')->after('UsersEmail');                                  		
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('Users', function($table)
		{      
    	$table->dropColumn('UsersInstitution'); 
    	$table->dropColumn('UsersTitle');     		    	   	      	   	    	    	     	    	  	    	    	   	
		});
	}
}
