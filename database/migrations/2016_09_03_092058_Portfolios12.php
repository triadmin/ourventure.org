<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Portfolios12 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      // Use DB::statement because we don't want to lose
      // data already in the production database for this column.
      DB::query("
        ALTER TABLE Portfolios
        MODIFY
        PortfoliosStatus
        ENUM(
          'Not Started',
          'In Progress',
          'Completed',
          'Submitted for Review',
          'In Review',
          'Scored',
          'Inactive',
          'Reopened for Editing',
          'Scored and Released to Candidate'
        )
        DEFAULT 'Not Started'
      ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
