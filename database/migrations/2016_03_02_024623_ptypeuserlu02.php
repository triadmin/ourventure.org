<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Ptypeuserlu02 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('PTypeUserLu', function($table)
      {
        $table->enum('PTypeUserLuUserType', [
          'Mentor',
          'Reviewer'
        ])->after('PTypeUserLuUserId')->default('Mentor');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('PTypeUserLu', function($table)
      {
        $table->dropColumn('PTypeUserLuUserType');
      });
    }
}
