<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Scoringplugins02 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      DB::statement('ALTER TABLE ScoringPlugins CHANGE UsersUpdatedAt ScoringPluginsUpdatedAt DATETIME');
      DB::statement('ALTER TABLE ScoringPlugins CHANGE UsersCreatedAt ScoringPluginsCreatedAt DATETIME');

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      DB::statement('ALTER TABLE ScoringPlugins CHANGE ScoringPluginsUpdatedAt UsersUpdatedAt DATETIME');
      DB::statement('ALTER TABLE ScoringPlugins CHANGE ScoringPluginsCreatedAt UsersCreatedAt DATETIME');
    }
}
