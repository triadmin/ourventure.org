<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Portfolios08 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('Portfolios', function($table)
      {
        $table->text('PortfoliosPassFail')->after('PortfoliosScoreText');
        $table->text('PortfoliosFailedStandards')->after('PortfoliosPassFail');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('Portfolios', function($table)
      {
        $table->dropColumn('PortfoliosPassFail');
        $table->dropColumn('PortfoliosFailedStandards');
      });
    }
}
