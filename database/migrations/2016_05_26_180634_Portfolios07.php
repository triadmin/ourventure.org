<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Portfolios07 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('Portfolios', function($table)
      {
        $table->text('PortfoliosScoreText')->after('PortfoliosScore');
        $table->integer('PortfoliosIOAPercentage')->after('PortfoliosScoreText');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('Portfolios', function($table)
        {
          $table->dropColumn('PortfoliosScoreText');
          $table->dropColumn('PortfoliosIOAPercentage');
        });
    }
}
