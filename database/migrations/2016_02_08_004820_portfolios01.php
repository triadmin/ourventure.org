<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Portfolios01 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('Portfolios', function (Blueprint $table) {
      	$table->increments('PortfoliosId');
      	$table->integer('PortfoliosAccountId')->index('PortfoliosAccountId'); 
      	$table->integer('PortfoliosUserId')->index('PortfoliosUserId'); 
      	$table->integer('PortfoliosPTypeId')->index('PortfoliosPTypeId'); 
      	$table->date('PortfoliosStart');
      	$table->date('PortfoliosEnd');	
      	$table->timestamp('PortfoliosUpdatedAt'); 
      	$table->timestamp('PortfoliosCreatedAt');  
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		  Schema::dropIfExists('Portfolios');
    }
}
