<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Ptypeuserlu01 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('PTypeUserLu', function (Blueprint $table) {
      	$table->increments('PTypeUserLuId');
      	$table->integer('PTypeUserLuAccountId')->index('PTypeUserLuAccountId'); 
      	$table->integer('PTypeUserLuPTypeId')->index('PTypeUserLuPTypeId');
      	$table->integer('PTypeUserLuUserId')->index('PTypeUserLuUserId');
      	$table->timestamp('PTypeUserLuUpdatedAt'); 
      	$table->timestamp('PTypeUserLuCreatedAt');  
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		  Schema::dropIfExists('PTypeUserLu');
    }
}
