<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Portfolios02 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('Portfolios', function($table)
      {
        $table->enum('PortfoliosMentor', [ 'Yes', 'No' ])->after('PortfoliosEnd')->default('Yes');
        $table->enum('PortfoliosViewScores', [ 'Yes', 'No' ])->after('PortfoliosEnd')->default('Yes');
        $table->enum('PortfoliosSharing', [ 'Yes', 'No' ])->after('PortfoliosEnd')->default('No');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('AcctUsersLu', function($table)
      {
    	$table->dropColumn('PortfoliosMentor');
    	$table->dropColumn('PortfoliosViewScores');
    	$table->dropColumn('PortfoliosSharing');
      });
    }
}
