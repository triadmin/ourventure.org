<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Qtypes01 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('QTypes', function (Blueprint $table) {
      	$table->increments('QTypesId');
        $table->integer('QTypesAccountId')->index('QTypesAccountId');
        $table->integer('QTypesPTypesId')->index('QTypesPTypesId');
        $table->enum('QTypesWhich', [ 'Artifacts', 'Resume' ])->index('QTypesWhich')->default('Artifacts');
        $table->string('QTypesLabel');
        $table->string('QTypesType');
        $table->mediumText('QTypesValues');
      	$table->integer('QTypesOrder')->index('QTypesOrder');
      	$table->timestamp('QTypesUpdatedAt');
      	$table->timestamp('QTypesCreatedAt');
      });
      
      Schema::dropIfExists('ArtifactExtraElements');      
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::dropIfExists('QTypes');
      
       Schema::create('ArtifactExtraElements', function (Blueprint $table) {
      	$table->increments('ArtifactExtraElementsId');
        $table->integer('ArtifactExtraElementsAccountId')->index('ArtifactExtraElementsAccountId');
        $table->integer('ArtifactExtraElementsPTypesId')->index('ArtifactExtraElementsPTypesId');
        $table->string('ArtifactExtraElementsLabel');
        $table->string('ArtifactExtraElementsType');
        $table->mediumText('ArtifactExtraElementsValues');
      	$table->integer('ArtifactExtraElementsOrder')->index('ArtifactExtraElementsOrder');
      	$table->timestamp('ArtifactExtraElementsUpdatedAt');
      	$table->timestamp('ArtifactExtraElementsCreatedAt');
      });     
    } 
}
