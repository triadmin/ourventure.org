<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ArtifactScores01 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ArtifactScores', function (Blueprint $table) {
      	$table->increments('ArtifactScoresId');
      	$table->integer('ArtifactScoresAccountId')->index('ArtifactScoresAccountId');
        $table->integer('ArtifactScoresScoringPluginId')->index('ArtifactScoresScoringPluginId');
        $table->integer('ArtifactScoresPortfolioId')->index('ArtifactScoresPortfolioId');
        $table->integer('ArtifactScoresArtifactId')->index('ArtifactScoresArtifactId');
        $table->text('ArtifactScoresBody');
        $table->decimal('ArtifactScoresScore', 5, 2);
        $table->timestamp('ArtifactScoresUpdatedAt');
      	$table->timestamp('ArtifactScoresCreatedAt');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ArtifactScores');
    }
}
