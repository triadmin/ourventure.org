<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PortfolioUserLu04 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('PortfolioUserLu', function($table)
      {
        $table->enum('PortfolioUserLuGovernanceConfirmed', [
          'No',
          'Yes'
        ])->after('PortfolioUserLuScoringComplete')->default('No');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('PortfolioUserLu', function($table)
      {
        $table->dropColumn('PortfolioUserLuGovernanceConfirmed');
      });
    }
}
