<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Users01 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('Users', function (Blueprint $table) {
      	$table->increments('UsersId');
      	$table->string('UsersFirstName');
      	$table->string('UsersLastName');
      	$table->string('UsersEmail'); 	
      	$table->string('UsersPassword', 500);  	
      	$table->timestamp('UsersUpdatedAt'); 
      	$table->timestamp('UsersCreatedAt');  
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		  Schema::dropIfExists('Users');
    }
}
