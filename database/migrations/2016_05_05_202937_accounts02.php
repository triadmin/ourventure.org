<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Accounts02 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('Accounts', function($table)
        {
          $table->string('AccountsScoringPlugins')->after('AccountsDisplayName')->default('IntervenerCertification');
        });
        
        Schema::dropIfExists('ScoringPlugins');
    }

    /**
     * Reverse the migrations.
     *
     * @return void 
     */
    public function down()
    {
        Schema::table('Accounts', function($table)
        {
          $table->dropColumn('AccountsScoringPlugins');
        });
    }
}
