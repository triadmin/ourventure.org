<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PortfolioUserLu01 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('PortfolioUserLu', function (Blueprint $table) {
      	$table->increments('PortfolioUserLuId');
      	$table->integer('PortfolioUserLuAccountId')->index('PortfolioUserLuAccountId');
      	$table->integer('PortfolioUserLuPortfolioId')->index('PortfolioUserLuPortfolioId');
      	$table->integer('PortfolioUserLuUserId')->index('PortfolioUserLuUserId');
      	$table->timestamp('PortfolioUserLuUpdatedAt');
      	$table->timestamp('PortfolioUserLuCreatedAt');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::dropIfExists('PortfolioUserLu');
    }
}
