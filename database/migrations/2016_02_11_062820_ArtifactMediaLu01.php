<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ArtifactMediaLu01 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('ArtifactMediaLu', function (Blueprint $table) {
      	$table->increments('ArtifactMediaLuId');
      	$table->integer('ArtifactMediaLuAccountId')->index('ArtifactMediaLuAccountId');      	
      	$table->integer('ArtifactMediaLuArtifactId');
      	$table->integer('ArtifactMediaLuMediaId');
      	$table->timestamp('ArtifactMediaLuUpdatedAt'); 
      	$table->timestamp('ArtifactMediaLuCreatedAt');  
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		  Schema::dropIfExists('ArtifactMediaLu');
    }
}
