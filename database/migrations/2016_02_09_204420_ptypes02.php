<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Ptypes02 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('PTypes', function($table)
      {
        $table->mediumText('PTypesBody')->after('PTypesName');
        $table->enum('PTypesMentor', [ 'Yes', 'No' ])->after('PTypesName')->default('Yes');
        $table->enum('PTypesViewScores', [ 'Yes', 'No' ])->after('PTypesName')->default('Yes');
        $table->enum('PTypesSharing', [ 'Yes', 'No' ])->after('PTypesName')->default('No');
        $table->date('PTypesStartDate')->after('PTypesName');
        $table->date('PTypesEndDate')->after('PTypesName');
      });
      
      DB::statement('ALTER TABLE PTypes CHANGE PTypesName PTypesTitle VARCHAR (255)');   
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      DB::statement('ALTER TABLE PTypes CHANGE PTypesTitle PTypesName VARCHAR (255)');      
      
      Schema::table('PTypes', function($table)
      {
        $table->dropColumn('PTypesBody');
        $table->dropColumn('PTypesMentor');
        $table->dropColumn('PTypesViewScores');
        $table->dropColumn('PTypesSharing');
        $table->dropColumn('PTypesStartDate');
        $table->dropColumn('PTypesEndDate');
      });
    }
}
